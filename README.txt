Le fichier .jar exécutable se trouve dans /target

La fonctionnalité de l'équipe de développement est de pouvoir masquer des figures à l'aide du Panneau "Drawings". Il faut simplement cliquer sur l'oeil pour faire apparaître et disparaître une figure.
