package controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.Spinner;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import model.DrawingMode;
import model.DrawingModel;
import model.command.Command;
import model.command.CreateDrawing;

public class PaintControllerTest extends ApplicationTest {
	private final List<String> drawingModes = Arrays.asList(DrawingMode.BRUSH, DrawingMode.ELLIPSE, DrawingMode.LINE,
			DrawingMode.MAN, DrawingMode.RECTANGLE, DrawingMode.UDES);

	private PaintController paintController;
	private ToggleButton toggleButton;
	private RadioMenuItem radioMenuItem;
	private String drawingMode;

	private ComboBox<String> drawingChooser;

	private Spinner<Double> objWidth;
	private Spinner<Double> objHeight;

	private ListView<DrawingModel> drawingPanel;
	private ListView<Command> historyPanel;

	private Pane propertyPanelWrapper;
	private Pane drawingPanelWrapper;
	private Pane historyPanelWrapper;
	private Pane textPanelWrapper;

	private Pane pane;

	@Override
	public void start(Stage stage) throws Exception {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/PaintView.fxml"));
		Scene scene = new Scene(fxmlLoader.load());

		stage.setScene(scene);

		paintController = fxmlLoader.<PaintController>getController();
		paintController.setStage(stage);

		stage.setMaximized(true);
		stage.show();
		stage.toFront();

		initialize();
	}

	@After
	public void tearDown() throws Exception {
		FxToolkit.hideStage();
	}

	private void initialize() {
		drawingChooser = lookup("#drawingChooser").query();
		drawingPanel = lookup("#drawingPanel").query();
		historyPanel = lookup("#historyPanel").query();
		propertyPanelWrapper = lookup("#propertyPanelWrapper").query();
		drawingPanelWrapper = lookup("#drawingPanelWrapper").query();
		historyPanelWrapper = lookup("#historyPanelWrapper").query();
		textPanelWrapper = lookup("#textPanelWrapper").query();
		pane = lookup("#canvas").query();
	}

	@Test
	public void testToggleButtonDrawingMode() {
		for (String drawingMode : drawingModes) {
			this.drawingMode = drawingMode;
			queryDrawingModeUiElements();
			assertDrawingModeFalse();
			clickOn(toggleButton);
			assertDrawingModeTrue();
		}
	}

	@Test
	public void testRadioMenuItemDrawingMode() {
		for (String drawingMode : drawingModes) {
			this.drawingMode = drawingMode;
			queryDrawingModeUiElements();
			assertDrawingModeFalse();
			clickOn("Edit").clickOn("Drawing Mode").clickOn(drawingMode);
			assertDrawingModeTrue();
		}
	}

	@Test
	public void testDrawingChooserDrawingMode() {
		for (String drawingMode : drawingModes) {
			this.drawingMode = drawingMode;
			queryDrawingModeUiElements();
			assertDrawingModeFalse();
			clickOn(drawingChooser).clickOn(drawingMode);
			assertDrawingModeTrue();
		}
	}

	private void queryDrawingModeUiElements() {
		toggleButton = lookup("#" + drawingMode.toLowerCase() + "ToggleButton").query();
		radioMenuItem = getRadioMenuItem(drawingMode);
	}

	private void assertDrawingModeFalse() {
		assertFalse(toggleButton.isSelected());
		assertFalse(radioMenuItem.isSelected());
		assertFalse(drawingChooser.getSelectionModel().getSelectedItem().equals(drawingMode));
	}

	private void assertDrawingModeTrue() {
		assertTrue(toggleButton.isSelected());
		assertTrue(radioMenuItem.isSelected());
		assertTrue(drawingChooser.getSelectionModel().getSelectedItem().equals(drawingMode));
	}

	private RadioMenuItem getRadioMenuItem(String drawingMode) {
		MenuBar menuBar = (MenuBar) lookup("#menuBar").query();
		Menu editMenu = menuBar.getMenus().get(1);
		Menu drawingModeMenu = (Menu) editMenu.getItems().get(4);
		ObservableList<MenuItem> menuItems = drawingModeMenu.getItems();
		RadioMenuItem radioMenuItem = null;
		for (int i = 0; i < menuItems.size(); i++) {
			if (menuItems.get(i).getText().equals(drawingMode)) {
				radioMenuItem = (RadioMenuItem) menuItems.get(i);
				break;
			}
		}
		return radioMenuItem;
	}

	@Test
	public void testDrawShapes() {
		for (int i = 0; i < drawingModes.size(); i++) {
			this.drawingMode = drawingModes.get(i);
			clickOn("#" + drawingMode.toLowerCase() + "ToggleButton");
			drag(new Point2D(100, 100)).dropTo(new Point2D(200, 200));
			objWidth = lookup("#objWidth").query();
			objHeight = lookup("#objHeight").query();
			assertEquals(100, objWidth.getValue(), 1.0);
			assertEquals(100, objHeight.getValue(), 1.0);
			assertEquals(drawingMode, drawingPanel.getItems().get(0).toString());
			assertEquals(new CreateDrawing().toString(), historyPanel.getItems().get(2 * i).toString());

			clickOn("#clearButton");
		}
	}

	@Test
	public void testShowHidePanels() {
		assertTrue(propertyPanelWrapper.getChildren().isEmpty());
		assertTrue(textPanelWrapper.getChildren().isEmpty());
		assertFalse(drawingPanelWrapper.getChildren().isEmpty());
		assertFalse(historyPanelWrapper.getChildren().isEmpty());
		clickOn("#rectangleToggleButton");
		drag(new Point2D(200, 200)).dropTo(new Point2D(250, 250));
		assertFalse(propertyPanelWrapper.getChildren().isEmpty());
		assertFalse(textPanelWrapper.getChildren().isEmpty());

		clickOn("Panels").clickOn("Property Panel");
		clickOn("Panels").clickOn("Drawings Panel");
		clickOn("Panels").clickOn("History Panel");
		clickOn("Panels").clickOn("Text Panel");

		assertTrue(propertyPanelWrapper.getChildren().isEmpty());
		assertTrue(textPanelWrapper.getChildren().isEmpty());
		assertTrue(drawingPanelWrapper.getChildren().isEmpty());
		assertTrue(historyPanelWrapper.getChildren().isEmpty());

		clickOn("Panels").clickOn("Property Panel");
		clickOn("Panels").clickOn("Drawings Panel");
		clickOn("Panels").clickOn("History Panel");
		clickOn("Panels").clickOn("Text Panel");

		assertFalse(propertyPanelWrapper.getChildren().isEmpty());
		assertFalse(textPanelWrapper.getChildren().isEmpty());
		assertFalse(drawingPanelWrapper.getChildren().isEmpty());
		assertFalse(historyPanelWrapper.getChildren().isEmpty());
	}

	@Test
	public void testMoveLayers() {
		clickOn("#rectangleToggleButton");
		drag(new Point2D(200, 200)).dropTo(new Point2D(250, 250));
		clickOn("#lineToggleButton");
		drag(new Point2D(150, 150)).dropTo(new Point2D(300, 300));
		clickOn("#ellipseToggleButton");
		drag(new Point2D(100, 100)).dropTo(new Point2D(350, 350));
		ObservableList<Node> nodes = pane.getChildren();

		String rectangle = Rectangle.class.toString();
		String line = Line.class.toString();
		String ellipse = Ellipse.class.toString();

		clickOn("#moveBottomButton");
		assertOrder(nodes, ellipse, rectangle, line);
		clickOn("#moveBottomButton");
		assertOrder(nodes, ellipse, rectangle, line);
		clickOn("#moveUpButton");
		assertOrder(nodes, rectangle, ellipse, line);
		clickOn("#moveUpButton");
		assertOrder(nodes, rectangle, line, ellipse);
		clickOn("#moveUpButton");
		assertOrder(nodes, rectangle, line, ellipse);
		clickOn("#moveDownButton");
		assertOrder(nodes, rectangle, ellipse, line);
		clickOn("#moveDownButton");
		assertOrder(nodes, ellipse, rectangle, line);
		clickOn("#moveDownButton");
		assertOrder(nodes, ellipse, rectangle, line);
		clickOn("#moveTopButton");
		assertOrder(nodes, rectangle, line, ellipse);
		clickOn("#moveTopButton");
		assertOrder(nodes, rectangle, line, ellipse);
	}

	private void assertOrder(ObservableList<Node> nodes, String firstClass, String secondClass, String thirdClass) {
		assertEquals(firstClass, ((Pane) nodes.get(nodes.size() - 3)).getChildren().get(1).getClass().toString());
		assertEquals(secondClass, ((Pane) nodes.get(nodes.size() - 2)).getChildren().get(1).getClass().toString());
		assertEquals(thirdClass, ((Pane) nodes.get(nodes.size() - 1)).getChildren().get(1).getClass().toString());
	}

}
