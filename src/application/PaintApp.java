package application;

import java.util.logging.Logger;

import controller.PaintController;
import helpers.ExceptionHelper;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class PaintApp extends Application {

	private static final Logger logger = Logger.getLogger(PaintApp.class.getName());

	@Override
	public void start(Stage stage) {
		try {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/PaintView.fxml"));
			Scene scene = new Scene(fxmlLoader.load());

			stage.setScene(scene);
			stage.setTitle("fPaintUS");
			stage.getIcons().add(new Image("/image/fPaintUS.png"));

			PaintController paintController = fxmlLoader.<PaintController>getController();
			paintController.setStage(stage);

			stage.show();

			// Intercept exit window action, it's the PaintController that will manage the
			// exit
			Platform.setImplicitExit(false);
			stage.setOnCloseRequest((final WindowEvent event) -> {
				paintController.onExit();
				event.consume();
			});

		} catch (Exception e) {
			logger.severe(ExceptionHelper.getStackTraceAsString(e));
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
