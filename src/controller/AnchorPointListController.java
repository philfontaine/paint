package controller;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import model.anchor.AnchorPoint;
import model.anchor.AnchorPointList;
import observer.IObserver;

public class AnchorPointListController implements IObserver {

	private AnchorPointList list;
	private Pane pane = new Pane();

	public AnchorPointListController(AnchorPointList list) {
		pane.setMouseTransparent(true);
		this.list = list;
		draw();
		list.getModel().register(this);
	}

	public Pane getPane() {
		return pane;
	}

	public AnchorPointList getList() {
		return list;
	}

	private void addAnchorToPane(AnchorPoint a) {
		Rectangle drawing = new Rectangle();
		drawing.relocate(a.getPosition().getX() - list.getAnchorSize() / 2,
				a.getPosition().getY() - list.getAnchorSize() / 2);
		drawing.setWidth(10);
		drawing.setHeight(10);
		drawing.setFill(Color.WHITE);
		drawing.setStroke(Color.BLACK);
		drawing.setStrokeWidth(1);
		pane.getChildren().add(drawing);
	}

	private void draw() {
		pane.getChildren().clear();
		for (AnchorPoint a : this.list.getList()) {
			addAnchorToPane(a);
		}
	}

	@Override
	public void update() {
		draw();
	}

}
