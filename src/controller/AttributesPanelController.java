package controller;

import javafx.scene.control.Spinner;
import javafx.scene.layout.Pane;
import list.ICurrentObserver;
import model.DrawingModel;
import model.DrawingModelList;
import model.Point2D;
import model.SceneContext;
import model.command.CommandList;
import model.command.EditDrawingHeight;
import model.command.EditDrawingRotation;
import model.command.EditDrawingTopLeft;
import model.command.EditDrawingWidth;
import observer.IObserver;

public class AttributesPanelController implements IObserver, ICurrentObserver<DrawingModel> {

	private DrawingModel model;

	private CommandList commandList;
	private DrawingModelList drawingModels;

	private Pane canvas;
	private Spinner<Double> ySpinner;
	private Spinner<Double> wSpinner;
	private Spinner<Double> hSpinner;
	private Spinner<Double> xSpinner;
	private Spinner<Double> rSpinner;

	public AttributesPanelController(Pane c, Spinner<Double> x, Spinner<Double> y, Spinner<Double> w, Spinner<Double> h,
			Spinner<Double> r, CommandList commandList, DrawingModelList drawingModels) {

		canvas = c;
		xSpinner = x;
		ySpinner = y;
		wSpinner = w;
		hSpinner = h;
		rSpinner = r;

		this.commandList = commandList;
		this.drawingModels = drawingModels;
		this.drawingModels.registerCurrentObserver(this);

		initialize();
	}

	private void initialize() {
		initializeXSpinner();
		initializeYSpinner();
		initializeWSpinner();
		initializeHSpinner();
		initializeRSpinner();
	}

	private void initializeXSpinner() {
		xSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null && model.getTopLeftX() != newValue) {
				Point2D oldTopLeft = model.getTopLeft();
				Point2D newTopLeft = new Point2D(newValue, oldTopLeft.getY());
				commandList.add(new EditDrawingTopLeft(oldTopLeft, newTopLeft,
						new SceneContext(canvas.getWidth(), canvas.getHeight()), drawingModels.getCurrentIndex()));
			}
		});
	}

	private void initializeYSpinner() {
		ySpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null && model.getTopLeftY() != newValue) {
				Point2D oldTopLeft = model.getTopLeft();
				Point2D newTopLeft = new Point2D(oldTopLeft.getX(), newValue);
				commandList.add(new EditDrawingTopLeft(oldTopLeft, newTopLeft,
						new SceneContext(canvas.getWidth(), canvas.getHeight()), drawingModels.getCurrentIndex()));
			}
		});

	}

	private void initializeWSpinner() {
		wSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null && model.getWidth() != newValue) {
				commandList.add(
						new EditDrawingWidth(oldValue, newValue, new SceneContext(canvas.getWidth(), canvas.getHeight()), drawingModels.getCurrentIndex()));
			}
		});
	}

	private void initializeHSpinner() {
		hSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null && model.getHeight() != newValue) {
				commandList.add(
						new EditDrawingHeight(oldValue, newValue, new SceneContext(canvas.getWidth(), canvas.getHeight()), drawingModels.getCurrentIndex()));
			}
		});
	}

	private void initializeRSpinner() {
		rSpinner.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null && model.getRotation() != newValue) {
				commandList.add(new EditDrawingRotation(oldValue, newValue, drawingModels.getCurrentIndex()));
			}
		});
	}

	@Override
	public void update() {
		setAttributesPanel();
	}

	private void setAttributesPanel() {
		if (model == null) {
			xSpinner.getValueFactory().setValue(0.0);
			ySpinner.getValueFactory().setValue(0.0);
			wSpinner.getValueFactory().setValue(0.0);
			hSpinner.getValueFactory().setValue(0.0);
			rSpinner.getValueFactory().setValue(0.0);
		} else {
			xSpinner.getValueFactory().setValue(model.getTopLeftX());
			ySpinner.getValueFactory().setValue(model.getTopLeftY());
			wSpinner.getValueFactory().setValue(model.getWidth());
			hSpinner.getValueFactory().setValue(model.getHeight());
			rSpinner.getValueFactory().setValue(model.getRotation());
		}
	}

	@Override
	public void onCurrentChange(DrawingModel o) {
		if (model != null) {
			model.unregister(this);
		}
		if (drawingModels.getCurrent() != null) {
			model = drawingModels.getCurrent();
			model.register(this);
		} else {
			model = null;
		}
		setAttributesPanel();
	}

}
