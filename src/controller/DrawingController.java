package controller;

import helpers.ColorHelper;
import helpers.DrawingTextModelHelper;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.DrawingModel;
import model.DrawingTextModel;
import model.anchor.AnchorPointList;
import observer.IObserver;

public abstract class DrawingController implements IObserver {

	private Rectangle selectionBoxBackground = new Rectangle();
	private Rectangle selectionBoxDashed = new Rectangle();
	private boolean isSelected = false;
	protected DrawingModel model;
	protected Node node;
	private Text text = new Text();
	protected Pane pane = new Pane();

	private AnchorPointListController anchorListController;

	public DrawingController(DrawingModel m) {
		pane.setMouseTransparent(true);
		model = m;
		m.register(this);
		m.getDrawingOptions().register(this);
		selectionBoxDashed.getStrokeDashArray().add(7.0);
		addToPane(text);
		anchorListController = new AnchorPointListController(m.getAnchorList());
	}
	
	public boolean isVisible() {
		return model.isVisible();
	}

	@Override
	public void update() {
		draw();
	}

	public Node getDrawing() {
		return pane;
	}

	public Node getNode() {
		return node;
	}

	public DrawingModel getModel() {
		return model;
	}

	public AnchorPointList getAnchorPoints() {
		return (anchorListController.getList());
	}

	public void setModel(DrawingModel m) {
		model = m;
	}

	protected abstract void drawDrawing();

	public void draw() {
		if (isVisible()) {
			show();
			drawDrawing();
			drawText();
			drawSelectionBox();
		} else {
			hide();
		}
		
	}

	public boolean isSelected() {
		return isSelected;
	}

	private void addToPane(Node n) {
		if (!pane.getChildren().contains(n)) {
			pane.getChildren().add(n);
		}
		n.toFront();
	}

	private void removeFromPane(Node n) {
		if (pane.getChildren().contains(n)) {
			pane.getChildren().remove(n);
		}
	}

	public void show() {
		addToPane(node);
		addToPane(selectionBoxBackground);
		addToPane(selectionBoxDashed);
		addToPane(anchorListController.getPane());
	}

	public void hide() {
		removeFromPane(node);
		removeFromPane(selectionBoxBackground);
		removeFromPane(selectionBoxDashed);
		removeFromPane(anchorListController.getPane());
	}

	public void drawSelectionBox() {

		if (model.isSelected()) {

			double strokeWidth = 1.0;
			double topLeftX = Math.floor(model.getTopLeftX() - model.getBrushSize() / 2);
			double topLeftY = Math.floor(model.getTopLeftY() - model.getBrushSize() / 2);
			double boxWidth = model.getWidth() + model.getBrushSize();
			double boxHeight = model.getHeight() + model.getBrushSize();
			double rotation = -model.getRotation();

			selectionBoxBackground.setStroke(Color.BLACK);
			selectionBoxBackground.setFill(Color.TRANSPARENT);
			selectionBoxBackground.setRotate(rotation);

			selectionBoxBackground.setWidth(boxWidth);
			selectionBoxBackground.setHeight(boxHeight);
			selectionBoxBackground.setStrokeWidth(strokeWidth);
			selectionBoxBackground.relocate(topLeftX, topLeftY);
			selectionBoxBackground.toFront();

			selectionBoxDashed.setStroke(Color.RED);
			selectionBoxDashed.setFill(Color.TRANSPARENT);
			selectionBoxDashed.setRotate(rotation);

			selectionBoxDashed.setWidth(boxWidth);
			selectionBoxDashed.setHeight(boxHeight);
			selectionBoxDashed.setStrokeWidth(strokeWidth);
			selectionBoxDashed.relocate(topLeftX, topLeftY);
			selectionBoxDashed.toFront();

			addToPane(selectionBoxBackground);
			addToPane(selectionBoxDashed);
			addToPane(anchorListController.getPane());
		} else {
			removeFromPane(selectionBoxBackground);
			removeFromPane(selectionBoxDashed);
			removeFromPane(anchorListController.getPane());
		}
	}

	public void relocateShape(Shape shape) {
		double halfBrushSize = model.getDrawingOptions().getBrushSize() / 2.0;
		shape.relocate(model.getTopLeftX() - halfBrushSize, model.getTopLeftY() - halfBrushSize);
	}

	private void drawText() {
		DrawingTextModel m = model.getTextModel();
		text.setCache(true);
		text.setText(m.getText());
		text.setFill(ColorHelper.toJavaFXColor(m.getColor()));
		text.setTextAlignment(DrawingTextModelHelper.alignmentConverter(m.getAlignment()));
		text.setTextOrigin(DrawingTextModelHelper.vPosConverter(m.getVerticalPosition()));
		text.toFront();
		text.setFont(Font.font(null, DrawingTextModelHelper.fontWeightConverter(m.getFontWeight()), m.getSize()));
		Bounds bounds = text.getLayoutBounds();
		text.setX(model.getTopLeftX() + model.getWidth() / 2 - bounds.getWidth() / 2);
		text.setY(model.getTopLeftY() + model.getHeight() / 2 - bounds.getDepth() / 2);
	}
}
