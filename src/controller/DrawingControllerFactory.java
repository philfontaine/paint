package controller;

import controller.shape.BrushController;
import controller.shape.EllipseController;
import controller.shape.ImageController;
import controller.shape.LineController;
import controller.shape.ManController;
import controller.shape.RectangleController;
import controller.shape.DrawingGroupController;
import controller.shape.UdesController;
import model.DrawingModel;
import model.SceneContext;
import model.shape.BrushModel;
import model.shape.DrawingGroupModel;
import model.shape.EllipseModel;
import model.shape.ImageModel;
import model.shape.LineModel;
import model.shape.ManModel;
import model.shape.RectangleModel;
import model.shape.UdesModel;

public class DrawingControllerFactory {
	private static final DrawingControllerFactory INSTANCE = new DrawingControllerFactory();

	private DrawingControllerFactory() {
	}

	public static DrawingControllerFactory getInstance() {
		return INSTANCE;
	}

	public DrawingController getController(DrawingModel drawingModel, SceneContext context) {
		DrawingController drawingController = null;
		if (drawingModel instanceof BrushModel) {
			drawingController = new BrushController((BrushModel) drawingModel);
		} else if (drawingModel instanceof EllipseModel) {
			drawingController = new EllipseController((EllipseModel) drawingModel);
		} else if (drawingModel instanceof RectangleModel) {
			drawingController = new RectangleController((RectangleModel) drawingModel);
		} else if (drawingModel instanceof LineModel) {
			drawingController = new LineController((LineModel) drawingModel);
		} else if (drawingModel instanceof UdesModel) {
			drawingController = new UdesController((UdesModel) drawingModel);
		} else if (drawingModel instanceof ManModel) {
			drawingController = new ManController((ManModel) drawingModel);
		} else if (drawingModel instanceof DrawingGroupModel) {
			drawingController = new DrawingGroupController((DrawingGroupModel) drawingModel, context);
		} else if (drawingModel instanceof ImageModel) {
			drawingController = new ImageController((ImageModel) drawingModel);
		}
		return drawingController;
	}
}
