package controller;

import javafx.scene.layout.Pane;
import list.ICurrentObserver;
import list.ILayeredListObserver;
import list.ObservableLayeredList;
import model.DrawingModel;
import model.SceneContext;

public class DrawingControllerList extends ObservableLayeredList<DrawingController>
		implements ILayeredListObserver<DrawingModel>, ICurrentObserver<DrawingModel> {

	private Pane canvas;

	public DrawingControllerList(Pane canvas) {
		this.canvas = canvas;
	}

	@Override
	public void onAdd(DrawingModel o) {
		DrawingController c = DrawingControllerFactory.getInstance().getController(o,
				new SceneContext(canvas.getWidth(), canvas.getHeight()));
		this.add(c);
		c.draw();
		addToPane(c);
	}

	@Override
	public void onRemove(DrawingModel o) {
		DrawingController c = findControllerRelatedToModel(o);
		if (c != null) {
			this.remove(c);
			removeFromPane(c);
		}
	}

	@Override
	public void onClear() {
		for (DrawingController c : content) {
			removeFromPane(c);
		}
		content.clear();
	}

	@Override
	public void onSwap(DrawingModel o1, DrawingModel o2) {
		DrawingController c1 = findControllerRelatedToModel(o1);
		DrawingController c2 = findControllerRelatedToModel(o2);
		this.swap(c1, c2);
		refreshPane();
	}

	@Override
	public void onCurrentChange(DrawingModel o) {
		this.current = findControllerRelatedToModel(o);
	}

	private DrawingController findControllerRelatedToModel(DrawingModel m) {
		for (DrawingController c : content) {
			if (c.getModel() == m) {
				return c;
			}
		}
		return null;
	}

	private void addToPane(DrawingController c) {
		if (!canvas.getChildren().contains(c.getDrawing())) {
			canvas.getChildren().add(c.getDrawing());
		}
	}

	private void removeFromPane(DrawingController c) {
		if (canvas.getChildren().contains(c.getDrawing())) {
			canvas.getChildren().remove(c.getDrawing());
		}
	}

	/**
	 * Method used to print the layered drawing in order of appeareance in the list
	 */
	private void refreshPane() {
		for (DrawingController c : content) {
			if (c.isVisible()) {
				c.show();
				c.getDrawing().toFront();
			} else {
				c.hide();
			}
		}
	}

	@Override
	public void onInsertAtIndex(DrawingModel o, int index) {
		DrawingController c = DrawingControllerFactory.getInstance().getController(o,
				new SceneContext(canvas.getWidth(), canvas.getHeight()));
		this.insertAtIndex(c, index);
		c.draw();
		addToPane(c);
		refreshPane();
	}
}
