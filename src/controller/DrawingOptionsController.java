package controller;

import helpers.ColorHelper;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Spinner;
import list.ICurrentObserver;
import model.DrawingModel;
import model.DrawingModelList;
import model.DrawingOptionsModel;
import model.command.CommandList;
import model.command.EditDrawingBrushColor;
import model.command.EditDrawingBrushSize;
import model.command.EditDrawingFillColor;
import model.shape.DrawingGroupModel;
import observer.IObserver;

public class DrawingOptionsController implements IObserver, ICurrentObserver<DrawingModel> {
	private Spinner<Double> brushSize;
	private ColorPicker brushColorPicker;
	private ColorPicker fillColorPicker;

	private CommandList commandList;
	private DrawingModelList drawingModels;

	private DrawingOptionsModel drawingOptionsModel;

	public DrawingOptionsController(Spinner<Double> brushSize, ColorPicker brushColorPicker,
			ColorPicker fillColorPicker, CommandList commandList, DrawingModelList controllerList,
			DrawingOptionsModel initialModel) {
		this.brushSize = brushSize;
		this.brushColorPicker = brushColorPicker;
		this.fillColorPicker = fillColorPicker;

		this.commandList = commandList;
		this.drawingModels = controllerList;
		this.drawingModels.registerCurrentObserver(this);

		this.drawingOptionsModel = initialModel;

		initialize();
	}

	private void initialize() {
		initializeBrushSize();
		initializeBrushColor();
		initializeFillColor();
		updatePanel();
	}

	private void initializeBrushSize() {
		brushSize.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (drawingOptionsModel.getBrushSize() != newValue) {
				if (drawingModels.getCurrent() != null) {
					commandList.add(new EditDrawingBrushSize(oldValue, newValue, drawingModels.getCurrentIndex()));
				} else {
					drawingOptionsModel.setBrushSize(newValue);
				}
			}
		});

	}

	private void initializeBrushColor() {
		brushColorPicker.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (!drawingOptionsModel.getBrushColor().isEqualTo(ColorHelper.fromJavaFXColor(newValue))) {
				if (drawingModels.getCurrent() != null) {
					commandList.add(new EditDrawingBrushColor(ColorHelper.fromJavaFXColor(oldValue),
							ColorHelper.fromJavaFXColor(newValue), drawingModels.getCurrentIndex()));
				} else {
					drawingOptionsModel.setBrushColor(ColorHelper.fromJavaFXColor(brushColorPicker.getValue()));
				}
			}
		});

	}

	private void initializeFillColor() {
		fillColorPicker.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (!drawingOptionsModel.getFillColor().isEqualTo(ColorHelper.fromJavaFXColor(newValue))) {
				if (drawingModels.getCurrent() != null) {
					commandList.add(new EditDrawingFillColor(ColorHelper.fromJavaFXColor(oldValue),
							ColorHelper.fromJavaFXColor(newValue), drawingModels.getCurrentIndex()));
				} else {
					drawingOptionsModel.setFillColor(ColorHelper.fromJavaFXColor(fillColorPicker.getValue()));
				}
			}
		});
	}

	public DrawingOptionsModel getModel() {
		return drawingOptionsModel;
	}

	public DrawingOptionsModel getNewModel() {
		return new DrawingOptionsModel(drawingOptionsModel);
	}

	@Override
	public void update() {
		updatePanel();
	}

	private void updatePanel() {
		brushSize.getValueFactory().setValue(drawingOptionsModel.getBrushSize());
		brushColorPicker.setValue(ColorHelper.toJavaFXColor(drawingOptionsModel.getBrushColor()));
		fillColorPicker.setValue(ColorHelper.toJavaFXColor(drawingOptionsModel.getFillColor()));
	}

	@Override
	public void onCurrentChange(DrawingModel newCurrent) {
		drawingOptionsModel.unregister(this);

		if (newCurrent != null && !(newCurrent instanceof DrawingGroupModel)) {
			drawingOptionsModel = newCurrent.getDrawingOptions();
		} else {
			drawingOptionsModel = new DrawingOptionsModel(drawingOptionsModel);
		}

		drawingOptionsModel.register(this);
		updatePanel();
	}

}
