package controller;

import javafx.scene.control.ListView;
import list.ICurrentObserver;
import list.ILayeredListObserver;
import model.DrawingModel;
import model.DrawingModelList;

public class DrawingPanelController implements ILayeredListObserver<DrawingModel>, ICurrentObserver<DrawingModel> {
	private ListView<DrawingModel> panel;
	private DrawingModelList drawingModels;

	public DrawingPanelController(ListView<DrawingModel> panel, DrawingModelList drawingModels) {
		this.panel = panel;
		this.drawingModels = drawingModels;
		initialize();
	}

	private void initialize() {
		drawingModels.registerListObserver(this);
		drawingModels.registerCurrentObserver(this);
		updatePanel();

		panel.getSelectionModel().selectedItemProperty().addListener((obs, oldValue, newValue) -> {
			if (newValue != null && drawingModels.getCurrent() != newValue) {
				drawingModels.setCurrent(newValue);
				drawingModels.selectCurrent();
			}
		});
	}

	public void updatePanel() {
		panel.getItems().clear();
		for (DrawingModel model : drawingModels.getList()) {
			panel.getItems().add(model);
		}
		panel.getSelectionModel().select(drawingModels.getCurrent());
	}

	@Override
	public void onAdd(DrawingModel o) {
		updatePanel();
	}

	@Override
	public void onRemove(DrawingModel o) {
		updatePanel();
	}

	@Override
	public void onClear() {
		updatePanel();
	}

	@Override
	public void onSwap(DrawingModel o1, DrawingModel o2) {
		updatePanel();
	}

	@Override
	public void onInsertAtIndex(DrawingModel o, int index) {
		updatePanel();
	}

	@Override
	public void onCurrentChange(DrawingModel newCurrent) {
		updatePanel();
	}
}
