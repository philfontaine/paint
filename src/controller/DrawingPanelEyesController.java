package controller;

import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import list.ICurrentObserver;
import list.ILayeredListObserver;
import model.DrawingModel;
import model.DrawingModelList;
import view.EyeButton;

public class DrawingPanelEyesController implements ILayeredListObserver<DrawingModel>, ICurrentObserver<DrawingModel>{
	private ListView<Button> panel;
	private DrawingModelList drawingModels;

	public DrawingPanelEyesController(ListView<Button> panel, DrawingModelList drawingModels) {
		this.panel = panel;
		this.drawingModels = drawingModels;
		initialize();
	}

	private void initialize() {
		drawingModels.registerListObserver(this);
		drawingModels.registerCurrentObserver(this);
		updatePanel();
	}

	public void updatePanel() {
		panel.getItems().clear();
		for (DrawingModel model : drawingModels.getList()) {
			EyeButton button = new EyeButton(model);
			panel.getItems().add(button);
		}
	}

	@Override
	public void onAdd(DrawingModel o) {
		updatePanel();
	}

	@Override
	public void onRemove(DrawingModel o) {
		updatePanel();
	}

	@Override
	public void onClear() {
		updatePanel();
	}

	@Override
	public void onSwap(DrawingModel o1, DrawingModel o2) {
		updatePanel();
	}

	@Override
	public void onInsertAtIndex(DrawingModel o, int index) {
		updatePanel();
	}

	@Override
	public void onCurrentChange(DrawingModel newCurrent) {
		updatePanel();
	}
}
