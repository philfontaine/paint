package controller;

import controller.MagneticGridController;
import javafx.scene.control.Spinner;
import javafx.scene.layout.Pane;

public class GridPanelController{
	
	private Pane canvas;
	
	private MagneticGridController grid;
	
	private Spinner<Double> width;
	private Spinner<Double> height;
	
	public GridPanelController(MagneticGridController grid, Spinner<Double> width, Spinner<Double> height, Pane canvas) {
		this.grid = grid;
		this.width = width;
		this.height = height;
		this.canvas = canvas;
		initialize();
	}
	
	private void initialize() {
		width.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (grid!=null && grid.getGrid().getScaleWidth() != newValue) {
				grid.updateWidth(newValue.intValue(), canvas.getWidth(), canvas.getHeight());		
			}
		});
		height.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (grid != null && grid.getGrid().getScaleHeight() != newValue) {
				grid.updateHeight(newValue.intValue(), canvas.getWidth(), canvas.getHeight());		
			}
		});
	}
}
