package controller;

import javafx.scene.control.ListView;
import model.command.Command;
import model.command.CommandList;
import observer.IObserver;

public class HistoryPanelController implements IObserver {

	private ListView<Command> panel;
	private CommandList commandList;

	public HistoryPanelController(ListView<Command> panel, CommandList commandList) {
		this.panel = panel;
		this.commandList = commandList;
		initialize();
	}

	private void initialize() {
		commandList.register(this);
		update();

		panel.getSelectionModel().selectedIndexProperty().addListener((obs, oldValue, newValue) -> {
			if ((int) newValue >= 0 && (int) oldValue != -1) {
				int delta = (int) newValue - (int) oldValue;
				if (delta > 0) {
					commandList.redo(delta);
				} else if (delta < 0) {
					commandList.undo(-delta);
				}
			}
		});
	}

	@Override
	public void update() {
		panel.getItems().clear();
		for (Command command : commandList.getCommandList()) {
			panel.getItems().add(command);
		}
		panel.getSelectionModel().select(commandList.getLastIndex());
	}
}
