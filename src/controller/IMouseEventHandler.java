package controller;

import model.Point2D;
import model.SceneContext;

public interface IMouseEventHandler {
	public void mousePressed(Point2D location, SceneContext context, MagneticGridController gridController);

	public void mouseDragged(Point2D location, SceneContext context, MagneticGridController gridController);

	public void mouseReleased(SceneContext context);
}
