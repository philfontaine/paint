package controller;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import model.MagneticGrid;

public class MagneticGridController {

	private MagneticGrid grid;
	private Pane pane;

	public MagneticGridController(int scaleWidth, int scaleHeight) {
		grid = new MagneticGrid(scaleWidth, scaleHeight);
		pane = new Pane();
	}

	public Pane getPane() {
		return pane;
	}

	public MagneticGrid getGrid() {
		return grid;
	}

	public void setWidth(int width) {
		grid.setScaleWidth(width);
	}

	public void setHeight(int height) {
		grid.setScaleHeight(height);
	}

	public boolean isActive() {
		return grid.getIsActive();
	}

	public void changeIsActive(double windowWidth, double windowHeight) {
		grid.changeIsActive();
		if (isActive()) {
			draw(windowWidth, windowHeight);
		} else {
			undraw();
		}
	}

	public void draw(double windowWidth, double windowHeight) {
		undraw();
		for (int x = grid.getScaleWidth(); x < windowWidth; x += grid.getScaleWidth()) {
			Line line = new Line();
			line.setStroke(Color.DARKGRAY);
			line.setStartX(x);
			line.setStartY(0);
			line.setEndX(x);
			line.setEndY(windowHeight);
			pane.getChildren().add(line);
		}
		for (int y = grid.getScaleHeight(); y < windowHeight; y += grid.getScaleHeight()) {
			Line line = new Line();
			line.setStartX(0);
			line.setStartY(y);
			line.setEndX(windowWidth);
			line.setEndY(y);
			pane.getChildren().add(line);
		}
	}

	public void undraw() {
		pane.getChildren().clear();
	}

	public void updateWidth(int newValue, double canvasWidth, double canvasHeight) {
		grid.setScaleWidth(newValue);
		draw(canvasWidth, canvasHeight);
	}

	public void updateHeight(int newValue, double canvasWidth, double canvasHeight) {
		grid.setScaleHeight(newValue);
		draw(canvasWidth, canvasHeight);
	}
}
