package controller;

import java.util.ArrayList;

public class ObservableController {
	private ArrayList<IObserverController> observers = new ArrayList<>();

	public void register(IObserverController observer) {
		if (!observers.contains(observer)) {
			observers.add(observer);
		}
	}

	public void unregister(IObserverController observer) {
		if (observers.contains(observer)) {
			observers.remove(observer);
		}
	}

	protected void notifyObservers() {
		for (IObserverController observer : observers) {
			observer.updateController();
		}
	}
}
