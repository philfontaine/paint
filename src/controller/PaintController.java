package controller;

import java.util.ArrayList;
import java.util.Optional;

import controller.opensave.OpenSaveManager;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.DrawingAttributesModel;
import model.DrawingMode;
import model.DrawingModel;
import model.DrawingModelFactory;
import model.DrawingModelList;
import model.Point2D;
import model.SceneContext;
import model.WindowModel;
import model.anchor.AnchorPoint;
import model.command.Clear;
import model.command.Command;
import model.command.CommandList;
import model.command.CreateDrawing;
import model.command.DeleteSelectedDrawings;
import model.command.DragAnchorPoint;
import model.command.GroupDrawings;
import model.command.Memorize;
import model.command.MoveBottomCommand;
import model.command.MoveDownCommand;
import model.command.MoveTopCommand;
import model.command.MoveUpCommand;
import model.command.Replicate;
import model.command.UngroupDrawings;
import view.PanelHider;

public class PaintController {

	private Stage stage;
	@FXML
	private BorderPane mainPane;
	@FXML
	private Pane canvas;

	@FXML
	private Spinner<Double> brushSize;
	@FXML
	private Spinner<Double> objXTop;
	@FXML
	private Spinner<Double> objYTop;
	@FXML
	private Spinner<Double> objWidth;
	@FXML
	private Spinner<Double> objHeight;
	@FXML
	private Spinner<Double> objRotation;
	@FXML
	private Spinner<Double> grWidth;
	@FXML
	private Spinner<Double> grHeight;
	@FXML
	private ColorPicker brushColorPicker;
	@FXML
	private ColorPicker fillColorPicker;
	@FXML
	private ComboBox<String> drawingChooser;
	@FXML
	private ToggleButton selectionButton;
	@FXML
	private RadioMenuItem brushRadioMenuItem;
	@FXML
	private RadioMenuItem rectangleRadioMenuItem;
	@FXML
	private RadioMenuItem ellipseRadioMenuItem;
	@FXML
	private RadioMenuItem lineRadioMenuItem;
	@FXML
	private RadioMenuItem udesRadioMenuItem;
	@FXML
	private RadioMenuItem manRadioMenuItem;
	@FXML
	private RadioMenuItem textRadioMenuItem;
	@FXML
	private ToggleButton brushToggleButton;
	@FXML
	private ToggleButton rectangleToggleButton;
	@FXML
	private ToggleButton ellipseToggleButton;
	@FXML
	private ToggleButton lineToggleButton;
	@FXML
	private ToggleButton udesToggleButton;
	@FXML
	private ToggleButton manToggleButton;
	@FXML
	private ToggleButton textToggleButton;

	@FXML
	private TextArea objText;
	@FXML
	private Spinner<Double> fontSize;
	@FXML
	private ColorPicker fontColorPicker;
	@FXML
	private ComboBox<String> objTextAlignment;
	@FXML
	private ComboBox<String> objTextVPos;
	@FXML
	private ComboBox<String> objFontWeight;

	@FXML
	private BorderPane panels;
	@FXML
	private ToolBar propertyPanel;
	@FXML
	private ToolBar textPanel;
	@FXML
	private ToolBar gridPanel;
	@FXML
	private ListView<DrawingModel> drawingPanel;
	@FXML
	private ListView<Button> drawingPanelEyes;
	@FXML
	private ListView<Command> historyPanel;

	@FXML
	private GridPane panelWrapper;
	@FXML
	private Pane propertyPanelWrapper;
	@FXML
	private Pane drawingPanelWrapper;
	@FXML
	private Pane historyPanelWrapper;
	@FXML
	private Pane textPanelWrapper;
	@FXML
	private Pane gridPanelWrapper;

	@FXML
	private ScrollPane panelSubWrapper;
	@FXML
	private VBox historySubWrapper;
	@FXML
	private VBox drawingSubWrapper;
	@FXML
	private VBox gridSubWrapper;
	@FXML
	private VBox propertySubWrapper;
	@FXML
	private VBox textSubWrapper;

	@FXML
	private Label mouseX;
	@FXML
	private Label mouseY;

	private String drawingMode = DrawingMode.BRUSH;

	private boolean saved = true;

	private DrawingModelList drawingModels;

	private SelectionGroupController selectionController;
	private DrawingOptionsController drawingOptionsController;

	private WindowModel windowModel = new WindowModel();
	private DrawingAttributesModel drawingAttributesModel = new DrawingAttributesModel();

	private CommandList commandList;

	private MagneticGridController gridController;

	private AnchorPoint draggedAnchor = null;
	private Point2D dragStartPoint;

	private OpenSaveManager openSaveManager;

	private boolean isDrawing = false;

	private double panelWidth = 175;
	private PanelsManager panelsManager;

	public void initialize() {
		drawingModels = new DrawingModelList();
		commandList = new CommandList(drawingModels);

		DrawingControllerList controllers = new DrawingControllerList(canvas);
		drawingModels.registerListObserver(controllers);
		drawingModels.registerCurrentObserver(controllers);

		drawingAttributesModel.initialize();
		drawingMode = drawingAttributesModel.getDrawingMode();

		openSaveManager = new OpenSaveManager(canvas, commandList, drawingModels);

		initializeControllers();
		initializeListeners();
		initializePanelManager();

		// We set the default color for both colorPickers
		drawingChooser.setValue(drawingMode);

	}

	private void initializeControllers() {
		drawingOptionsController = new DrawingOptionsController(brushSize, brushColorPicker, fillColorPicker,
				commandList, drawingModels, drawingAttributesModel.getDrawingOptionsModel());

		gridController = new MagneticGridController(60, 60);

		canvas.getChildren().add(gridController.getPane());
		new AttributesPanelController(canvas, objXTop, objYTop, objWidth, objHeight, objRotation, commandList,
				drawingModels);

		new TextPanelController(commandList, drawingModels, objText, fontSize, fontColorPicker, objTextAlignment,
				objTextVPos, objFontWeight);

		new DrawingPanelController(drawingPanel, drawingModels);

		new DrawingPanelEyesController(drawingPanelEyes, drawingModels);

		new GridPanelController(gridController, grWidth, grHeight, canvas);

		new HistoryPanelController(historyPanel, commandList);

		selectionController = new SelectionGroupController(new SceneContext(canvas.getWidth(), canvas.getHeight()),
				drawingModels, commandList);

		canvas.getChildren().add(selectionController.getSelectionBoxController().getDrawing());

	}

	private void initializeListeners() {
		canvas.widthProperty().addListener((obs, newValue, oldValue) -> {
			if (gridController.isActive()) {
				gridController.draw(canvas.getWidth(), canvas.getHeight());
			}
		});

		canvas.heightProperty().addListener((obs, newValue, oldValue) -> {
			if (gridController.isActive()) {
				gridController.draw(canvas.getWidth(), canvas.getHeight());
			}
		});

		canvas.setOnMouseMoved(this::adjustMouseLocationLabels);

		drawingChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldMode, String newMode) {
				synchronizeDrawingMode(newMode);
			}
		});

		mainPane.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.DELETE) {
				deleteSelectedItems();
			}
		});
	}

	private void initializePanelManager() {
		GridPane.setRowIndex(drawingPanelEyes, 0);
		GridPane.setColumnIndex(drawingPanelEyes, 0);
		GridPane.setRowIndex(drawingPanel, 0);
		GridPane.setColumnIndex(drawingPanel, 1);
		panelSubWrapper.setPrefWidth(panelWidth);
		drawingPanelEyes.setPrefWidth(50);
		PanelHider panelsHider = new PanelHider(panelWrapper, panelSubWrapper);
		PanelHider propertyHider = new PanelHider(propertyPanelWrapper, propertySubWrapper);
		PanelHider gridHider = new PanelHider(gridPanelWrapper, gridSubWrapper);
		PanelHider textHider = new PanelHider(textPanelWrapper, textSubWrapper);
		PanelHider drawingHider = new PanelHider(drawingPanelWrapper, drawingSubWrapper);
		PanelHider historyHider = new PanelHider(historyPanelWrapper, historySubWrapper);
		panelsManager = new PanelsManager(panelsHider, propertyHider, gridHider, textHider, drawingHider, historyHider,
				drawingModels);

	}

	private void adjustMouseLocationLabels(MouseEvent event) {
		mouseX.setText("" + (int) Math.floor(event.getX()));
		mouseY.setText("" + (int) Math.floor(event.getY()));
	}

	private void synchronizeDrawingMode(String drawingMode) {
		selectionController.disableSelection();
		this.drawingMode = drawingMode;
		drawingChooser.setValue(drawingMode);
		switch (drawingMode) {
		case DrawingMode.BRUSH:
			brushRadioMenuItem.setSelected(true);
			brushToggleButton.setSelected(true);
			break;
		case DrawingMode.RECTANGLE:
			rectangleRadioMenuItem.setSelected(true);
			rectangleToggleButton.setSelected(true);
			break;
		case DrawingMode.ELLIPSE:
			ellipseRadioMenuItem.setSelected(true);
			ellipseToggleButton.setSelected(true);
			break;
		case DrawingMode.LINE:
			lineRadioMenuItem.setSelected(true);
			lineToggleButton.setSelected(true);
			break;
		case DrawingMode.UDES:
			udesRadioMenuItem.setSelected(true);
			udesToggleButton.setSelected(true);
			break;
		case DrawingMode.MAN:
			manRadioMenuItem.setSelected(true);
			manToggleButton.setSelected(true);
			break;
		case DrawingMode.TEXT:
			textRadioMenuItem.setSelected(true);
			textToggleButton.setSelected(true);
			break;
		default:
			break;
		}
	}

	public void onDrawBrush() {
		synchronizeDrawingMode(DrawingMode.BRUSH);
	}

	public void onDrawRectangle() {
		synchronizeDrawingMode(DrawingMode.RECTANGLE);
	}

	public void onDrawEllipse() {
		synchronizeDrawingMode(DrawingMode.ELLIPSE);
	}

	public void onDrawLine() {
		synchronizeDrawingMode(DrawingMode.LINE);
	}

	public void onDrawUdeS() {
		synchronizeDrawingMode(DrawingMode.UDES);
	}

	public void onDrawMan() {
		synchronizeDrawingMode(DrawingMode.MAN);
	}

	public void onDrawText() {
		synchronizeDrawingMode(DrawingMode.TEXT);
	}

	public void onClear() {
		clear();
	}

	public void clear() {
		commandList.add(new Clear());
		canvas.getChildren().clear();
		saved = true;
	}

	public void onUndo() {
		commandList.undo();
	}

	public void onRedo() {
		commandList.redo();
	}

	public void onPropertyPanelHide() {
		panelsManager.changeState(PanelType.PROPERTY);
	}

	public void onDrawingPanelHide() {
		panelsManager.changeState(PanelType.DRAWING);
	}

	public void onHistoryPanelHide() {
		panelsManager.changeState(PanelType.HISTORY);
	}

	public void onTextPanelHide() {
		panelsManager.changeState(PanelType.TEXT);
	}

	public void onGridPanelHide() {
		panelsManager.changeState(PanelType.GRID);
	}

	public void onOpen() {
		if (promptUserToSave()) {
			newPaint();
			openSaveManager.open();
		}
	}

	public void onSave() {
		drawingModels.deselectAll();
		if (gridController.isActive()) {
			gridController.changeIsActive(canvas.getWidth(), canvas.getHeight());
		}
		openSaveManager.save();
	}

	public void onImportImage() {
		openSaveManager.open();
	}

	public void onNewPaint() {
		if (promptUserToSave()) {
			newPaint();
		}
	}

	public void onExit() {
		if (promptUserToSave()) {
			store();
			Platform.exit();
		}
	}

	public void onMoveTop() {
		if (drawingModels.getCurrent() != drawingModels.getLast()) {
			commandList.add(new MoveTopCommand());
		}
	}

	public void onMoveBottom() {
		if (drawingModels.getCurrent() != drawingModels.getFirst()) {
			commandList.add(new MoveBottomCommand());
		}
	}

	public void onMoveUp() {
		if (drawingModels.getCurrent() != drawingModels.getLast()) {
			commandList.add(new MoveUpCommand());
		}
	}

	public void onMoveDown() {
		if (drawingModels.getCurrent() != drawingModels.getFirst()) {
			commandList.add(new MoveDownCommand());
		}
	}

	public void onDelete() {
		deleteSelectedItems();
	}

	public void setStage(Stage stage) {
		this.stage = stage;
		windowModel.initialize();
		boolean isMaximized = windowModel.isMaximized();
		if (isMaximized) {
			stage.setMaximized(windowModel.isMaximized());
		} else {
			stage.setX(windowModel.getX());
			stage.setY(windowModel.getY());
			stage.setWidth(windowModel.getWidth());
			stage.setHeight(windowModel.getHeight());
		}

	}

	public void mousePressed(MouseEvent e) {
		Point2D eventLocation = new Point2D(e.getX(), e.getY());
		draggedAnchor = drawingModels.anchorUnderMouse(eventLocation);
		if (draggedAnchor != null) {
			dragStartPoint = new Point2D(draggedAnchor.getPosition().getX(), draggedAnchor.getPosition().getY());
		} else {

			SceneContext context = new SceneContext(canvas.getWidth(), canvas.getHeight());

			if (selectionController.isSelectionEnabled()) {
				selectionController.mousePressed(eventLocation, context, gridController);
			} else {
				drawingModels.deselectAll();
				if (gridController.isActive()) {
					eventLocation = gridController.getGrid().nearestPoint(eventLocation, context);
				}
				DrawingModel drawingModel = DrawingModelFactory.getInstance().getModel(eventLocation, context,
						drawingMode, drawingOptionsController.getNewModel());

				commandList.add(new CreateDrawing(drawingModel));
				isDrawing = true;

				saved = false;
			}

		}
	}

	public void mouseDragged(MouseEvent e) {
		adjustMouseLocationLabels(e);

		Point2D eventLocation = new Point2D(e.getX(), e.getY());
		SceneContext context = new SceneContext(canvas.getWidth(), canvas.getHeight());
		if (draggedAnchor != null) {
			if (gridController.isActive()) {
				eventLocation = gridController.getGrid().nearestPoint(eventLocation, context);
			}
			draggedAnchor.setPosition(eventLocation, context);
			setPaneMinSize(drawingModels.getCurrent());
		} else if (selectionController.isSelectionEnabled()) {
			selectionController.mouseDragged(eventLocation, context, gridController);
		} else {
			drawingModels.getCurrent().update(eventLocation, context);
			setPaneMinSize(drawingModels.getCurrent());
		}
	}

	public void mouseReleased() {
		if (draggedAnchor != null) {
			commandList.add(new DragAnchorPoint(dragStartPoint,
					new Point2D(draggedAnchor.getPosition().getX(), draggedAnchor.getPosition().getY()),
					draggedAnchor.getPosSet(), drawingModels.indexOf(draggedAnchor.getModel())));
			draggedAnchor = null;
		} else if (isDrawing) {
			drawingModels.getCurrent().finish();
			drawingModels.selectCurrent();
			isDrawing = false;
		} else {
			SceneContext context = new SceneContext(canvas.getWidth(), canvas.getHeight());
			selectionController.mouseReleased(context);
		}
	}

	private void setPaneMinSize(DrawingModel m) {
		double minWidth = m.getTopLeftX() + m.getWidth();
		if (minWidth > canvas.getMinWidth()) {
			canvas.setMinWidth(minWidth);
		}
		double minHeight = m.getTopLeftY() + m.getHeight();
		if (minHeight > canvas.getMinHeight()) {
			canvas.setMinHeight(minHeight);
		}
	}

	private void groupSelectedItems() {
		ArrayList<Integer> selected = (ArrayList<Integer>) drawingModels.getSelectedItemsIndexes();
		if (!selected.isEmpty()) {
			commandList.add(new GroupDrawings(selected));
		}
	}

	private void ungroupSelectedItems() {
		ArrayList<Integer> selected = (ArrayList<Integer>) drawingModels.getSelectedItemsIndexes();
		if (!selected.isEmpty()) {
			commandList.add(new UngroupDrawings(selected));
		}
	}

	private void deleteSelectedItems() {
		ArrayList<Integer> selected = (ArrayList<Integer>) drawingModels.getSelectedItemsIndexes();
		if (!selected.isEmpty()) {
			commandList.add(new DeleteSelectedDrawings(selected));
		}
	}

	private void store() {
		windowModel.set(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight(), stage.isMaximized());
		drawingAttributesModel.set(drawingOptionsController.getModel(), drawingMode);
	}

	/**
	 * 
	 * @return true: Action was not cancelled; false: Action was cancelled
	 * 
	 */
	private boolean promptUserToSave() {
		boolean success = false;
		if (saved) {
			success = true;
		} else {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Exit Confirmation");
			alert.setContentText("Do you want to save your changes?");

			ButtonType save = new ButtonType("Save");
			ButtonType dontSave = new ButtonType("Don't Save");
			ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

			alert.getButtonTypes().setAll(save, dontSave, cancel);

			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent()) {
				if (result.get() == save) {
					openSaveManager.save();
					success = true;
				} else if (result.get() == dontSave) {
					success = true;
				}
			}
		}
		return success;
	}

	private void newPaint() {
		drawingModels.clear();
		commandList.setList(new ArrayList<>());
		saved = true;
	}

	public void onGrid() {
		gridController.changeIsActive(canvas.getWidth(), canvas.getHeight());
		if (gridController.isActive()) {
			panelsManager.show(PanelType.GRID);
		} else {
			panelsManager.hide(PanelType.GRID);
		}
	}

	public void onSelection() {
		selectionController.enableSelection();
	}

	public void onGroup() {
		groupSelectedItems();
	}

	public void onUngroup() {
		ungroupSelectedItems();
	}

	public void onMemorize() {
		commandList.add(new Memorize(drawingModels.indexOf(drawingModels.getCurrent())));
	}

	public void onReplicate() {
		commandList.add(new Replicate());
	}

}
