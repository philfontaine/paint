package controller;

public enum PanelType {
	PROPERTY, GRID, TEXT, DRAWING, HISTORY
}
