package controller;

import list.ICurrentObserver;
import model.DrawingModel;
import model.DrawingModelList;
import view.PanelHider;

public class PanelsManager implements ICurrentObserver<DrawingModel> {
	private PanelHider panelsHider;
	private PanelHider propertyHider;
	private PanelHider gridHider;
	private PanelHider textHider;
	private PanelHider drawingHider;
	private PanelHider historyHider;

	public PanelsManager(PanelHider panelsHider, PanelHider propertyHider, PanelHider gridHider, PanelHider textHider,
			PanelHider drawingHider, PanelHider historyHider, DrawingModelList drawingModels) {

		drawingModels.registerCurrentObserver(this);
		this.panelsHider = panelsHider;
		this.propertyHider = propertyHider;
		this.gridHider = gridHider;
		this.textHider = textHider;
		this.drawingHider = drawingHider;
		this.historyHider = historyHider;

		show(PanelType.HISTORY);
		show(PanelType.DRAWING);

	}

	private PanelHider select(PanelType type) {
		switch (type) {
		case PROPERTY:
			return propertyHider;
		case GRID:
			return gridHider;
		case TEXT:
			return textHider;
		case DRAWING:
			return drawingHider;
		case HISTORY:
			return historyHider;
		default:
			return null;
		}
	}

	public void hide(PanelType type) {
		PanelHider panelHider = select(type);
		if (panelHider != null) {
			panelHider.hide();
		}
		if (!isAPanelVisible()) {
			hide();
		}
	}

	public void show(PanelType type) {
		if (!isAPanelVisible()) {
			show();
		}
		PanelHider panelHider = select(type);
		if (panelHider != null) {
			panelHider.show();
		}
	}

	public void changeState(PanelType type) {
		if (!isAPanelVisible()) {
			show();
		}
		PanelHider panelHider = select(type);
		if (panelHider != null) {
			panelHider.changeState();
		}
		if (!isAPanelVisible()) {
			hide();
		}
	}

	public void show() {
		panelsHider.show();
	}

	public void hide() {
		panelsHider.hide();
	}

	private boolean isAPanelVisible() {
		return (propertyHider.isVisible() || gridHider.isVisible() || textHider.isVisible() || drawingHider.isVisible()
				|| historyHider.isVisible());
	}

	@Override
	public void onCurrentChange(DrawingModel newCurrent) {
		if (newCurrent == null) {
			hide(PanelType.PROPERTY);
			hide(PanelType.TEXT);
		} else {
			show(PanelType.PROPERTY);
			show(PanelType.TEXT);
		}

	}
}
