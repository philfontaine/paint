package controller;

import controller.shape.SelectionRectangleController;
import model.Color;
import model.DrawingModel;
import model.DrawingModelList;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.command.CommandList;
import model.command.EditDrawingTopLeft;
import model.shape.RectangleModel;

public class SelectionGroupController implements IMouseEventHandler {

	private SelectionRectangleController selectionBoxController;
	private boolean hasDragged = false;
	private Point2D startPoint;

	private boolean isDraggingDrawing = false;
	private DrawingModel draggingDrawing = null;
	private Point2D topLeftCurrentPointOnStartDragging = null;

	private DrawingModelList drawingModels;

	private CommandList commandList;

	private boolean selectionEnabled;

	public SelectionGroupController(SceneContext context, DrawingModelList drawingModels, CommandList commandList) {
		selectionBoxController = new SelectionRectangleController(new RectangleModel(new Point2D(), context,
				new DrawingOptionsModel(1.0, new Color(150, 150, 150, 1), new Color(240, 240, 240, 0.25))));
		this.drawingModels = drawingModels;
		this.commandList = commandList;
	}

	public SelectionRectangleController getSelectionBoxController() {
		return selectionBoxController;
	}

	public void enableSelection() {
		this.selectionEnabled = true;
	}

	public void disableSelection() {
		this.selectionEnabled = false;
	}

	@Override
	public void mousePressed(Point2D location, SceneContext context, MagneticGridController gridController) {
		startPoint = new Point2D(location.getX(), location.getY());
		if (selectionEnabled) {
			draggingDrawing = getFirstItemUnderClickLocation();
			if (draggingDrawing != null && draggingDrawing.isSelected()) {
				isDraggingDrawing = true;
				topLeftCurrentPointOnStartDragging = new Point2D(draggingDrawing.getTopLeftX(),
						draggingDrawing.getTopLeftY());
				drawingModels.deselectAll();
				drawingModels.setCurrent(draggingDrawing);
				drawingModels.selectCurrent();
			} else {
				drawingModels.deselectAll();
				drawingModels.setCurrent(null);
				startSelection(location, context);
			}
		}
	}

	@Override
	public void mouseDragged(Point2D location, SceneContext context, MagneticGridController gridController) {
		if (selectionEnabled) {
			if (isDraggingDrawing) {
				double newTopLeftX = topLeftCurrentPointOnStartDragging.getX() + location.getX() - startPoint.getX();
				double newTopLeftY = topLeftCurrentPointOnStartDragging.getY() + location.getY() - startPoint.getY();
				Point2D newTopLeft = new Point2D(newTopLeftX, newTopLeftY);
				if (gridController.isActive()) {
					newTopLeft = gridController.getGrid().nearestPoint(newTopLeft, context);
				}
				draggingDrawing.updateTopLeft(newTopLeft, context);
			} else {
				dragSelection(location, context);
				hasDragged = true;
			}
		}
	}

	@Override
	public void mouseReleased(SceneContext context) {
		if (selectionEnabled) {
			if (isDraggingDrawing) {
				Point2D newTopLeft = draggingDrawing.getTopLeft();
				Point2D oldTopLeft = new Point2D(topLeftCurrentPointOnStartDragging);
				if (!newTopLeft.sameAs(oldTopLeft)) {
					commandList.add(new EditDrawingTopLeft(oldTopLeft, newTopLeft, context, drawingModels.indexOf(draggingDrawing)));
				}
				drawingModels.deselectAll();
				drawingModels.setCurrent(draggingDrawing);
				drawingModels.selectCurrent();
				isDraggingDrawing = false;
				draggingDrawing = null;
				topLeftCurrentPointOnStartDragging = null;
			} else {
				selectItems();
			}

		}
		reset();
	}

	private void reset() {
		hideSelectionBox();
		startPoint = null;
		hasDragged = false;
	}

	private void startSelection(Point2D startPoint, SceneContext context) {
		this.startPoint = startPoint;
		selectionBoxController.getModel().updateInitialPoint(startPoint, context);
		selectionBoxController.getModel().updateFinalPoint(startPoint, context);
		showSelectionBox();
	}

	private void dragSelection(Point2D location, SceneContext context) {
		selectionBoxController.getModel().updateFinalPoint(location, context);
	}

	private void showSelectionBox() {
		selectionBoxController.show();
		selectionBoxController.getDrawing().toFront();
	}

	private void hideSelectionBox() {
		selectionBoxController.hide();
	}

	public boolean isSelectionEnabled() {
		return selectionEnabled;
	}

	private void selectItems() {
		if (hasDragged) {
			selectItemsUnderSelectionBox();
		} else {
			selecFirstItemUnderClickLocation();
		}

	}

	private void selecFirstItemUnderClickLocation() {
		DrawingModel model = getFirstItemUnderClickLocation();
		if (model != null) {
			model.select();
			drawingModels.setCurrent(model);
		}
	}

	private DrawingModel getFirstItemUnderClickLocation() {
		for (int i = drawingModels.size() - 1; i >= 0; i--) {
			DrawingModel model = drawingModels.get(i);
			if (model.contains(startPoint)) {
				return (model);
			}
		}
		return (null);
	}

	private void selectItemsUnderSelectionBox() {
		for (DrawingModel model : drawingModels.getList()) {
			if (model.isWithin(selectionBoxController.getModel())) {
				model.select();
			}
		}
	}
}
