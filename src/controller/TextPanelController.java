package controller;

import helpers.ColorHelper;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextArea;
import list.ICurrentObserver;
import model.DrawingModel;
import model.DrawingModelList;
import model.command.CommandList;
import model.command.EditText;
import model.command.EditTextAlignment;
import model.command.EditTextColor;
import model.command.EditTextFontWeight;
import model.command.EditTextSize;
import model.command.EditTextVPos;
import observer.IObserver;

public class TextPanelController implements IObserver, ICurrentObserver<DrawingModel> {

	private DrawingModel model;

	private CommandList commandList;
	private DrawingModelList drawingModels;

	private TextArea textArea;
	private Spinner<Double> fontSize;
	private ColorPicker fontColorPicker;
	private ComboBox<String> objTextAlignment;
	private ComboBox<String> objTextVPos;
	private ComboBox<String> objFontWeight;

	public TextPanelController(CommandList commandList, DrawingModelList drawingModels, TextArea t,
			Spinner<Double> fontSize, ColorPicker fontColorPicker, ComboBox<String> objTextAlignment,
			ComboBox<String> objTextVPos, ComboBox<String> objFontWeight) {

		textArea = t;
		this.fontSize = fontSize;
		this.fontColorPicker = fontColorPicker;
		this.objTextAlignment = objTextAlignment;
		this.objTextVPos = objTextVPos;
		this.objFontWeight = objFontWeight;

		this.commandList = commandList;
		this.drawingModels = drawingModels;
		this.drawingModels.registerCurrentObserver(this);

		initialize();
	}

	private void initialize() {
		initializeTextArea();
		initializeFontSize();
		initializeFontColor();
		initializeAlignment();
		initializeVPos();
		initializeFontWeight();
	}

	private void initializeTextArea() {
		textArea.textProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null) {
				if (!model.getTextModel().getText().equals(newValue)) {
					if (drawingModels.getCurrent() != null) {
						commandList.add(new EditText(oldValue, newValue, drawingModels.getCurrentIndex()));
					} else {
						model.getTextModel().setText(newValue);
					}
				}
				model.updateText(model.getTextModel());
			}
		});
	}

	private void initializeFontSize() {
		fontSize.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null) {
				if (model != null && model.getTextModel().getSize() != newValue) {
					if (drawingModels.getCurrent() != null) {
						commandList.add(new EditTextSize(oldValue, newValue, drawingModels.getCurrentIndex()));
					} else {
						model.getTextModel().setSize(newValue);
					}
				}
				model.updateText(model.getTextModel());
			}
		});
	}

	private void initializeFontColor() {
		fontColorPicker.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null) {
				if (!model.getTextModel().getColor().isEqualTo(ColorHelper.fromJavaFXColor(newValue))) {
					if (drawingModels.getCurrent() != null) {
						commandList.add(new EditTextColor(ColorHelper.fromJavaFXColor(oldValue),
								ColorHelper.fromJavaFXColor(newValue), drawingModels.getCurrentIndex()));
					} else {
						model.getTextModel().setColor(ColorHelper.fromJavaFXColor(fontColorPicker.getValue()));
					}
				}
				model.updateText(model.getTextModel());
			}
		});
	}

	private void initializeAlignment() {
		objTextAlignment.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null) {
				if (!model.getTextModel().getAlignment().equals(newValue)) {
					if (drawingModels.getCurrent() != null) {
						commandList.add(new EditTextAlignment(oldValue, newValue, drawingModels.getCurrentIndex()));
					} else {
						model.getTextModel().setAlignment(newValue);
					}
				}
				model.updateText(model.getTextModel());
			}
		});
	}

	private void initializeVPos() {
		objTextVPos.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null) {
				if (!model.getTextModel().getVerticalPosition().equals(newValue)) {
					if (drawingModels.getCurrent() != null) {
						commandList.add(new EditTextVPos(oldValue, newValue, drawingModels.getCurrentIndex()));
					} else {
						model.getTextModel().setVerticalPosition(newValue);
					}
				}
				model.updateText(model.getTextModel());
			}
		});
	}

	private void initializeFontWeight() {
		objFontWeight.valueProperty().addListener((obs, oldValue, newValue) -> {
			if (model != null) {
				if (!model.getTextModel().getFontWeight().equals(newValue)) {
					if (drawingModels.getCurrent() != null) {
						commandList.add(new EditTextFontWeight(oldValue, newValue, drawingModels.getCurrentIndex()));
					} else {
						model.getTextModel().setFontWeight(newValue);
					}
				}
				model.updateText(model.getTextModel());
			}
		});
	}

	@Override
	public void onCurrentChange(DrawingModel newCurrent) {
		if (model != null) {
			model.getTextModel().unregister(this);
		}
		if (drawingModels.getCurrent() != null) {
			model = drawingModels.getCurrent();
			model.getTextModel().register(this);
		} else {
			model = null;
		}
		setTextPanel();
	}

	@Override
	public void update() {
		setTextPanel();
	}

	private void setTextPanel() {
		if (model == null) {
			textArea.textProperty().setValue(null);
			fontSize.getValueFactory().setValue(null);
			fontColorPicker.setValue(null);
			objTextAlignment.setValue(null);
			objTextVPos.setValue(null);
			objFontWeight.setValue(null);
		} else {
			textArea.textProperty().setValue(model.getTextModel().getText());
			fontSize.getValueFactory().setValue(model.getTextModel().getSize());
			fontColorPicker.setValue(ColorHelper.toJavaFXColor(model.getTextModel().getColor()));
			objTextAlignment.setValue(model.getTextModel().getAlignment());
			objTextVPos.setValue(model.getTextModel().getVerticalPosition());
			objFontWeight.setValue(model.getTextModel().getFontWeight());
		}
	}

}
