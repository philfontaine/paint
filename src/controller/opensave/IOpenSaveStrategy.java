package controller.opensave;

import java.io.File;

public interface IOpenSaveStrategy {
	public void open(File file);

	public void save(File file);
}
