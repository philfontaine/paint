package controller.opensave;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import helpers.CompareImages;
import helpers.ExceptionHelper;
import helpers.ExtensionHelper;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.command.CommandList;
import model.command.ImportImageCommand;
import model.shape.ImageModel;

public class OpenSaveImage implements IOpenSaveStrategy {

	private static final Logger logger = Logger.getLogger(OpenSaveImage.class.getName());

	private Pane canvas;
	private CommandList commandList;

	public OpenSaveImage(Pane canvas, CommandList commandList) {
		this.canvas = canvas;
		this.commandList = commandList;
	}

	@Override
	public void open(File file) {
		try {
			BufferedImage bufferedImage = ImageIO.read(file);
			ImageModel imgModel = new ImageModel(new Point2D(0, 0), new SceneContext(canvas.getWidth(), canvas.getHeight()),
					new DrawingOptionsModel(), bufferedImage);
			commandList.add(new ImportImageCommand(imgModel));

			ImageView imageViewImported = new ImageView(SwingFXUtils.toFXImage(bufferedImage, null));
			Image imgImported = imageViewImported.getImage();

			Image logoUdeS = new Image("image/Logo.png");
			Image noSignal = new Image("image/noSignal.png");

			if (CompareImages.areSame(imgImported, logoUdeS)) {
				String sound = "sound/Sound.mp3";
				Media media = new Media(new File("src/" + sound).toURI().toString());
				MediaPlayer mediaPlayer = new MediaPlayer(media);
				mediaPlayer.play();

				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information Dialog");
				alert.setHeaderText("Picture detection");
				alert.setContentText("Congratulations! You imported the UdeS logo successfully!");
				alert.showAndWait();

			}else if (CompareImages.areSame(imgImported, noSignal)) {
				String sound = "sound/Sound1.mp3";
				Media media = new Media(new File("src/" + sound).toURI().toString());
				MediaPlayer mediaPlayer = new MediaPlayer(media);
				mediaPlayer.play();

				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Information Dialog");
				alert.setHeaderText("Picture detection");
				alert.setContentText("Congratulations! You imported the no signal picture successfully!");
				alert.showAndWait();

			}

		} catch (IOException e) {
			logger.severe(ExceptionHelper.getStackTraceAsString(e));
		}

	}

	@Override
	public void save(File file) {
		try {
			WritableImage writableImage = new WritableImage((int) canvas.getWidth(), (int) canvas.getHeight());
			canvas.snapshot(null, writableImage);
			RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
			ImageIO.write(renderedImage, ExtensionHelper.getExtension(file), file);
		} catch (IOException ex) {
			logger.severe(ExceptionHelper.getStackTraceAsString(ex));
		}

	}

}
