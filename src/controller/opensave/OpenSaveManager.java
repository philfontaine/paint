package controller.opensave;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import model.DrawingModelList;
import model.command.CommandList;
import model.shape.EllipseModel;
import model.shape.LineModel;
import model.shape.RectangleModel;

public class OpenSaveManager {
	private LinkedHashMap<ExtensionFilter, IOpenSaveStrategy> map = new LinkedHashMap<>();

	private final List<String> supportedModelsSvg = Arrays.asList(RectangleModel.class.getName(),
			EllipseModel.class.getName(), LineModel.class.getName());

	private DrawingModelList drawingModelList;

	// Need those references because we will remove and put back these objects in
	// the map
	private ExtensionFilter extensionFilterSvg;
	private OpenSaveSvg openSaveSvg;

	public OpenSaveManager(Pane canvas, CommandList commandList, DrawingModelList drawingModelList) {
		this.drawingModelList = drawingModelList;
		map.put(new ExtensionFilter("Image files (*.png, *.jpg)", "*.png", "*.PNG", "*.jpg", "*.JPG"),
				new OpenSaveImage(canvas, commandList));
		map.put(new ExtensionFilter("XML files (*.xml)", "*.xml"), new OpenSaveXml(commandList));

		extensionFilterSvg = new ExtensionFilter("SVG files (*.svg)", "*.svg");
		openSaveSvg = new OpenSaveSvg(commandList, drawingModelList);

		map.put(extensionFilterSvg, openSaveSvg);
	}

	public void open() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(map.keySet());

		File file = fileChooser.showOpenDialog(null);

		if (file != null) {
			map.get(fileChooser.getSelectedExtensionFilter()).open(file);
		}
	}

	public void save() {
		updateMap();

		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(map.keySet());

		File file = fileChooser.showSaveDialog(null);

		if (file != null) {
			map.get(fileChooser.getSelectedExtensionFilter()).save(file);
		}
	}

	private void updateMap() {
		if (drawingModelList.getList().stream()
				.anyMatch(drawingModel -> !supportedModelsSvg.contains(drawingModel.getClass().getName()))) {
			map.remove(extensionFilterSvg);
		} else {
			map.putIfAbsent(extensionFilterSvg, openSaveSvg);
		}
	}
}
