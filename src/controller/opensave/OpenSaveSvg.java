package controller.opensave;

import java.io.File;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import helpers.ExceptionHelper;
import model.DrawingModel;
import model.DrawingModelList;
import model.command.CommandList;
import model.command.CreateDrawing;
import model.shape.svg.DrawingSvg;
import model.shape.svg.DrawingSvgList;

public class OpenSaveSvg implements IOpenSaveStrategy {

	private static final Logger logger = Logger.getLogger(OpenSaveSvg.class.getName());

	private CommandList commandList;
	private DrawingModelList drawingModelList;

	public OpenSaveSvg(CommandList commandList, DrawingModelList drawingModelList) {
		this.commandList = commandList;
		this.drawingModelList = drawingModelList;
	}

	@Override
	public void open(File file) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(DrawingSvgList.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			DrawingSvgList drawingSvgList = (DrawingSvgList) unmarshaller.unmarshal(file);

			for (DrawingSvg drawingSVG : drawingSvgList.list) {
				DrawingModel drawingModel = drawingSVG.getDrawingModel();
				if (drawingModel != null) {
					commandList.add(new CreateDrawing(drawingSVG.getDrawingModel()));
				}
			}

		} catch (Exception e) {
			logger.severe(ExceptionHelper.getStackTraceAsString(e));
		}

	}

	@Override
	public void save(File file) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(DrawingSvgList.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			DrawingSvgList drawingSVGList = new DrawingSvgList();
			for (DrawingModel drawingModel : drawingModelList.getList()) {
				DrawingSvg drawingSvg = drawingModel.getDrawingSvg();
				if (drawingSvg != null) {
					drawingSVGList.list.add(drawingModel.getDrawingSvg());
				}
			}

			jaxbMarshaller.marshal(drawingSVGList, file);
		} catch (Exception e) {
			logger.severe(ExceptionHelper.getStackTraceAsString(e));
		}
	}
}
