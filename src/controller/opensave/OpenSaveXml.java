package controller.opensave;

import java.io.File;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import helpers.ExceptionHelper;
import model.command.CommandList;

public class OpenSaveXml implements IOpenSaveStrategy {
	private static final Logger logger = Logger.getLogger(OpenSaveXml.class.getName());

	private CommandList commandList;

	public OpenSaveXml(CommandList commandList) {
		this.commandList = commandList;
	}

	@Override
	public void open(File file) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(CommandList.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			CommandList tempCommandList = (CommandList) unmarshaller.unmarshal(file);
			commandList.addAll(tempCommandList.getCommandList());
			commandList.execute();
		} catch (JAXBException e) {
			logger.severe(ExceptionHelper.getStackTraceAsString(e));
		}
	}

	@Override
	public void save(File file) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(CommandList.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(commandList, file);
		} catch (JAXBException e) {
			logger.severe(ExceptionHelper.getStackTraceAsString(e));
		}
	}

}
