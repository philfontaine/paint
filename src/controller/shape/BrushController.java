package controller.shape;

import java.util.ArrayList;

import controller.DrawingController;
import helpers.ColorHelper;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import model.Point2D;
import model.shape.BrushModel;

public class BrushController extends DrawingController {

	public BrushController(BrushModel m) {
		super(m);
		node = new Pane();
		draw();
	}

	@Override
	public void drawDrawing() {

		BrushModel brushModel = (BrushModel) model;
		Pane pane = (Pane) node;
		ArrayList<Point2D> points = (ArrayList<Point2D>) brushModel.getPoints();
		pane.getChildren().clear();
		Color brushColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getBrushColor());
		Color fillColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getFillColor());
		for (int i = 1; i < points.size(); i++) {
			Line line = new Line();
			line.setFill(fillColor);
			line.setStroke(brushColor);
			line.setStrokeWidth(model.getDrawingOptions().getBrushSize());

			line.setStartX(points.get(i - 1).getX());
			line.setStartY(points.get(i - 1).getY());
			line.setEndX(points.get(i).getX());
			line.setEndY(points.get(i).getY());

			pane.getChildren().add(line);
		}
		node.setRotate(-model.getRotation());
	}
}
