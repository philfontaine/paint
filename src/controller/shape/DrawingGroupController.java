package controller.shape;

import java.util.ArrayList;
import java.util.List;

import controller.DrawingController;
import controller.DrawingControllerFactory;
import javafx.scene.layout.Pane;
import model.DrawingModel;
import model.SceneContext;
import model.shape.DrawingGroupModel;

public class DrawingGroupController extends DrawingController {

	private ArrayList<DrawingController> children = new ArrayList<>();

	public DrawingGroupController(DrawingGroupModel m, SceneContext context) {
		super(m);
		initializePane(context);
		draw();
	}

	private void initializePane(SceneContext context) {
		Pane p = new Pane();

		DrawingGroupModel shapeList = (DrawingGroupModel) this.model;

		for (DrawingModel m : shapeList.getChildren()) {
			DrawingController controller = DrawingControllerFactory.getInstance().getController(m, context);
			children.add(controller);
			p.getChildren().add(controller.getDrawing());
		}

		node = p;
		pane.getChildren().add(node);
	}

	@Override
	public void drawDrawing() {
		for (DrawingController controller : children) {
			controller.draw();
		}
	}

	public List<DrawingController> getChildren() {
		return children;
	}
}
