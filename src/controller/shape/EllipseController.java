package controller.shape;

import controller.DrawingController;
import helpers.ColorHelper;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import model.shape.EllipseModel;

public class EllipseController extends DrawingController {

	public EllipseController(EllipseModel m) {
		super(m);
		node = new Ellipse();
		pane.getChildren().add(node);
	}

	@Override
	public void drawDrawing() {
		Ellipse ellipse = (Ellipse) node;
		Color brushColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getBrushColor());
		Color fillColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getFillColor());
		ellipse.setFill(fillColor);
		ellipse.setStroke(brushColor);
		ellipse.setStrokeWidth(model.getDrawingOptions().getBrushSize());

		ellipse.setRotate(-model.getRotation());

		ellipse.setRadiusX(model.getWidth() / 2);
		ellipse.setRadiusY(model.getHeight() / 2);

		this.relocateShape(ellipse);
	}
}
