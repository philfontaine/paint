package controller.shape;

import controller.DrawingController;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.ImageView;
import model.shape.ImageModel;

public class ImageController extends DrawingController {
	public ImageController(ImageModel m) {
		super(m);
		node = new ImageView(SwingFXUtils.toFXImage(m.getBufferedImage(), null));
		pane.getChildren().add(node);
	}

	@Override
	protected void drawDrawing() {
		ImageView imageView = (ImageView) node;
		imageView.setX(model.getTopLeftX());
		imageView.setY(model.getTopLeftY());
		imageView.setFitWidth(model.getWidth());
		imageView.setFitHeight(model.getHeight());
	}

}
