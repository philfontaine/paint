package controller.shape;

import controller.DrawingController;
import helpers.ColorHelper;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import model.shape.LineModel;

public class LineController extends DrawingController {

	public LineController(LineModel m) {
		super(m);
		node = new Line();
		pane.getChildren().add(node);
	}

	@Override
	public void drawDrawing() {
		Line line = (Line) node;

		Color brushColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getBrushColor());
		Color fillColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getFillColor());
		line.setFill(fillColor);
		line.setStroke(brushColor);
		line.setStrokeWidth(model.getDrawingOptions().getBrushSize());

		line.setStartX(model.getXi());
		line.setStartY(model.getYi());
		line.setEndX(model.getXf());
		line.setEndY(model.getYf());

		this.relocateShape(line);
		line.setRotate(-model.getRotation());
	}
}
