package controller.shape;

import controller.DrawingController;
import helpers.ColorHelper;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.HLineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.QuadCurveTo;
import javafx.scene.shape.VLineTo;
import model.shape.ManModel;

public class ManController extends DrawingController {

	Path path;
	Circle leftEye;
	Circle rightEye;

	public ManController(ManModel m) {
		super(m);
		Pane pane = new Pane();
		path = new Path();
		leftEye = new Circle();
		rightEye = new Circle();
		pane.getChildren().addAll(path, leftEye, rightEye);
		node = pane;
		this.pane.getChildren().add(node);
	}

	@Override
	public void drawDrawing() {

		double xi = model.getXi();
		double yi = model.getYi();
		double xf = model.getXf();
		double yf = model.getYf();
		double width = model.getWidth();
		double height = model.getHeight();

		double deltaX = width / 8;
		double deltaY = height / 10;

		/* Head points */
		double p1x = Math.min(xi, xf) + deltaX;
		double p1y = Math.min(yi, yf) + deltaY;

		double p2x = p1x + 2 * deltaX;
		double p3y = p1y - deltaY;
		double p4x = p1x + 4 * deltaX;
		double p5y = p1y;
		double p6x = p1x + 6 * deltaX;
		double p7x = p1x + 7 * deltaX;
		double p7y = p1y + deltaY;
		double p8y = p1y + 7 * deltaY;
		double p9x = p1x + 6 * deltaX;
		double p9y = p1y + 8 * deltaY;
		double p10x = p1x + 5 * deltaX;
		double p11y = p1y + 9 * deltaY;
		double p12x = p1x + deltaX;
		double p13y = p1y + 8 * deltaY;
		double p14x = p1x;
		double p15x = p1x - deltaX;
		double p15y = p1y + 7 * deltaY;
		double p16y = p1y + deltaY;

		/* Eye points */
		double leftEyeX = p1x + 1.5 * deltaX;
		double leftEyeY = p1y + 2.5 * deltaY;
		double rightEyeX = p1x + 4.5 * deltaX;
		double rightEyeY = leftEyeY;

		/* Mouth points */
		double mouth1x = p1x + deltaX;
		double mouth2x = p1x + 2 * deltaX;
		double mouth3x = p1x + 4 * deltaX;
		double mouth4x = p1x + 5 * deltaX;
		double mouthY = p1y + 5 * deltaY;

		/* Head outline */
		MoveTo moveTo = new MoveTo(p1x, p1y);

		HLineTo d12 = new HLineTo();
		d12.setX(p2x);

		VLineTo d23 = new VLineTo();
		d23.setY(p3y);

		HLineTo d34 = new HLineTo();
		d34.setX(p4x);

		VLineTo d45 = new VLineTo();
		d45.setY(p5y);

		HLineTo d56 = new HLineTo();
		d56.setX(p6x);

		QuadCurveTo d67 = new QuadCurveTo();
		d67.setX(p7x);
		d67.setY(p7y);
		d67.setControlX(p7x);
		d67.setControlY(p5y);

		VLineTo d78 = new VLineTo();
		d78.setY(p8y);

		QuadCurveTo d89 = new QuadCurveTo();
		d89.setX(p9x);
		d89.setY(p9y);
		d89.setControlX(p7x);
		d89.setControlY(p9y);

		HLineTo d910 = new HLineTo();
		d910.setX(p10x);

		VLineTo d1011 = new VLineTo();
		d1011.setY(p11y);

		HLineTo d1112 = new HLineTo();
		d1112.setX(p12x);

		VLineTo d1213 = new VLineTo();
		d1213.setY(p13y);

		HLineTo d1314 = new HLineTo();
		d1314.setX(p14x);

		QuadCurveTo d1415 = new QuadCurveTo();
		d1415.setX(p15x);
		d1415.setY(p15y);
		d1415.setControlX(p15x);
		d1415.setControlY(p13y);

		VLineTo d1516 = new VLineTo();
		d1516.setY(p16y);

		QuadCurveTo d161 = new QuadCurveTo();
		d161.setX(p1x);
		d161.setY(p1y);
		d161.setControlX(p15x);
		d161.setControlY(p1y);

		path.getElements().clear();
		path.getElements().addAll(moveTo, d12, d23, d34, d45, d56, d67, d78, d89, d910, d1011, d1112, d1213, d1314,
				d1415, d1516, d161);

		/* Mouth */
		MoveTo moveToMouth = new MoveTo(mouth1x, mouthY);

		QuadCurveTo mouthLeft = new QuadCurveTo();
		mouthLeft.setX(mouth2x);
		mouthLeft.setY(mouthY);
		mouthLeft.setControlX(leftEyeX);
		mouthLeft.setControlY(leftEyeY + 1.5 * deltaY);

		QuadCurveTo mouthUp = new QuadCurveTo();
		mouthUp.setX(mouth3x);
		mouthUp.setY(mouthY);
		mouthUp.setControlX(p12x + 2 * deltaX);
		mouthUp.setControlY(p11y - 2.5 * deltaY);

		QuadCurveTo mouthRight = new QuadCurveTo();
		mouthRight.setX(mouth4x);
		mouthRight.setY(mouthY);
		mouthRight.setControlX(rightEyeX);
		mouthRight.setControlY(rightEyeY + 1.5 * deltaY);

		QuadCurveTo mouthDown = new QuadCurveTo();
		mouthDown.setX(mouth1x);
		mouthDown.setY(mouthY);
		mouthDown.setControlX(p12x + 2 * deltaX);
		mouthDown.setControlY(p11y - deltaY);

		path.getElements().addAll(moveToMouth, mouthLeft, mouthUp, mouthRight, mouthDown);

		/* Eyes */
		leftEye.setCenterX(leftEyeX);
		leftEye.setCenterY(leftEyeY);
		leftEye.setRadius(Math.min(deltaX, deltaY) / 2);

		rightEye.setCenterX(rightEyeX);
		rightEye.setCenterY(rightEyeY);
		rightEye.setRadius(Math.min(deltaX, deltaY) / 2);

		Color brushColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getBrushColor());
		Color fillColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getFillColor());

		path.setFill(fillColor);
		path.setStroke(brushColor);
		node.setRotate(-model.getRotation());

		path.setStrokeWidth(model.getDrawingOptions().getBrushSize());

	}
}
