package controller.shape;

import controller.DrawingController;
import helpers.ColorHelper;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import model.shape.RectangleModel;

public class RectangleController extends DrawingController {

	public RectangleController(RectangleModel m) {
		super(m);
		node = new Rectangle();
		pane.getChildren().add(node);
	}

	@Override
	public void drawDrawing() {
		Rectangle rectangle = (Rectangle) node;

		Color brushColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getBrushColor());
		Color fillColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getFillColor());
		rectangle.setFill(fillColor);
		rectangle.setStroke(brushColor);
		rectangle.setStrokeWidth(model.getDrawingOptions().getBrushSize());

		rectangle.setRotate(-model.getRotation());
		rectangle.setWidth(model.getWidth());
		rectangle.setHeight(model.getHeight());
		this.relocateShape(rectangle);
	}
}
