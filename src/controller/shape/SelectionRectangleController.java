package controller.shape;

import javafx.scene.shape.Rectangle;
import model.shape.RectangleModel;

public class SelectionRectangleController extends RectangleController {
	public SelectionRectangleController(RectangleModel m) {
		super(m);
		Rectangle rect = (Rectangle) node;
		rect.getStrokeDashArray().add(7.0);
	}
}
