package controller.shape;

import controller.DrawingController;
import helpers.ColorHelper;
import javafx.scene.paint.Color;
import javafx.scene.shape.HLineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.QuadCurveTo;
import javafx.scene.shape.VLineTo;
import model.shape.UdesModel;

public class UdesController extends DrawingController {

	public UdesController(UdesModel m) {
		super(m);
		node = new Path();
		pane.getChildren().add(node);
	}

	@Override
	public void drawDrawing() {

		double xi = model.getXi();
		double yi = model.getYi();
		double xf = model.getXf();
		double yf = model.getYf();
		double width100 = model.getWidth() / 100;
		double height100 = model.getHeight() / 100;

		// point1
		double p1x = Math.max(xi, xf);
		double p1y = Math.min(yi, yf);

		// point2
		double p2x = p1x - 8 * width100;

		// point3
		double p3x = p1x - 25 * width100;
		double p3y = p1y + 21 * height100;

		// point4
		double p4x = p1x - 42 * width100;

		// point5
		double p5x = p1x - 79 * width100;

		// point6
		double p6y = p1y + 25 * height100;

		// point7
		double p7x = p1x - 50 * width100;
		double p7y = p1y + 54 * height100;

		// point8
		double p8x = p1x - 30 * width100;
		double p8y = p1y + 74 * height100;

		// point9
		double p9y = p1y + 94 * height100;

		// pointC10
		double p10x = p1x - 75 * width100;
		double p10y = p1y + 58 * height100;

		// point11
		// point12
		double p12x = p1x - Math.abs(xf - xi);
		double p12y = p1y + Math.abs(yf - yi);

		// point13
		double p13x = p12x + 8 * width100;

		// point14
		double p14x = p12x + 25 * width100;
		double p14y = p12y - 21 * height100;

		// point15
		double p15x = p12x + 42 * width100;

		// point16
		double p16x = p12x + 79 * width100;

		// point17
		double p17y = p12y - 25 * height100;

		// point18
		double p18x = p12x + 50 * width100;
		double p18y = p12y - 54 * height100;

		// point19
		double p19x = p12x + 30 * width100;
		double p19y = p12y - 74 * height100;

		// point20
		double p20y = p12y - 94 * height100;

		// point21
		double p21x = p12x + 75 * width100;
		double p21y = p12y - 58 * height100;

		// point22
		// point23

		// pointc1
		double pc1y = p1y + 53 * height100;
		// pointc1
		double pc2y = p12y - 53 * height100;

		// Path
		Path path = (Path) this.node;

		MoveTo moveTo = new MoveTo(p1x, p1y);

		// d12
		HLineTo d12 = new HLineTo();
		d12.setX(p2x);
		// d23
		QuadCurveTo d23 = new QuadCurveTo();
		d23.setX(p3x);
		d23.setY(p3y);
		d23.setControlX(p2x);
		d23.setControlY(p3y);
		// d34
		QuadCurveTo d34 = new QuadCurveTo();
		d34.setX(p4x);
		d34.setY(p1y);
		d34.setControlX(p3x + (p3x - p2x));
		d34.setControlY(p3y);
		// d45
		HLineTo d45 = new HLineTo();
		d45.setX(p5x);
		// d56
		QuadCurveTo d56 = new QuadCurveTo();
		d56.setX(p12x);
		d56.setY(p6y);
		d56.setControlX(p12x);
		d56.setControlY(p1y);
		// d67
		QuadCurveTo d67 = new QuadCurveTo();
		d67.setX(p7x);
		d67.setY(p7y);
		d67.setControlX(p12x);
		d67.setControlY(pc1y);
		// d78
		QuadCurveTo d78 = new QuadCurveTo();
		d78.setX(p8x);
		d78.setY(p8y);
		d78.setControlX(p8x);
		d78.setControlY(p7y);
		// d89
		QuadCurveTo d89 = new QuadCurveTo();
		d89.setX(p7x);
		d89.setY(p9y);
		d89.setControlX(p8x);
		d89.setControlY(p9y);
		// d910
		QuadCurveTo d910 = new QuadCurveTo();
		d910.setX(p10x);
		d910.setY(p10y);
		d910.setControlX(p7x);
		d910.setControlY(p10y);
		// d1011
		QuadCurveTo d1011 = new QuadCurveTo();
		d1011.setX(p12x);
		d1011.setY(p9y);
		d1011.setControlX(p12x);
		d1011.setControlY(p10y);
		// d1112
		VLineTo d1112 = new VLineTo();
		d1112.setY(p12y);

		// d1213
		HLineTo d1213 = new HLineTo();
		d1213.setX(p13x);
		// d1314
		QuadCurveTo d1314 = new QuadCurveTo();
		d1314.setX(p14x);
		d1314.setY(p14y);
		d1314.setControlX(p13x);
		d1314.setControlY(p14y);
		// d1415
		QuadCurveTo d1415 = new QuadCurveTo();
		d1415.setX(p15x);
		d1415.setY(p12y);
		d1415.setControlX(p14x + (p14x - p13x));
		d1415.setControlY(p14y);
		// d1516
		HLineTo d1516 = new HLineTo();
		d1516.setX(p16x);
		// d1617
		QuadCurveTo d1617 = new QuadCurveTo();
		d1617.setX(p1x);
		d1617.setY(p17y);
		d1617.setControlX(p1x);
		d1617.setControlY(p12y);
		// d1718
		QuadCurveTo d1718 = new QuadCurveTo();
		d1718.setX(p18x);
		d1718.setY(p18y);
		d1718.setControlX(p1x);
		d1718.setControlY(pc2y);
		// d1819
		QuadCurveTo d1819 = new QuadCurveTo();
		d1819.setX(p19x);
		d1819.setY(p19y);
		d1819.setControlX(p19x);
		d1819.setControlY(p18y);
		// d1920
		QuadCurveTo d1920 = new QuadCurveTo();
		d1920.setX(p18x);
		d1920.setY(p20y);
		d1920.setControlX(p19x);
		d1920.setControlY(p20y);
		// d2021
		QuadCurveTo d2021 = new QuadCurveTo();
		d2021.setX(p21x);
		d2021.setY(p21y);
		d2021.setControlX(p18x);
		d2021.setControlY(p21y);
		// d2122
		QuadCurveTo d2122 = new QuadCurveTo();
		d2122.setX(p1x);
		d2122.setY(p20y);
		d2122.setControlX(p1x);
		d2122.setControlY(p21y);
		// d221
		VLineTo d221 = new VLineTo();
		d221.setY(p1y);

		path.getElements().clear();
		path.getElements().addAll(moveTo, d12, d23, d34, d45, d56, d67, d78, d89, d910, d1011, d1112, d1213, d1314,
				d1415, d1516, d1617, d1718, d1819, d1920, d2021, d2122, d221);

		Color brushColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getBrushColor());
		Color fillColor = ColorHelper.toJavaFXColor(model.getDrawingOptions().getFillColor());
		path.setFill(fillColor);
		path.setStroke(brushColor);
		path.setStrokeWidth(model.getDrawingOptions().getBrushSize());
		path.setRotate(-model.getRotation());
	}
}
