package helpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Color;

public class ColorHelper {
	private ColorHelper() {
	}

	public static javafx.scene.paint.Color toJavaFXColor(Color c) {
		return javafx.scene.paint.Color.rgb(c.getR(), c.getG(), c.getB(), c.getOpacity());
	}

	public static Color fromJavaFXColor(javafx.scene.paint.Color c) {
		return new Color((int) (c.getRed() * 255), (int) (c.getGreen() * 255), (int) (c.getBlue() * 255),
				c.getOpacity());
	}

	public static Color fromSvgColor(String svgColor) {
		Color color = new Color();
		Pattern pattern = Pattern.compile("rgba\\( *([0-9]+), *([0-9]+), *([0-9]+), *([0-1]?\\.[0-9]+)\\)");
		Matcher matcher = pattern.matcher(svgColor);
		if (matcher.matches()) {
			color.setR(Integer.valueOf(matcher.group(1)));
			color.setG(Integer.valueOf(matcher.group(2)));
			color.setB(Integer.valueOf(matcher.group(3)));
			color.setOpacity(Double.valueOf(matcher.group(4)));
		}
		return color;
	}
}
