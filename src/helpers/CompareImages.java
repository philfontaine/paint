package helpers;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class CompareImages {

	private CompareImages() {
	}

	public static boolean areSame(Image imgImported, Image logoUdeS) {

		int imgImportedWidth = (int) imgImported.getWidth();
		int imgImportedHeight = (int) imgImported.getHeight();
		PixelReader pixelReaderImported = imgImported.getPixelReader();

		int logoWidth = (int) logoUdeS.getWidth();
		int logoHeight = (int) logoUdeS.getHeight();
		PixelReader pixelReaderLogo = logoUdeS.getPixelReader();

		if (imgImportedWidth == logoWidth && imgImportedHeight == logoHeight) {
			for (int i = 0; i < logoWidth; i++) {
				for (int j = 0; j < logoHeight; j++) {
					Color colorLogo = pixelReaderLogo.getColor(i, j);
					Color colorImported = pixelReaderImported.getColor(i, j);
					if (!colorLogo.equals(colorImported)) {
						return false;
					}
				}
			}
			return true;
		}
		return false;
	}
}
