package helpers;

import javafx.geometry.VPos;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import model.TextModelAlignment;
import model.TextModelFontWeight;
import model.TextModelVerticalPos;

public class DrawingTextModelHelper {
	private DrawingTextModelHelper() {

	}

	public static TextAlignment alignmentConverter(String alignment) {
		switch (alignment) {
		case TextModelAlignment.CENTER:
			return TextAlignment.CENTER;
		case TextModelAlignment.LEFT:
			return TextAlignment.LEFT;
		case TextModelAlignment.RIGHT:
			return TextAlignment.RIGHT;
		case TextModelAlignment.JUSTIFY:
			return TextAlignment.JUSTIFY;
		default:
			return TextAlignment.CENTER;
		}
	}

	public static FontWeight fontWeightConverter(String fontWeight) {
		switch (fontWeight) {
		case TextModelFontWeight.NORMAL:
			return FontWeight.NORMAL;
		case TextModelFontWeight.BLACK:
			return FontWeight.BLACK;
		case TextModelFontWeight.BOLD:
			return FontWeight.BOLD;
		case TextModelFontWeight.EXTRA_BOLD:
			return FontWeight.EXTRA_BOLD;
		case TextModelFontWeight.EXTRA_LIGHT:
			return FontWeight.EXTRA_LIGHT;
		case TextModelFontWeight.LIGHT:
			return FontWeight.LIGHT;
		case TextModelFontWeight.MEDIUM:
			return FontWeight.MEDIUM;
		case TextModelFontWeight.SEMI_BOLD:
			return FontWeight.SEMI_BOLD;
		case TextModelFontWeight.THIN:
			return FontWeight.THIN;
		default:
			return FontWeight.NORMAL;
		}
	}

	public static VPos vPosConverter(String vPos) {
		switch (vPos) {
		case TextModelVerticalPos.CENTER:
			return VPos.CENTER;
		case TextModelVerticalPos.BASELINE:
			return VPos.BASELINE;
		case TextModelVerticalPos.BOTTOM:
			return VPos.BOTTOM;
		case TextModelVerticalPos.TOP:
			return VPos.TOP;
		default:
			return VPos.CENTER;
		}
	}

}
