package helpers;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHelper {
	private ExceptionHelper() {
	}

	public static String getStackTraceAsString(Exception e) {
		StringWriter stringWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}
}
