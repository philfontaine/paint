package helpers;

import java.io.File;

public class ExtensionHelper {
	private ExtensionHelper() {
	}

	public static String getExtension(File file) {
		String fileName = file.getName();
		int i = fileName.lastIndexOf('.');
		return (i > 0) ? fileName.substring(i + 1).toLowerCase() : "";
	}
}
