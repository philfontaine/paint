package helpers;

public class RatioConverter {
	private RatioConverter() {
	}

	public static double ratioToValue(double ratio, double limit1, double limit2) {
		return ratio * (limit2 - limit1) + limit1;
	}

	public static double valueToRatio(double value, double limit1, double limit2) {
		return (value - limit1) / (limit2 - limit1);
	}
}
