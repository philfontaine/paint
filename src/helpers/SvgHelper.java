package helpers;

import model.Color;
import model.DrawingOptionsModel;

public class SvgHelper {
	private SvgHelper() {
	}

	public static String getStyle(DrawingOptionsModel drawingOptions) {
		String style = "";
		style += "fill:" + SvgHelper.getRgba(drawingOptions.getFillColor()) + ";";
		style += "stroke:" + SvgHelper.getRgba(drawingOptions.getBrushColor()) + ";";
		style += "stroke-width:" + drawingOptions.getBrushSize();
		return style;
	}

	private static String getRgba(Color color) {
		return "rgba(" + color.getR() + "," + color.getG() + "," + color.getB() + "," + color.getOpacity() + ")";
	}
}
