package list;

public interface ICurrentObserver<T> {
	public void onCurrentChange(T newCurrent);
}
