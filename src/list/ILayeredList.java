package list;

public interface ILayeredList<T> {
	public void add(T o);

	public void remove(T o);

	public void clear();

	public void swap(T o1, T o2);

	public void swap(int i1, int i2);

	public void moveTop(T o);

	public void moveBottom(T o);

	public void moveUp(T o);

	public void moveDown(T o);

	public T getFirst();

	public T getLast();

	public T get(int index);

	public void insertAtIndex(T o, int index);

	public int indexOf(T o);

	public boolean contains(T o);

	public int size();

	public void moveTo(T o, int index);
}
