package list;

public interface ILayeredListObserver<T> {
	public void onAdd(T o);

	public void onRemove(T o);

	public void onClear();

	public void onSwap(T o1, T o2);

	public void onInsertAtIndex(T o, int index);
}
