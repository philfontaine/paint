package list;

import java.util.ArrayList;
import java.util.List;

public abstract class ObservableLayeredList<T> implements ILayeredList<T> {

	private ArrayList<ILayeredListObserver<T>> listObservers = new ArrayList<>();
	private ArrayList<ICurrentObserver<T>> currentObservers = new ArrayList<>();

	protected ArrayList<T> content = new ArrayList<>();

	protected T current;

	public void registerListObserver(ILayeredListObserver<T> o) {
		if (!listObservers.contains(o)) {
			listObservers.add(o);
		}
	}

	public void unregisterListObserver(ILayeredListObserver<T> o) {
		if (listObservers.contains(o)) {
			listObservers.remove(o);
		}
	}

	public void registerCurrentObserver(ICurrentObserver<T> o) {
		if (!currentObservers.contains(o)) {
			currentObservers.add(o);
		}
	}

	public void unregisterCurrentObserver(ICurrentObserver<T> o) {
		if (currentObservers.contains(o)) {
			currentObservers.remove(o);
		}
	}

	public void notifyAdd(T toAdd) {
		for (ILayeredListObserver<T> observer : listObservers) {
			observer.onAdd(toAdd);
		}
	}

	public void notifyRemove(T toRemove) {
		for (ILayeredListObserver<T> observer : listObservers) {
			observer.onRemove(toRemove);
		}
	}

	public void notifyCurrentChanged(T current) {
		for (ICurrentObserver<T> observer : currentObservers) {
			observer.onCurrentChange(current);
		}
	}

	public void notifyClear() {
		for (ILayeredListObserver<T> observer : listObservers) {
			observer.onClear();
		}
	}

	public void notifySwap(T o1, T o2) {
		for (ILayeredListObserver<T> observer : listObservers) {
			observer.onSwap(o1, o2);
		}
	}

	public void notifyInsertAtIndex(T o, int index) {
		for (ILayeredListObserver<T> observer : listObservers) {
			observer.onInsertAtIndex(o, index);
		}
	}

	@Override
	public T getLast() {
		return (content != null && !content.isEmpty()) ? content.get(content.size() - 1) : null;
	}

	@Override
	public T getFirst() {
		return (content != null && !content.isEmpty()) ? content.get(0) : null;
	}

	@Override
	public T get(int index) {
		return content.get(index);
	}

	@Override
	public void add(T toAdd) {
		if (!content.contains(toAdd)) {
			content.add(toAdd);
			setCurrent(toAdd);
			notifyAdd(toAdd);
		}
	}

	@Override
	public void remove(T toRemove) {
		if (content.contains(toRemove)) {
			content.remove(toRemove);
			notifyRemove(toRemove);
			setCurrent(getLast());
		}
	}

	@Override
	public void clear() {
		content.clear();
		notifyClear();
		setCurrent(null);
	}

	@Override
	public void swap(T o1, T o2) {
		if (content.contains(o1) && content.contains(o2)) {
			int index1 = content.indexOf(o1);
			int index2 = content.indexOf(o2);
			content.set(index1, o2);
			content.set(index2, o1);
			notifySwap(o1, o2);
		}
	}

	@Override
	public void swap(int i1, int i2) {
		if (i1 > -1 && i1 < content.size() && i2 > -1 && i2 < content.size()) {
			swap(content.get(i1), content.get(i2));
		}
	}

	@Override
	public void moveTop(T o) {
		remove(o);
		insertAtIndex(o, size());
	}

	@Override
	public void moveBottom(T o) {
		remove(o);
		insertAtIndex(o, 0);
	}

	@Override
	public void moveUp(T o) {
		int index = content.indexOf(o);

		if (index > -1 && index < content.size() - 1) {
			swap(index, index + 1);
		}
	}

	@Override
	public void moveDown(T o) {
		int index = content.indexOf(o);

		if (index > 0) {
			swap(index, index - 1);
		}
	}

	protected void setCurrent(T current) {
		if (!this.contains(current)) {
			this.current = null;
			notifyCurrentChanged(current);
		}else if (this.current != current) {
			this.current = current;
			notifyCurrentChanged(current);
		}
	}

	public T getCurrent() {
		return current;
	}
	
	public int getCurrentIndex() {
		return indexOf(getCurrent());
	}

	public List<T> getList() {
		return content;
	}

	public void setList(List<T> content) {
		this.content.clear();
		for (T o : content) {
			add(o);
		}
	}

	@Override
	public void insertAtIndex(T o, int index) {
		content.add(index, o);
		setCurrent(o);
		notifyInsertAtIndex(o, index);
	}

	@Override
	public int indexOf(T o) {
		return content.indexOf(o);
	}

	@Override
	public boolean contains(T o) {
		for (T t : content) {
			if (t == o) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int size() {
		return content.size();
	}

	@Override
	public void moveTo(T o, int index) {
		remove(o);
		insertAtIndex(o, index);
	}
}
