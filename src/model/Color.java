package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Color")
@XmlAccessorType(XmlAccessType.NONE)
public class Color {
	@XmlElement
	private int r;
	@XmlElement
	private int g;
	@XmlElement
	private int b;
	@XmlElement
	private double opacity;

	public Color(int red, int green, int blue, double opacity) {
		this.r = getInRange(red);
		this.g = getInRange(green);
		this.b = getInRange(blue);
		this.opacity = getInRange(opacity);
	}

	public Color(int red, int green, int blue) {
		this(red, green, blue, 1.0);
	}

	public Color() {
		this(0, 0, 0);
	}

	public Color(Color other) {
		this.r = other.r;
		this.g = other.g;
		this.b = other.b;
		this.opacity = other.opacity;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = getInRange(r);
	}

	public int getG() {
		return g;
	}

	public void setG(int g) {
		this.g = getInRange(g);
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = getInRange(b);
	}

	public double getOpacity() {
		return opacity;
	}

	public void setOpacity(double opacity) {
		this.opacity = getInRange(opacity);
	}

	private static int getInRange(int color) {
		if (color < 0) {
			color = 0;
		} else if (color > 255) {
			color = 255;
		}
		return color;
	}

	private static double getInRange(double opacity) {
		if (opacity < 0.0) {
			opacity = 0.0;
		} else if (opacity > 1.0) {
			opacity = 1.0;
		}
		return opacity;
	}

	public boolean isEqualTo(Color color) {
		return this.r == color.r && this.g == color.g && this.b == color.b && this.opacity == color.opacity;
	}

}
