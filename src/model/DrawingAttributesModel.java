package model;

import java.util.prefs.Preferences;

public class DrawingAttributesModel {
	private static final String DRAWING_MODE = "DrawingMode";
	private static final String BRUSH_SIZE = "BrushSize";
	private static final String BRUSH_COLOR_R = "BrushColorR";
	private static final String BRUSH_COLOR_G = "BrushColorG";
	private static final String BRUSH_COLOR_B = "BrushColorB";
	private static final String FILL_COLOR_R = "FillColorR";
	private static final String FILL_COLOR_G = "FillColorG";
	private static final String FILL_COLOR_B = "FillColorB";
	private static final String DEFAULT_DRAWING_MODE = "Brush";
	private static final double DEFAULT_BRUSH_SIZE = 1;
	private static final int DEFAULT_BRUSH_COLOR_R = 0;
	private static final int DEFAULT_BRUSH_COLOR_G = 0;
	private static final int DEFAULT_BRUSH_COLOR_B = 0;
	private static final int DEFAULT_FILL_COLOR_R = 255;
	private static final int DEFAULT_FILL_COLOR_G = 255;
	private static final int DEFAULT_FILL_COLOR_B = 255;
	private static final String NODE_NAME = "DrawingAttributes";

	private String drawingMode;
	private DrawingOptionsModel drawingOptionsModel;

	public DrawingAttributesModel() {
		drawingOptionsModel = new DrawingOptionsModel();
	}

	public void initialize() {
		Preferences preferences = Preferences.userRoot().node(NODE_NAME);

		drawingMode = preferences.get(DRAWING_MODE, DEFAULT_DRAWING_MODE);

		drawingOptionsModel.setBrushSize(preferences.getDouble(BRUSH_SIZE, DEFAULT_BRUSH_SIZE));

		int brushColorR = preferences.getInt(BRUSH_COLOR_R, DEFAULT_BRUSH_COLOR_R);
		int brushColorG = preferences.getInt(BRUSH_COLOR_G, DEFAULT_BRUSH_COLOR_G);
		int brushColorB = preferences.getInt(BRUSH_COLOR_B, DEFAULT_BRUSH_COLOR_B);
		drawingOptionsModel.setBrushColor(new Color(brushColorR, brushColorG, brushColorB));

		int fillColorR = preferences.getInt(FILL_COLOR_R, DEFAULT_FILL_COLOR_R);
		int fillColorG = preferences.getInt(FILL_COLOR_G, DEFAULT_FILL_COLOR_G);
		int fillColorB = preferences.getInt(FILL_COLOR_B, DEFAULT_FILL_COLOR_B);
		drawingOptionsModel.setFillColor(new Color(fillColorR, fillColorG, fillColorB));
	}

	public void set(DrawingOptionsModel drawingOptionsModel, String drawingMode) {
		this.drawingOptionsModel = drawingOptionsModel;
		this.drawingMode = drawingMode;

		Preferences preferences = Preferences.userRoot().node(NODE_NAME);

		preferences.put(DRAWING_MODE, drawingMode);

		preferences.putDouble(BRUSH_SIZE, drawingOptionsModel.getBrushSize());

		preferences.putInt(BRUSH_COLOR_R, drawingOptionsModel.getBrushColor().getR());
		preferences.putInt(BRUSH_COLOR_G, drawingOptionsModel.getBrushColor().getG());
		preferences.putInt(BRUSH_COLOR_B, drawingOptionsModel.getBrushColor().getB());

		preferences.putInt(FILL_COLOR_R, drawingOptionsModel.getFillColor().getR());
		preferences.putInt(FILL_COLOR_G, drawingOptionsModel.getFillColor().getG());
		preferences.putInt(FILL_COLOR_B, drawingOptionsModel.getFillColor().getB());
	}

	public String getDrawingMode() {
		return drawingMode;
	}

	public DrawingOptionsModel getDrawingOptionsModel() {
		return drawingOptionsModel;
	}
}
