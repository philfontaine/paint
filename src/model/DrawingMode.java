package model;

public class DrawingMode {
	private DrawingMode() {
	}

	public static final String BRUSH = "Brush";
	public static final String RECTANGLE = "Rectangle";
	public static final String ELLIPSE = "Ellipse";
	public static final String LINE = "Line";
	public static final String UDES = "UdeS";
	public static final String MAN = "Man";
	public static final String TEXT = "Text";
	public static final String IMAGE = "Image";
}
