package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import model.anchor.AnchorPoint;
import model.anchor.AnchorPointList;
import model.anchor.anchorposition.AnchorPositionSet;
import model.shape.BrushModel;
import model.shape.EllipseModel;
import model.shape.LineModel;
import model.shape.ManModel;
import model.shape.RectangleModel;
import model.shape.DrawingGroupModel;
import model.shape.UdesModel;
import model.shape.svg.DrawingSvg;
import observer.Observable;

@XmlRootElement(name = "DrawingModel")
@XmlSeeAlso({ EllipseModel.class, LineModel.class, ManModel.class, RectangleModel.class, BrushModel.class,
		UdesModel.class, DrawingGroupModel.class })
@XmlAccessorType(XmlAccessType.NONE)
public abstract class DrawingModel extends Observable {

	@XmlElement
	private Point2D pi = new Point2D(0, 0);
	@XmlElement
	private Point2D pf = new Point2D(0, 0);

	@XmlElement
	private double rotation;

	@XmlElement
	private DrawingOptionsModel drawingOptions;

	@XmlElement
	private DrawingTextModel textModel = new DrawingTextModel();

	@XmlElement
	private boolean selected = false;
	@XmlElement
	private boolean visible = true;

	// Pas dans XML
	private AnchorPointList anchorList;
	
	
	public DrawingModel() {
		this.drawingOptions = new DrawingOptionsModel();
		pi = new Point2D(0, 0);
		pf = new Point2D(0, 0);
		anchorList = new AnchorPointList(this);
	}

	public DrawingModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptions) {
		this.drawingOptions = drawingOptions;
		setXi(pt.getX(), context.getWidth());
		setXf(pt.getX(), context.getWidth());
		setYi(pt.getY(), context.getHeight());
		setYf(pt.getY(), context.getHeight());
		anchorList = new AnchorPointList(this);
	}

	public DrawingModel(DrawingModel other) {
		this.drawingOptions = new DrawingOptionsModel(other.getDrawingOptions());
		this.pi = new Point2D(other.pi);
		this.pf = new Point2D(other.pf);
		this.rotation = other.rotation;
		this.textModel = new DrawingTextModel(other.textModel);
		anchorList = new AnchorPointList(this);
	}

	public abstract void update(Point2D pt, SceneContext context);

	public DrawingSvg getDrawingSvg() {
		return null;
	}

	public void setDrawingOptions(DrawingOptionsModel drawingOptionsModel) {
		drawingOptions = drawingOptionsModel;
	}

	// GETTERS
	public DrawingOptionsModel getDrawingOptions() {
		return drawingOptions;
	}

	public double getTopLeftX() {
		return Math.min(pi.getX(), pf.getX());
	}

	public double getTopLeftY() {
		return Math.min(pi.getY(), pf.getY());
	}

	public Point2D getTopLeft() {
		return (new Point2D(getTopLeftX(), getTopLeftY()));
	}

	public double getWidth() {
		return Math.abs(pi.getX() - pf.getX());
	}

	public double getHeight() {
		return Math.abs(pi.getY() - pf.getY());
	}

	public double getXi() {
		return pi.getX();
	}

	public double getYi() {
		return pi.getY();
	}

	public double getXf() {
		return pf.getX();
	}

	public double getYf() {
		return pf.getY();
	}

	public Point2D getPi() {
		return pi;
	}

	public Point2D getPf() {
		return pf;
	}

	public double getRotation() {
		return rotation;
	}

	public DrawingTextModel getTextModel() {
		return textModel;
	}

	public AnchorPointList getAnchorList() {
		return (anchorList);
	}

	public boolean isSelected() {
		return selected;
	}

	public void select() {
		if (!selected) {
			selected = true;
			notifyObservers();
		}
	}

	public void deselect() {
		if (selected) {
			selected = false;
			notifyObservers();
		}
	}

	public boolean isVisible() {
		return visible;
	}
	
	public void changeVisible() {
		visible = !visible;
		notifyObservers();
	}
	
	public void finish() {	} // called when drawingModel is finished to be drawn, useful if Override
	
	// Private setters called by update methods below
	private void setXi(double x, double cw) {
		pi.setX(setInBoundaries(x, cw));
	}

	private void setYi(double y, double ch) {
		pi.setY(setInBoundaries(y, ch));
	}

	private void setXf(double x, double cw) {
		pf.setX(setInBoundaries(x, cw));
	}

	private void setYf(double y, double ch) {
		pf.setY(setInBoundaries(y, ch));
	}

	private void setTopLeftX(double x) {
		if (pi.getX() < pf.getX()) {
			pf.setX(pf.getX() + x - pi.getX());
			pi.setX(x);
		} else {
			pi.setX(pi.getX() + x - pf.getX());
			pf.setX(x);
		}
	}

	private void setTopLeftX(double x, double cw) {
		setTopLeftX(setLocInBoundaries(x, this.getWidth(), cw));
	}

	private void setTopLeftY(double y) {
		if (pi.getY() < pf.getY()) {
			pf.setY(pf.getY() + y - pi.getY());
			pi.setY(y);
		} else {
			pi.setY(pi.getY() + y - pf.getY());
			pf.setY(y);
		}
	}

	private void setTopLeftY(double y, double ch) {
		setTopLeftY(setLocInBoundaries(y, this.getHeight(), ch));
	}

	private void setTopLeft(Point2D pt) {
		setTopLeftX(pt.getX());
		setTopLeftY(pt.getY());
	}

	private void setTopLeft(Point2D pt, SceneContext context) {
		setTopLeftX(pt.getX(), context.getWidth());
		setTopLeftY(pt.getY(), context.getHeight());
	}

	private void setWidth(double w) {
		if (w >= 0) {
			if(getTopLeftX() == pi.getX()) {
				pf.setX(getTopLeftX() + w);
			} else {
				pi.setX(getTopLeftX() + w);
			}
		}
	}

	private void setWidth(double w, double cw) {
		setWidth(setModelDimInBoundaries(getTopLeftX(), w, cw));
	}

	private void setHeight(double h) {
		if(h >= 0)
			if (getTopLeftY() == pi.getY()) {
				pf.setY(getTopLeftY() + h);
			} else {
				pi.setY(getTopLeftY() + h);
			}
	}

	private void setHeight(double h, double ch) {
		setHeight(setModelDimInBoundaries(getTopLeftY(), h, ch));
	}

	private void setText(DrawingTextModel t) {
		textModel = t;
	}

	private void setRotation(double angle) {
		if (angle < -360) {
			angle = -360;
		} else if (angle > 360) {
			angle = 360;
		}
		this.rotation = angle;
	}

	// UPDATERS (with observers notification)

	public void updateInitialPoint(Point2D pt) {
		pi.setX(pt.getX());
		pi.setY(pt.getY());
		notifyObservers();
	}

	public void updateInitialPoint(Point2D pt, SceneContext context) {
		setXi(pt.getX(), context.getWidth());
		setYi(pt.getY(), context.getHeight());
		notifyObservers();
	}

	public void updateFinalPoint(Point2D pt) {
		pf.setX(pt.getX());
		pf.setY(pt.getY());
		notifyObservers();
	}

	public void updateFinalPoint(Point2D pt, SceneContext context) {
		setXf(pt.getX(), context.getWidth());
		setYf(pt.getY(), context.getHeight());
		notifyObservers();
	}

	public void updateTopLeft(Point2D pt) {
		setTopLeft(pt);
		notifyObservers();
	}

	public void updateTopLeft(Point2D pt, SceneContext context) {
		setTopLeft(pt, context);
		notifyObservers();
	}

	public void updateWidth(double w) {
		setWidth(w);
		notifyObservers();
	}

	public void updateWidth(double w, double cw) {
		if (w != getWidth()) {
			setWidth(w, cw);
			notifyObservers();
		}
	}

	public void updateHeight(double h) {
		setHeight(h);
		notifyObservers();
	}

	public void updateHeight(double h, double ch) {
		if (h != getHeight()) {
			setHeight(h, ch);
			notifyObservers();
		}
	}

	public void updateRotation(double r) {
		if (r != getRotation()) {
			setRotation(r);
			notifyObservers();
		}
	}

	public void updateText(DrawingTextModel t) {
		setText(t);
		notifyObservers();
	}

	protected boolean isOutOfBoundaries(double location, double contextSpace) {
		boolean isOut = false;
		double halfBrushSize = getBrushSize() / 2;
		if (location < halfBrushSize || location > contextSpace - halfBrushSize) {
			isOut = true;
		}
		return isOut;
	}

	public double getBrushSize() {
		return drawingOptions.getBrushSize();
	}

	protected double setInBoundaries(double loc, double space) {
		double halfBrushSize = drawingOptions.getBrushSize() / 2;
		if (loc < halfBrushSize) {
			return halfBrushSize;
		} else if (loc > space - halfBrushSize) {
			return space - halfBrushSize;
		}
		return loc;
	}

	protected double setLocInBoundaries(double loc, double modelDimention, double space) {
		double newLoc = setInBoundaries(loc, space);
		double newOtherSideLoc = newLoc + modelDimention;
		double newOtherSideLocInBoundaries = setInBoundaries(newOtherSideLoc, space);
		double newLocWithOtherSideInBoundaries = newOtherSideLocInBoundaries - modelDimention;
		double newLocWithOtherSideInBoundariesInBoundaries = setInBoundaries(newLocWithOtherSideInBoundaries, space);

		if (!isOutOfBoundaries(newLocWithOtherSideInBoundariesInBoundaries, space)) {
			return (newLocWithOtherSideInBoundariesInBoundaries);
		}
		return loc;
	}

	protected double setModelDimInBoundaries(double loc, double modelDimention, double space) {
		double otherSide = loc + modelDimention;
		double otherSideInBoundaries = setInBoundaries(otherSide, space);
		return (modelDimention - (otherSide - otherSideInBoundaries));
		
	}

	public boolean contains(Point2D pt) {
		double thisHalfBrush = this.getBrushSize() / 2;
		return (pt.getX() >= this.getTopLeftX() - thisHalfBrush
				&& pt.getY() >= this.getTopLeftY() - thisHalfBrush
				&& pt.getX() <= this.getTopLeftX() + this.getWidth() + thisHalfBrush
				&& pt.getY() <= this.getTopLeftY() + this.getHeight() + thisHalfBrush);
	}

	public boolean isWithin(DrawingModel other) {
		double thisHalfBrush = this.getBrushSize() / 2;
		double otherHalfBrush = other.getBrushSize() / 2;
		return (getTopLeftX() - thisHalfBrush >= other.getTopLeftX() - otherHalfBrush
				&& getTopLeftY() - thisHalfBrush >= other.getTopLeftY() - otherHalfBrush
				&& getTopLeftX() + getWidth() + thisHalfBrush <= other.getTopLeftX() + other.getWidth() + otherHalfBrush
				&& getTopLeftY() + getHeight() + thisHalfBrush <= other.getTopLeftY() + other.getHeight() + otherHalfBrush);
	}

	public AnchorPoint anchorUnderMouse(Point2D p) {
		return anchorList.isOnAnchor(p);
	}

	public AnchorPoint getAnchorAtPosition(AnchorPositionSet posSet) {
		for (AnchorPoint a : anchorList.getList()) {
			if (a.asSamePosSet(posSet)) {
				return a;
			}
		}
		return null;
	}
}
