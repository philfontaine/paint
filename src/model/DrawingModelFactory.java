package model;

import model.shape.BrushModel;
import model.shape.EllipseModel;
import model.shape.ImageModel;
import model.shape.LineModel;
import model.shape.ManModel;
import model.shape.RectangleModel;
import model.shape.DrawingGroupModel;
import model.shape.UdesModel;

public class DrawingModelFactory {
	private static final DrawingModelFactory INSTANCE = new DrawingModelFactory();

	private DrawingModelFactory() {
	}

	public static DrawingModelFactory getInstance() {
		return INSTANCE;
	}

	public DrawingModel getModel(Point2D pt, SceneContext context, String drawingMode,
			DrawingOptionsModel drawingOptions) {
		DrawingModel drawingModel = null;
		switch (drawingMode) {
		case DrawingMode.BRUSH:
			drawingModel = new BrushModel(pt, context, drawingOptions);
			break;
		case DrawingMode.ELLIPSE:
			drawingModel = new EllipseModel(pt, context, drawingOptions);
			break;
		case DrawingMode.RECTANGLE:
		case DrawingMode.TEXT:
			drawingModel = new RectangleModel(pt, context, drawingOptions);
			break;
		case DrawingMode.LINE:
			drawingModel = new LineModel(pt, context, drawingOptions);
			break;
		case DrawingMode.UDES:
			drawingModel = new UdesModel(pt, context, drawingOptions);
			break;
		case DrawingMode.MAN:
			drawingModel = new ManModel(pt, context, drawingOptions);
			break;
		case DrawingMode.IMAGE:
			drawingModel = new ImageModel();
			break;
		default:
			break;
		}
		return drawingModel;
	}

	public static DrawingModel getModel(DrawingModel other) {
		DrawingModel drawingModel = null;
		if (other instanceof BrushModel) {
			drawingModel = new BrushModel((BrushModel) other);
		} else if (other instanceof EllipseModel) {
			drawingModel = new EllipseModel((EllipseModel) other);
		} else if (other instanceof RectangleModel) {
			drawingModel = new RectangleModel((RectangleModel) other);
		} else if (other instanceof LineModel) {
			drawingModel = new LineModel((LineModel) other);
		} else if (other instanceof UdesModel) {
			drawingModel = new UdesModel((UdesModel) other);
		} else if (other instanceof ManModel) {
			drawingModel = new ManModel((ManModel) other);
		} else if (other instanceof DrawingGroupModel) {
			drawingModel = new DrawingGroupModel((DrawingGroupModel) other);
		} else if (other instanceof ImageModel) {
			drawingModel = new ImageModel((ImageModel) other);
		}
		return drawingModel;
	}

}
