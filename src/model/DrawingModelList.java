package model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import list.ILayeredList;
import list.ObservableLayeredList;
import model.anchor.AnchorPoint;

@XmlRootElement
public class DrawingModelList extends ObservableLayeredList<DrawingModel> implements ILayeredList<DrawingModel> {

	private DrawingModel memorized;

	public DrawingModelList() {
		super();
	}

	public DrawingModelList(DrawingModelList other) {
		this.content = new ArrayList<>(other.getList());
	}

	@Override
	public void setCurrent(DrawingModel current) {
		if (this.current != null) {
			this.current.deselect();
		}

		super.setCurrent(current);
	}

	@Override
	public int size() {
		return content.size();
	}

	@Override
	public List<DrawingModel> getList() {
		return content;
	}

	public void selectAll() {
		for (DrawingModel c : content) {
			c.select();
		}
	}

	public void selectCurrent() {
		if (current != null) {
			current.select();
		}
	}

	public void deselectAll() {
		for (DrawingModel c : content) {
			c.deselect();
		}
	}

	public List<Integer> getSelectedItemsIndexes() {
		ArrayList<Integer> result = new ArrayList<>();

		for (DrawingModel c : content) {
			if (c.isSelected()) {
				result.add(content.indexOf(c));
			}
		}
		return result;
	}

	public AnchorPoint anchorUnderMouse(Point2D p) {
		for (DrawingModel m : content) {
			if (m.isSelected() && m.anchorUnderMouse(p) != null) {
				return (m.anchorUnderMouse(p));
			}
		}
		return null;
	}

	public DrawingModel memorize(int index) {
		if (index >= 0 && index < size()) {
			setMemorized(DrawingModelFactory.getModel(this.get(index)));
		}
		return this.memorized;
	}

	public DrawingModel getMemorized() {
		return memorized;
	}

	public void setMemorized(DrawingModel memorized) {
		this.memorized = memorized;
	}

	public DrawingModel replicate() {
		if (memorized != null) {
			DrawingModel model = DrawingModelFactory.getModel(memorized);
			add(model);
			return model;
		} else {
			return null;
		}
	}

}
