package model;

import observer.Observable;

public class DrawingOptionsModel extends Observable {
	private double brushSize;

	private Color brushColor;

	private Color fillColor;

	public DrawingOptionsModel() {
		this.brushSize = 1;
		this.brushColor = new Color(0, 0, 0);
		this.fillColor = new Color(255, 255, 255);
	}

	public DrawingOptionsModel(double size, Color stroke, Color fill) {
		brushSize = size;
		brushColor = stroke;
		fillColor = fill;
	}

	public DrawingOptionsModel(DrawingOptionsModel drawingOptionsModel) {
		this.brushSize = drawingOptionsModel.brushSize;
		this.brushColor = new Color(drawingOptionsModel.brushColor);
		this.fillColor = new Color(drawingOptionsModel.fillColor);
	}

	public double getBrushSize() {
		return brushSize;
	}

	public void setBrushSize(double brushSize) {
		this.brushSize = brushSize;
		notifyObservers();
	}

	public Color getBrushColor() {
		return brushColor;
	}

	public void setBrushColor(Color brushColor) {
		this.brushColor = brushColor;
		notifyObservers();
	}

	public Color getFillColor() {
		return fillColor;
	}

	public void setFillColor(Color fillColor) {
		this.fillColor = fillColor;
		notifyObservers();
	}

}
