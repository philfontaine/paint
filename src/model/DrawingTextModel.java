package model;

import observer.Observable;

public class DrawingTextModel extends Observable {
	private String text = "";
	private Double size = 30.;
	private Color color = new Color(0, 0, 0);
	private String verticalPosition = TextModelVerticalPos.CENTER;
	private String alignment = TextModelAlignment.CENTER;
	private String fontWeight = TextModelFontWeight.NORMAL;

	public DrawingTextModel() {

	}

	public DrawingTextModel(DrawingTextModel other) {
		this.text = other.text;
		this.size = other.size;
		this.color = new Color(other.color);
		this.verticalPosition = other.verticalPosition;
		this.alignment = other.alignment;
		this.fontWeight = other.fontWeight;
	}

	public void setText(String text) {
		this.text = text;
		notifyObservers();
	}

	public void setSize(Double newValue) {
		this.size = newValue;
		notifyObservers();
	}

	public void setColor(Color color) {
		this.color = color;
		notifyObservers();
	}

	public void setVerticalPosition(String verticalPosition) {
		this.verticalPosition = verticalPosition;
		notifyObservers();
	}

	public void setAlignment(String alignment) {
		this.alignment = alignment;
		notifyObservers();
	}

	public void setFontWeight(String fontWeight) {
		this.fontWeight = fontWeight;
		notifyObservers();
	}

	public String getText() {
		return text;
	}

	public Double getSize() {
		return size;
	}

	public Color getColor() {
		return color;
	}

	public String getVerticalPosition() {
		return verticalPosition;
	}

	public String getAlignment() {
		return alignment;
	}

	public String getFontWeight() {
		return fontWeight;
	}
}
