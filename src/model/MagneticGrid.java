package model;

public class MagneticGrid {

	private int scaleWidth;
	private int scaleHeight;
	private boolean isActive;

	public MagneticGrid(int scaleWidth, int scaleHeight) {
		this.scaleWidth = scaleWidth;
		this.scaleHeight = scaleHeight;
		isActive = false;
	}

	public void setScaleWidth(int width) {
		scaleWidth = width;
	}

	public void setScaleHeight(int height) {
		scaleHeight = height;
	}

	public void changeIsActive() {
		isActive = !isActive;
	}

	public int getScaleWidth() {
		return scaleWidth;
	}

	public int getScaleHeight() {
		return scaleHeight;
	}

	public boolean getIsActive() {
		return isActive;
	}

	private double nearestXY(double xy, double cwh, int scaleSize) {
		int scale = (int) xy / scaleSize;
		double size = scaleSize;
		double rest = ((int) xy % scaleSize) / size;
		if (rest > 0.5) {
			scale++;
		}
		if (scale >= Math.floor(cwh / scaleSize) + 1) {
			return cwh;
		}
		return (double) scale * scaleSize;
	}

	public Point2D nearestPoint(Point2D point, SceneContext context) {
		return new Point2D(nearestXY(point.getX(), context.getWidth(), scaleWidth),
				nearestXY(point.getY(), context.getHeight(), scaleHeight));
	}
}
