package model;

public class Point2D {
	private double x = 0;
	private double y = 0;

	public Point2D() {
	}

	public Point2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Point2D(Point2D other) {
		this.x = other.x;
		this.y = other.y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public boolean sameAs(Point2D other) {
		return (this.getX() == other.getX() && this.getY() == other.getY());
	}

	public Point2D add(Point2D other) {
		return new Point2D(this.x + other.x, this.y + other.y);
	}

	public Point2D subtract(Point2D other) {
		return new Point2D(this.x - other.x, this.y - other.y);
	}
}
