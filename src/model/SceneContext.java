package model;

public class SceneContext {
	private double width;
	private double height;

	public SceneContext() {
	}

	public SceneContext(double w, double h) {
		width = w;
		height = h;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

}
