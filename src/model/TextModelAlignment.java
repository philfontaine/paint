package model;

public class TextModelAlignment {
	private TextModelAlignment() {
	}
	
	public static final String CENTER = "Center";
	public static final String LEFT = "Left";
	public static final String RIGHT = "Right";
	public static final String JUSTIFY = "Justify";
}
