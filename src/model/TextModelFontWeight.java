package model;

public class TextModelFontWeight {
	private TextModelFontWeight() {
	}
	
	public static final String BLACK = "Black";
	public static final String BOLD = "Bold";
	public static final String EXTRA_BOLD = "ExtraBold";
	public static final String EXTRA_LIGHT = "ExtraLight";
	public static final String LIGHT = "Light";
	public static final String MEDIUM = "Medium";
	public static final String NORMAL = "Normal";
	public static final String SEMI_BOLD = "SemiBold";
	public static final String THIN = "Thin";
}
