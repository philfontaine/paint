package model;

public class TextModelVerticalPos {
	private TextModelVerticalPos() {
	}
	
	public static final String CENTER = "Center";
	public static final String BASELINE = "Baseline";
	public static final String BOTTOM = "Bottom";
	public static final String TOP = "Top";
}
