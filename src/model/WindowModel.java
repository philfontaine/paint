package model;

import java.util.prefs.Preferences;

public class WindowModel {
	private static final String WINDOW_POSITION_X = "WindowPositionX";
	private static final String WINDOW_POSITION_Y = "WindowPositionY";
	private static final String WINDOW_WIDTH = "WindowWidth";
	private static final String WINDOW_HEIGHT = "WindowHeight";
	private static final String WINDOW_IS_MAXIMIZED = "WindowIsMaximized";
	private static final double DEFAULT_X = 10;
	private static final double DEFAULT_Y = 10;
	private static final double DEFAULT_WIDTH = 800;
	private static final double DEFAULT_HEIGHT = 600;
	private static final boolean DEFAULT_IS_MAXIMIZED = false;
	private static final String NODE_NAME = "Window";

	private double x;
	private double y;
	private double width;
	private double height;
	private boolean isMaximized;

	public void initialize() {
		Preferences preferences = Preferences.userRoot().node(NODE_NAME);
		x = preferences.getDouble(WINDOW_POSITION_X, DEFAULT_X);
		y = preferences.getDouble(WINDOW_POSITION_Y, DEFAULT_Y);
		width = preferences.getDouble(WINDOW_WIDTH, DEFAULT_WIDTH);
		height = preferences.getDouble(WINDOW_HEIGHT, DEFAULT_HEIGHT);
		isMaximized = preferences.getBoolean(WINDOW_IS_MAXIMIZED, DEFAULT_IS_MAXIMIZED);
	}

	public void set(double x, double y, double width, double height, boolean isMaximized) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.isMaximized = isMaximized;

		Preferences preferences = Preferences.userRoot().node(NODE_NAME);
		preferences.putBoolean(WINDOW_IS_MAXIMIZED, isMaximized);
		if (!isMaximized) {
			preferences.putDouble(WINDOW_POSITION_X, x);
			preferences.putDouble(WINDOW_POSITION_Y, y);
			preferences.putDouble(WINDOW_WIDTH, width);
			preferences.putDouble(WINDOW_HEIGHT, height);
		}

	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getWidth() {
		return width;
	}

	public double getHeight() {
		return height;
	}

	public boolean isMaximized() {
		return isMaximized;
	}
}
