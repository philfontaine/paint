package model.anchor;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;
import model.anchor.anchorposition.AnchorPositionSet;

public class AnchorPoint {
	private DrawingModel model;
	private AnchorPositionSet posSet;

	public AnchorPoint(DrawingModel m, AnchorPositionSet posSet) {
		model = m;
		this.posSet = posSet;
	}

	public void setPosition(Point2D draggedPosition, SceneContext ctx) {
		posSet.modify(model, draggedPosition, ctx);
	}

	public void setPosition(Point2D draggedPosition) {
		posSet.modify(model, draggedPosition);
	}

	public Point2D getPosition() {
		return posSet.getPosition(model);
	}

	public DrawingModel getModel() {
		return model;
	}

	public AnchorPositionSet getPosSet() {
		return posSet;
	}

	public boolean asSamePosSet(AnchorPositionSet otherPosSet) {
		return this.getPosSet().sameAs(otherPosSet);
	}
}
