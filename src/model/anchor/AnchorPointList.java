package model.anchor;

import java.util.ArrayList;
import java.util.List;

import model.DrawingModel;
import model.Point2D;
import model.anchor.anchorposition.AnchorPositionNoX;
import model.anchor.anchorposition.AnchorPositionNoY;
import model.anchor.anchorposition.AnchorPositionSet;
import model.anchor.anchorposition.AnchorPositionXF;
import model.anchor.anchorposition.AnchorPositionXI;
import model.anchor.anchorposition.AnchorPositionYF;
import model.anchor.anchorposition.AnchorPositionYI;
import model.anchor.anchorposition.IAnchorPosition;

public class AnchorPointList {

	private List<AnchorPoint> list = new ArrayList<>();
	private DrawingModel model;
	private double anchorSize = 10;

	public AnchorPointList(DrawingModel m) {
		this.model = m;

		IAnchorPosition top = new AnchorPositionYI();
		IAnchorPosition bottom = new AnchorPositionYF();
		IAnchorPosition left = new AnchorPositionXI();
		IAnchorPosition right = new AnchorPositionXF();
		IAnchorPosition noHorizontal = new AnchorPositionNoX();
		IAnchorPosition noVertical = new AnchorPositionNoY();

		list.add(new AnchorPoint(model, new AnchorPositionSet(left, top)));
		list.add(new AnchorPoint(model, new AnchorPositionSet(left, noVertical)));
		list.add(new AnchorPoint(model, new AnchorPositionSet(left, bottom)));
		list.add(new AnchorPoint(model, new AnchorPositionSet(noHorizontal, bottom)));
		list.add(new AnchorPoint(model, new AnchorPositionSet(right, bottom)));
		list.add(new AnchorPoint(model, new AnchorPositionSet(right, noVertical)));
		list.add(new AnchorPoint(model, new AnchorPositionSet(right, top)));
		list.add(new AnchorPoint(model, new AnchorPositionSet(noHorizontal, top)));
	}

	public AnchorPoint isOnAnchor(Point2D p) {
		for (AnchorPoint a : list) {
			if (isOnDim(p.getX(), a.getPosition().getX()) && isOnDim(p.getY(), a.getPosition().getY())) {
				return a;
			}
		}
		return null;
	}

	public List<AnchorPoint> getList() {
		return list;
	}

	public DrawingModel getModel() {
		return model;
	}

	public double getAnchorSize() {
		return anchorSize;
	}

	private boolean isOnDim(double test, double anchor) {
		int tolerance = 2;
		return (test >= anchor - anchorSize / 2 - tolerance && test <= anchor + anchorSize / 2 + tolerance);
	}
}
