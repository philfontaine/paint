package model.anchor.anchorposition;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;

public class AnchorPositionNoX implements IAnchorPosition{

	@Override
	public AnchorPositionType getType() {
		return AnchorPositionType.NOX;
	}

	@Override
	public void modify(DrawingModel m, Point2D draggedPosition, SceneContext ctx) {
		//Nothing to Do
	}

	@Override
	public void modify(DrawingModel m, Point2D draggedPosition) {
		//Nothing to Do
	}

	@Override
	public double getPosition(DrawingModel m) {
		return (m.getXi() + m.getXf()) / 2;
	}
}
