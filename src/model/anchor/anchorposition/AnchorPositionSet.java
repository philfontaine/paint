package model.anchor.anchorposition;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;

public class AnchorPositionSet {

	private IAnchorPosition hPosition;
	private IAnchorPosition vPosition;

	public AnchorPositionSet(IAnchorPosition hPosition, IAnchorPosition vPosition) {
		if (hPosition.getType() == AnchorPositionType.XI || hPosition.getType() == AnchorPositionType.XF
				|| hPosition.getType() == AnchorPositionType.NOX) {
			this.hPosition = hPosition;
		} else {
			this.hPosition = new AnchorPositionNoX();
		}
		if (vPosition.getType() == AnchorPositionType.YI || vPosition.getType() == AnchorPositionType.YF
				|| vPosition.getType() == AnchorPositionType.NOY) {
			this.vPosition = vPosition;
		} else {
			this.vPosition = new AnchorPositionNoY();
		}
	}

	public void modify(DrawingModel m, Point2D draggedPosition, SceneContext ctx) {
		hPosition.modify(m, draggedPosition, ctx);
		vPosition.modify(m, draggedPosition, ctx);
	}

	public void modify(DrawingModel m, Point2D draggedPosition) {
		hPosition.modify(m, draggedPosition);
		vPosition.modify(m, draggedPosition);
	}

	public Point2D getPosition(DrawingModel m) {
		return (new Point2D(hPosition.getPosition(m), vPosition.getPosition(m)));
	}

	public IAnchorPosition getVpos() {
		return vPosition;
	}

	public IAnchorPosition getHpos() {
		return hPosition;
	}

	public boolean sameAs(AnchorPositionSet other) {
		return (this.getHpos().getType() == other.getHpos().getType()
				&& this.getVpos().getType() == other.getVpos().getType());
	}
}
