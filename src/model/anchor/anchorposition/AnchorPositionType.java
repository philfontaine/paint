package model.anchor.anchorposition;

public enum AnchorPositionType {
	YI, YF, XI, XF, NOX, NOY
}
