package model.anchor.anchorposition;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;

public class AnchorPositionXF implements IAnchorPosition{

	@Override
	public AnchorPositionType getType() {
		return AnchorPositionType.XF;
	}

	@Override
	public void modify(DrawingModel m, Point2D draggedPosition, SceneContext ctx) {
		m.updateFinalPoint(new Point2D(draggedPosition.getX(), m.getYf()), ctx);
	}
	
	@Override
	public void modify(DrawingModel m, Point2D draggedPosition) {
		m.updateFinalPoint(new Point2D(draggedPosition.getX(), m.getYf()));
	}

	@Override
	public double getPosition(DrawingModel m) {
		return m.getXf();
	}

}
