package model.anchor.anchorposition;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;

public class AnchorPositionXI implements IAnchorPosition{

	@Override
	public AnchorPositionType getType() {
		return AnchorPositionType.XI;
	}

	@Override
	public void modify(DrawingModel m, Point2D draggedPosition, SceneContext ctx) {
		m.updateInitialPoint(new Point2D(draggedPosition.getX(), m.getYi()), ctx);
	}

	@Override
	public void modify(DrawingModel m, Point2D draggedPosition) {
		m.updateInitialPoint(new Point2D(draggedPosition.getX(), m.getYi()));
	}

	@Override
	public double getPosition(DrawingModel m) {
		return m.getXi();
	}

}
