package model.anchor.anchorposition;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;

public class AnchorPositionYF implements IAnchorPosition{

	@Override
	public AnchorPositionType getType() {
		return AnchorPositionType.YF;
	}

	@Override
	public void modify(DrawingModel m, Point2D draggedPosition, SceneContext ctx) {
		m.updateFinalPoint(new Point2D(m.getXf(), draggedPosition.getY()), ctx);
	}
	
	@Override
	public void modify(DrawingModel m, Point2D draggedPosition) {
		m.updateFinalPoint(new Point2D(m.getXf(), draggedPosition.getY()));
	}

	@Override
	public double getPosition(DrawingModel m) {
		return m.getYf();
	}

}
