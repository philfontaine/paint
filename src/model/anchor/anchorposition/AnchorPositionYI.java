package model.anchor.anchorposition;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;

public class AnchorPositionYI implements IAnchorPosition{

	@Override
	public AnchorPositionType getType() {
		return AnchorPositionType.YI;
	}

	@Override
	public void modify(DrawingModel m, Point2D draggedPosition, SceneContext ctx) {
		m.updateInitialPoint(new Point2D(m.getXi(), draggedPosition.getY()), ctx);
	}
	
	@Override
	public void modify(DrawingModel m, Point2D draggedPosition) {
		m.updateInitialPoint(new Point2D(m.getXi(), draggedPosition.getY()));
	}

	@Override
	public double getPosition(DrawingModel m) {
		return m.getYi();
	}

}
