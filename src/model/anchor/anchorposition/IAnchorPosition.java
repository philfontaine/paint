package model.anchor.anchorposition;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;

public interface IAnchorPosition {
	
	public AnchorPositionType getType();
	public void modify(DrawingModel m, Point2D draggedPosition, SceneContext ctx);
	public void modify(DrawingModel m, Point2D draggedPosition);
	public double getPosition(DrawingModel m);
}
