package model.command;

import javax.xml.bind.annotation.XmlElement;

public class ChangeCurrent extends Command {

	private int oldIndex;
	@XmlElement
	public int newIndex;

	public ChangeCurrent() {

	}

	public ChangeCurrent(int index) {
		newIndex = index;
	}

	@Override
	protected void executeCommand() {
		oldIndex = list.getList().indexOf(list.getCurrent());
		list.setCurrent(list.getList().get(newIndex));
	}

	@Override
	public void undo() {
		list.setCurrent(list.getList().get(oldIndex));
	}

	@Override
	public String toString() {
		return "Change Current";
	}
}
