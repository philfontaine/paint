package model.command;

import model.DrawingModelList;

public class Clear extends Command {
	private DrawingModelList backupList;

	@Override
	public void executeCommand() {
		backupList = new DrawingModelList(list);
		list.clear();
	}

	@Override
	public void undo() {
		list.setList(backupList.getList());
	}

	@Override
	public String toString() {
		return "Clear";
	}
}
