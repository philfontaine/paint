package model.command;

import javax.xml.bind.annotation.XmlSeeAlso;

import model.DrawingModelList;

@XmlSeeAlso({ Clear.class, CreateDrawing.class, DeleteSelectedDrawings.class, EditDrawingBrushColor.class,
		EditDrawingBrushSize.class, EditDrawingFillColor.class, EditDrawingHeight.class, EditDrawingRotation.class,
		EditDrawingTopLeft.class, EditDrawingWidth.class, GroupDrawings.class, UngroupDrawings.class,
		MoveBottomCommand.class, MoveDownCommand.class, MoveTopCommand.class, MoveUpCommand.class, ChangeCurrent.class,
		ImportImageCommand.class, DragAnchorPoint.class, EditDrawingFinalPoint.class, EditDrawingInitialPoint.class,
		EditText.class, EditTextAlignment.class, EditTextColor.class, EditTextFontWeight.class, EditTextSize.class,
		EditTextVPos.class, Memorize.class, Replicate.class })
public abstract class Command {
	protected DrawingModelList list;

	public void execute(DrawingModelList drawingModelList) {
		this.list = drawingModelList;
		executeCommand();
	}

	protected abstract void executeCommand();

	public abstract void undo();

	@Override
	public abstract String toString();
}
