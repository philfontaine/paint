package model.command;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import model.DrawingModelList;
import observer.Observable;

@XmlRootElement
public class CommandList extends Observable {
	@XmlElement
	private ArrayList<Command> list = new ArrayList<>();

	private DrawingModelList drawingModelList;

	private int lastIndex = -1;

	private boolean lastActionWasClear = false;

	public CommandList() {

	}

	public CommandList(DrawingModelList drawingModelList) {
		this.drawingModelList = drawingModelList;
	}

	public void clear() {
		removeTrailingElements();
		lastIndex = -1;
		lastActionWasClear = true;
		notifyObservers();
	}

	public void add(Command command) {
		lastActionWasClear = false;
		removeTrailingElements();
		lastIndex++;
		list.add(command);
		command.execute(drawingModelList);
		notifyObservers();
	}

	private void undoMethod() {
		if (lastActionWasClear) {
			lastIndex = list.size() - 1;
			lastActionWasClear = false;
			// Need to unclear
		} else {
			if (lastIndex >= 0) {
				list.get(lastIndex).undo();
				lastIndex--;
			}
		}
	}

	public void undo() {
		undoMethod();
		notifyObservers();
	}

	public void undo(int index) {
		for (int i = 0; i < index; i++) {
			undoMethod();
		}
		notifyObservers();
	}

	public void execute() {
		for (Command command : list) {
			command.execute(drawingModelList);
		}
		notifyObservers();
	}

	private void redoMethod() {
		if (lastIndex + 1 < list.size()) {
			lastIndex++;
			list.get(lastIndex).execute(drawingModelList);
		}
	}

	public void redo() {
		redoMethod();
		notifyObservers();
	}

	public void redo(int index) {
		for (int i = 0; i < index; i++) {
			redoMethod();
		}
		notifyObservers();
	}

	private void removeTrailingElements() {
		if (lastIndex + 1 < list.size()) {
			list.subList(lastIndex + 1, list.size()).clear();
		}
	}

	public List<Command> getCommandList() {
		return list;
	}

	public void setList(List<Command> list) {
		this.list = (ArrayList<Command>) list;
		lastIndex = list.size() - 1;
		notifyObservers();
	}

	public void addAll(List<Command> list) {
		this.list.addAll(list);
		lastIndex = this.list.size() - 1;
		notifyObservers();
	}

	public int getLastIndex() {
		return lastIndex;
	}

}
