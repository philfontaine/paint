package model.command;

import javax.xml.bind.annotation.XmlElement;

import model.DrawingModel;

public class CreateDrawing extends Command {

	@XmlElement
	private DrawingModel drawingModel;

	public CreateDrawing() {

	}

	public CreateDrawing(DrawingModel drawingModel) {
		this.drawingModel = drawingModel;
	}

	@Override
	public void executeCommand() {
		list.add(drawingModel);
	}

	@Override
	public void undo() {
		list.remove(drawingModel);
	}

	@Override
	public String toString() {
		return "Creating a drawing";
	}

}
