package model.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlElement;

import model.DrawingModel;

public class DeleteSelectedDrawings extends Command {
	@XmlElement
	private ArrayList<Integer> toDelete;

	private TreeMap<Integer, DrawingModel> drawingMap = new TreeMap<>();

	public DeleteSelectedDrawings() {

	}

	public DeleteSelectedDrawings(List<Integer> toDelete) {
		this.toDelete = (ArrayList<Integer>) toDelete;
	}

	@Override
	public void executeCommand() {
		ArrayList<DrawingModel> deletions = new ArrayList<>();
		for (int index : toDelete) {
			DrawingModel m = list.get(index);
			deletions.add(m);
			drawingMap.put(index, m);
		}

		// Remove separate entities
		for (DrawingModel selection : deletions) {
			list.remove(selection);
		}
	}

	@Override
	public void undo() {
		for (Map.Entry<Integer, DrawingModel> entry : drawingMap.entrySet()) {
			int index = entry.getKey();
			DrawingModel model = entry.getValue();
			list.insertAtIndex(model, index);
		}
		drawingMap.clear();
	}

	@Override
	public String toString() {
		return "Deleting selected drawings";
	}

}
