package model.command;

import javax.xml.bind.annotation.XmlElement;

import model.Point2D;
import model.anchor.AnchorPoint;
import model.anchor.anchorposition.AnchorPositionSet;

public class DragAnchorPoint extends EditDrawingCommand {

	@XmlElement
	AnchorPositionSet anchorPosSet;

	public DragAnchorPoint() {

	}

	public DragAnchorPoint(Object oldValue, Object newValue, AnchorPositionSet anchorPosSet, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
		this.anchorPosSet = anchorPosSet;
	}

	@Override
	protected void executeCommand() {
		AnchorPoint anchor = getDrawing().getAnchorAtPosition(anchorPosSet);
		anchor.setPosition((Point2D) newValue);
	}

	@Override
	public void undo() {
		AnchorPoint anchor = getDrawing().getAnchorAtPosition(anchorPosSet);
		anchor.setPosition((Point2D) oldValue);
	}

	@Override
	public String toString() {
		return ("Edit Size");
	}
}
