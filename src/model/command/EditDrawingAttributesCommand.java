package model.command;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import model.SceneContext;

@XmlRootElement
public abstract class EditDrawingAttributesCommand extends EditDrawingCommand {

	@XmlElement
	protected SceneContext context;

	public EditDrawingAttributesCommand(Object oldValue, Object newValue, SceneContext context, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
		this.context = context;
	}

	public EditDrawingAttributesCommand() {

	}
}
