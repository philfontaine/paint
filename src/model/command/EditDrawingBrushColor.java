package model.command;

import model.Color;

public class EditDrawingBrushColor extends EditDrawingCommand {
	public EditDrawingBrushColor() {
	}

	public EditDrawingBrushColor(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getDrawingOptions().setBrushColor((Color) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getDrawingOptions().setBrushColor((Color) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Brush Color";
	}
}