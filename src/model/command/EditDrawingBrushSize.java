package model.command;

public class EditDrawingBrushSize extends EditDrawingCommand {
	public EditDrawingBrushSize() {
	}

	public EditDrawingBrushSize(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getDrawingOptions().setBrushSize((Double) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getDrawingOptions().setBrushSize((Double) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Brush Size";
	}
}