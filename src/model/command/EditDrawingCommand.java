package model.command;

import javax.xml.bind.annotation.XmlElement;

import model.DrawingModel;

public abstract class EditDrawingCommand extends Command {
	@XmlElement
	protected Object oldValue;
	@XmlElement
	protected Object newValue;
	@XmlElement
	private int drawingIndex;

	public EditDrawingCommand() {

	}

	public EditDrawingCommand(Object oldValue, Object newValue, int drawingIndex) {
		this.oldValue = oldValue;
		this.newValue = newValue;
		this.drawingIndex = drawingIndex;
	}
	
	protected DrawingModel getDrawing() {
		return list.getList().get(drawingIndex);
	}
	
	protected int getDrawingIndex() {
		return drawingIndex;
	}
}
