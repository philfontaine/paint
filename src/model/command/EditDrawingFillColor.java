package model.command;

import model.Color;

public class EditDrawingFillColor extends EditDrawingCommand {
	public EditDrawingFillColor() {

	}

	public EditDrawingFillColor(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getDrawingOptions().setFillColor((Color) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getDrawingOptions().setFillColor((Color) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Fill Color";
	}
}
