package model.command;

import model.Point2D;

public class EditDrawingFinalPoint extends EditDrawingCommand {

	public EditDrawingFinalPoint() {

	}

	public EditDrawingFinalPoint(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	protected void executeCommand() {
		getDrawing().updateFinalPoint((Point2D) newValue);
	}

	@Override
	public void undo() {
		getDrawing().updateFinalPoint((Point2D) oldValue);
	}

	@Override
	public String toString() {
		return ("Edit Anchor");
	}
}
