package model.command;

import model.SceneContext;

public class EditDrawingHeight extends EditDrawingAttributesCommand {

	public EditDrawingHeight(Object oldValue, Object newValue, SceneContext context, int drawingIndex) {
		super(oldValue, newValue, context, drawingIndex);
	}

	public EditDrawingHeight() {

	}

	@Override
	public void executeCommand() {
		getDrawing().updateHeight((double) newValue, context.getHeight());
	}

	@Override
	public void undo() {
		getDrawing().updateHeight((double) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Height";
	}
}
