package model.command;

import model.Point2D;

public class EditDrawingInitialPoint extends EditDrawingCommand {

	public EditDrawingInitialPoint(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	protected void executeCommand() {
		getDrawing().updateInitialPoint((Point2D) newValue);
	}

	@Override
	public void undo() {
		getDrawing().updateInitialPoint((Point2D) oldValue);
	}

	@Override
	public String toString() {
		return ("Edit Anchor");
	}
}
