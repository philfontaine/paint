package model.command;

public class EditDrawingRotation extends EditDrawingCommand {
	public EditDrawingRotation() {

	}

	public EditDrawingRotation(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().updateRotation((double) newValue);
	}

	@Override
	public void undo() {
		getDrawing().updateRotation((double) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Rotation";
	}
}