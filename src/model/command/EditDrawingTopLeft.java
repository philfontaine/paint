package model.command;

import model.Point2D;
import model.SceneContext;

public class EditDrawingTopLeft extends EditDrawingAttributesCommand {

	public EditDrawingTopLeft(Object oldValue, Object newValue, SceneContext context, int drawingIndex) {
		super(oldValue, newValue, context, drawingIndex);
	}

	public EditDrawingTopLeft() {

	}

	@Override
	public void executeCommand() {
		getDrawing().updateTopLeft((Point2D) newValue, context);
	}

	@Override
	public void undo() {
		getDrawing().updateTopLeft((Point2D) oldValue);
	}

	@Override
	public String toString() {
		return "Move Drawing";
	}
}
