package model.command;

import model.SceneContext;

public class EditDrawingWidth extends EditDrawingAttributesCommand {

	public EditDrawingWidth(Object oldValue, Object newValue, SceneContext context, int drawingIndex) {
		super(oldValue, newValue, context, drawingIndex);
	}

	public EditDrawingWidth() {

	}

	@Override
	public void executeCommand() {
		getDrawing().updateWidth((double) newValue, context.getWidth());
	}

	@Override
	public void undo() {
		getDrawing().updateWidth((double) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Width";
	}
}