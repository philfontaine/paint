package model.command;

public class EditText extends EditDrawingCommand {
	public EditText() {
	}

	public EditText(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getTextModel().setText((String) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getTextModel().setText((String) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Text";
	}
}