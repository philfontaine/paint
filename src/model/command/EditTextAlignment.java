package model.command;

public class EditTextAlignment extends EditDrawingCommand {
	public EditTextAlignment() {
	}

	public EditTextAlignment(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getTextModel().setAlignment((String) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getTextModel().setAlignment((String) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Text Alignment";
	}
}