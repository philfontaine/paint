package model.command;

import model.Color;

public class EditTextColor extends EditDrawingCommand {
	public EditTextColor() {
	}

	public EditTextColor(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getTextModel().setColor((Color) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getTextModel().setColor((Color) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Font Color";
	}
}