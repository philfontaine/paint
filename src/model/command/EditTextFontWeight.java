package model.command;

public class EditTextFontWeight extends EditDrawingCommand {
	public EditTextFontWeight() {
	}

	public EditTextFontWeight(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getTextModel().setFontWeight((String) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getTextModel().setFontWeight((String) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Text Font Weightt";
	}
}