package model.command;

public class EditTextSize extends EditDrawingCommand {
	public EditTextSize() {
	}

	public EditTextSize(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getTextModel().setSize((Double) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getTextModel().setSize((Double) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Font Size";
	}
}