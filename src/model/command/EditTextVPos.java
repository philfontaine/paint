package model.command;

public class EditTextVPos extends EditDrawingCommand {
	public EditTextVPos() {
	}

	public EditTextVPos(Object oldValue, Object newValue, int drawingIndex) {
		super(oldValue, newValue, drawingIndex);
	}

	@Override
	public void executeCommand() {
		getDrawing().getTextModel().setVerticalPosition((String) newValue);
	}

	@Override
	public void undo() {
		getDrawing().getTextModel().setVerticalPosition((String) oldValue);
	}

	@Override
	public String toString() {
		return "Edit Text Vertical Position";
	}
}