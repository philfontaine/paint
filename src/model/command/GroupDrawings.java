package model.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlElement;

import model.DrawingModel;
import model.shape.DrawingGroupModel;

public class GroupDrawings extends Command {

	@XmlElement
	private ArrayList<Integer> toGroup;

	private DrawingGroupModel shapeGroup;

	private TreeMap<Integer, DrawingModel> drawingMap = new TreeMap<>();

	public GroupDrawings() {

	}

	public GroupDrawings(List<Integer> toGroup) {
		this.toGroup = (ArrayList<Integer>) toGroup;
	}

	@Override
	protected void executeCommand() {
		// We need at least 2 entities to create a group.

		if (toGroup.size() > 1) {
			shapeGroup = new DrawingGroupModel();

			ArrayList<DrawingModel> grouped = new ArrayList<>();
			for (int index : toGroup) {
				DrawingModel m = list.get(index);
				grouped.add(m);
				drawingMap.put(index, m);
			}

			// Remove separate entities
			for (DrawingModel selection : grouped) {
				list.remove(selection);
				shapeGroup.addChild(selection);
			}
			shapeGroup.select();
			list.add(shapeGroup);
		}

	}

	@Override
	public void undo() {
		if (shapeGroup != null) {
			list.remove(shapeGroup);

			for (Map.Entry<Integer, DrawingModel> entry : drawingMap.entrySet()) {
				int index = entry.getKey();
				DrawingModel model = entry.getValue();

				list.insertAtIndex(model, index);
			}

			drawingMap.clear();
		}
	}

	@Override
	public String toString() {
		return "Group Drawings";
	}

}
