package model.command;

import javax.xml.bind.annotation.XmlElement;
import model.DrawingModel;

public class ImportImageCommand extends Command {

	@XmlElement
	private DrawingModel drawingModel;
	
	public ImportImageCommand() {}
	
	public ImportImageCommand(DrawingModel drawingModel) {
		this.drawingModel = drawingModel;
	}
	
	@Override
	protected void executeCommand() {
		list.add(drawingModel);	
	}
	
	@Override
	public void undo() {
		list.remove(drawingModel);
	}
	
	@Override
	public String toString() {
		return "Import Image";
	}

}

