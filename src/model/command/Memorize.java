package model.command;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import model.DrawingModel;

@XmlRootElement(name = "Memorize")
@XmlAccessorType(XmlAccessType.FIELD)
public class Memorize extends Command {

	private DrawingModel oldMemo;

	@XmlElement
	private int index;

	public Memorize() {

	}

	public Memorize(int index) {
		this.index = index;
	}

	@Override
	protected void executeCommand() {
		oldMemo = list.getMemorized();
		list.memorize(index);
	}

	@Override
	public void undo() {
		list.setMemorized(oldMemo);
	}

	@Override
	public String toString() {
		return "Memorize";
	}

}
