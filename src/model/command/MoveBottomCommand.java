package model.command;

import javax.xml.bind.annotation.XmlElement;

import model.DrawingModel;

public class MoveBottomCommand extends Command {
	private DrawingModel model;
	@XmlElement
	private int position;

	@Override
	protected void executeCommand() {
		model = list.getCurrent();
		this.position = list.getList().indexOf(model);
		list.moveBottom(model);
	}

	@Override
	public void undo() {
		list.moveTo(model, position);
	}

	@Override
	public String toString() {
		return "Move Bottom";
	}

}