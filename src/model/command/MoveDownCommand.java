package model.command;

import model.DrawingModel;

public class MoveDownCommand extends Command {

	DrawingModel model;

	@Override
	protected void executeCommand() {
		this.model = list.getCurrent();
		list.moveDown(model);
	}

	@Override
	public void undo() {
		list.moveUp(model);
	}

	@Override
	public String toString() {
		return "Move Down";
	}
}
