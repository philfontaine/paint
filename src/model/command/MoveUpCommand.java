package model.command;

import model.DrawingModel;

public class MoveUpCommand extends Command {
	private DrawingModel model;

	@Override
	protected void executeCommand() {
		this.model = list.getCurrent();
		list.moveUp(model);
	}

	@Override
	public void undo() {
		list.moveDown(model);
	}

	@Override
	public String toString() {
		return "Move Up";
	}
}
