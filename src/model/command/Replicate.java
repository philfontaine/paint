package model.command;

import model.DrawingModel;

public class Replicate extends Command {

	private DrawingModel replicated;

	@Override
	protected void executeCommand() {
		replicated = list.replicate();
	}

	@Override
	public void undo() {
		if (replicated != null) {
			list.remove(replicated);
		}
	}

	@Override
	public String toString() {
		return "Replicate";
	}

}
