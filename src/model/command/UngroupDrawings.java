package model.command;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import model.DrawingModel;
import model.shape.DrawingGroupModel;

public class UngroupDrawings extends Command {

	@XmlElement
	private ArrayList<Integer> toUnGroup;

	private ArrayList<DrawingGroupModel> ungrouped = new ArrayList<>();

	public UngroupDrawings() {

	}

	public UngroupDrawings(List<Integer> toUnGroup) {
		this.toUnGroup = (ArrayList<Integer>) toUnGroup;
	}

	@Override
	protected void executeCommand() {

		ArrayList<DrawingModel> ungroup = new ArrayList<>();
		for (int index : toUnGroup) {
			ungroup.add(list.get(index));
		}

		// Separate entities
		for (DrawingModel selection : ungroup) {
			if (selection instanceof DrawingGroupModel) {
				ungrouped.add((DrawingGroupModel) selection);

				list.remove(selection);

				ArrayList<DrawingModel> subControllers = (ArrayList<DrawingModel>) ((DrawingGroupModel) selection)
						.getChildren();
				for (DrawingModel model : subControllers) {
					list.add(model);
					model.select();
				}
			}
		}

	}

	@Override
	public void undo() {
		for (DrawingGroupModel group : ungrouped) {
			for (DrawingModel model : group.getChildren()) {
				list.remove(model);
			}
			list.add(group);
		}
	}

	@Override
	public String toString() {
		return "Ungroup Drawings";
	}

}
