package model.shape;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import helpers.RatioConverter;
import model.DrawingModel;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;

@XmlAccessorType(XmlAccessType.NONE)
public class BrushModel extends DrawingModel {
	private double lastX;
	private double lastY;
	private double last2X;
	private double last2Y;

	private List<Point2D> points = new ArrayList<>();
	@XmlElement
	private ArrayList<Point2D> ratioPoints = null;

	public BrushModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptions) {
		super(pt, context, drawingOptions);
		lastX = pt.getX();
		lastY = pt.getY();
		last2X = pt.getX();
		last2Y = pt.getY();
		points.add(new Point2D(last2X, last2Y));
		points.add(new Point2D(lastX, lastY));
	}

	public BrushModel() {
		super();
	}

	public BrushModel(BrushModel other) {
		super(other);
		for (Point2D pt : other.points) {
			this.points.add(new Point2D(pt));
		}
	}

	@Override
	public void update(Point2D pt, SceneContext context) {
		double x = pt.getX();

		if (x > getXf()) {
			updateFinalPoint(new Point2D(x, this.getYf()), context);
		} else if (x < getXi()) {
			updateInitialPoint(new Point2D(x, this.getYi()), context);
		}

		double y = pt.getY();

		if (y > getYf()) {
			updateFinalPoint(new Point2D(this.getXf(), y), context);
		} else if (y < getYi()) {
			updateInitialPoint(new Point2D(this.getXi(), y), context);
		}

		last2X = lastX;
		last2Y = lastY;
		lastX = x;
		lastY = y;
		points.add(new Point2D(lastX, lastY));

		notifyObservers();
	}

	public double getLastX() {
		return lastX;
	}

	public double getLastY() {
		return lastY;
	}

	public double getLast2X() {
		return last2X;
	}

	public double getLast2Y() {
		return last2Y;
	}

	public List<Point2D> getPoints() {
		if (ratioPoints == null) {
			return points;
		}
		return getPointsFromRatio();
	}

	@Override
	public void finish() {
		if (ratioPoints == null) {
			setPointsInRatio();
		}
	}

	private void setPointsInRatio() {
		double ratioX;
		double ratioY;
		ratioPoints = new ArrayList<>();
		for (Point2D p : points) {
			ratioX = RatioConverter.valueToRatio(p.getX(), getXi(), getXf());
			ratioY = RatioConverter.valueToRatio(p.getY(), getYi(), getYf());
			ratioPoints.add(new Point2D(ratioX, ratioY));
		}
	}

	private List<Point2D> getPointsFromRatio() {
		double x;
		double y;
		List<Point2D> pointsFromRatio = new ArrayList<>();
		for (Point2D p : ratioPoints) {
			x = RatioConverter.ratioToValue(p.getX(), getXi(), getXf());
			y = RatioConverter.ratioToValue(p.getY(), getYi(), getYf());
			pointsFromRatio.add(new Point2D(x, y));
		}
		return pointsFromRatio;
	}

	@Override
	public String toString() {
		return ("Brush");
	}
}
