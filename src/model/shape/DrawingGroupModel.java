package model.shape;

import java.util.ArrayList;
import java.util.List;

import helpers.RatioConverter;
import model.DrawingModel;
import model.DrawingModelFactory;
import model.Point2D;
import model.SceneContext;

public class DrawingGroupModel extends DrawingModel {

	ArrayList<DrawingModel> children = new ArrayList<>();

	public DrawingGroupModel() {
		super();
	}

	public DrawingGroupModel(DrawingGroupModel other) {
		super(other);
		for (DrawingModel child : other.children) {
			this.children.add(DrawingModelFactory.getModel(child));
		}
		computeModelAttributes();
	}

	public void addChild(DrawingModel child) {
		if (!children.contains(child)) {
			children.add(child);
			computeModelAttributes();
		}
	}

	@Override
	public void update(Point2D pt, SceneContext context) {
		// Not supported
	}

	@Override
	public double getBrushSize() {
		double maxSize = 0;

		for (DrawingModel m : children) {
			double brushSize = m.getDrawingOptions().getBrushSize();
			if (brushSize > maxSize) {
				maxSize = brushSize;
			}
		}

		return maxSize;
	}

	@Override
	public void updateWidth(double w, double cw) {
		notifyObservers();
	}

	@Override
	public void updateHeight(double h, double ch) {
		notifyObservers();
	}

	@Override
	public void select() {
		super.select();
		for (DrawingModel m : children) {
			m.deselect();
		}
	}

	@Override
	public void deselect() {
		super.deselect();
		for (DrawingModel m : children) {
			m.deselect();
		}
	}

	public void computeModelAttributes() {
		this.getDrawingOptions().setBrushSize(getBrushSize());

		double infinity = Double.POSITIVE_INFINITY;

		double minX = infinity;
		double minY = infinity;
		double maxX = 0;
		double maxY = 0;

		for (DrawingModel model : children) {
			if (model.getTopLeftX() < minX) {
				minX = model.getTopLeftX();
			}

			if (model.getTopLeftY() < minY) {
				minY = model.getTopLeftY();
			}

			if (model.getTopLeftX() + model.getWidth() > maxX) {
				maxX = model.getTopLeftX() + model.getWidth();
			}

			if (model.getTopLeftY() + model.getHeight() > maxY) {
				maxY = model.getTopLeftY() + model.getHeight();
			}
		}

		super.updateInitialPoint(new Point2D(minX, minY), new SceneContext(infinity, infinity));
		super.updateFinalPoint(new Point2D(maxX, maxY), new SceneContext(infinity, infinity));
	}

	@Override
	public void updateInitialPoint(Point2D pt, SceneContext context) {
		Point2D previousInitial = new Point2D(getPi());
		Point2D previousFinal = new Point2D(getPf());
		super.updateInitialPoint(pt, context);
		Point2D nextInitial = new Point2D(getPi());
		Point2D nextFinal = new Point2D(getPf());
		scaleSubDrawing(previousInitial, previousFinal, nextInitial, nextFinal);
	}

	@Override
	public void updateFinalPoint(Point2D pt, SceneContext context) {
		Point2D previousInitial = new Point2D(getPi());
		Point2D previousFinal = new Point2D(getPf());
		super.updateFinalPoint(pt, context);
		Point2D nextInitial = new Point2D(getPi());
		Point2D nextFinal = new Point2D(getPf());
		scaleSubDrawing(previousInitial, previousFinal, nextInitial, nextFinal);
	}

	@Override
	public void updateTopLeft(Point2D pt, SceneContext context) {

		double x = setLocInBoundaries(pt.getX(), this.getWidth(), context.getWidth());
		double y = setLocInBoundaries(pt.getY(), this.getHeight(), context.getHeight());
		pt = new Point2D(x, y);

		Point2D delta = pt.subtract(getTopLeft());

		for (DrawingModel model : children) {
			model.updateTopLeft(model.getTopLeft().add(delta), context);
		}

		super.updateTopLeft(pt, context);
	}

	public List<DrawingModel> getChildren() {
		return children;
	}

	public void setChildren(List<DrawingModel> children) {
		this.children = (ArrayList<DrawingModel>) children;
	}

	@Override
	public String toString() {
		return ("ShapeGroup");
	}
	
	
	private void scaleSubDrawing(Point2D previousInitial, Point2D previousFinal, Point2D nextInitial, Point2D nextFinal) {
		double ratioInitX;
		double ratioInitY;
		double ratioFinalX;
		double ratioFinalY;
		for (DrawingModel m : children) {
			ratioInitX = RatioConverter.valueToRatio(m.getXi(), previousInitial.getX(), previousFinal.getX());
			ratioInitY = RatioConverter.valueToRatio(m.getYi(), previousInitial.getY(), previousFinal.getY());
			ratioFinalX = RatioConverter.valueToRatio(m.getXf(), previousInitial.getX(), previousFinal.getX());
			ratioFinalY = RatioConverter.valueToRatio(m.getYf(), previousInitial.getY(), previousFinal.getY());

			m.getPi().setX(RatioConverter.ratioToValue(ratioInitX, nextInitial.getX(), nextFinal.getX()));
			m.getPi().setY(RatioConverter.ratioToValue(ratioInitY, nextInitial.getY(), nextFinal.getY()));
			m.getPf().setX(RatioConverter.ratioToValue(ratioFinalX, nextInitial.getX(), nextFinal.getX()));
			m.getPf().setY(RatioConverter.ratioToValue(ratioFinalY, nextInitial.getY(), nextFinal.getY()));
		}
	}
	
	
}
