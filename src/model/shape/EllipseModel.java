package model.shape;

import helpers.SvgHelper;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.svg.DrawingSvg;
import model.shape.svg.EllipseSvg;

public class EllipseModel extends ShapeModel {
	public EllipseModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptions) {
		super(pt, context, drawingOptions);
	}

	public EllipseModel() {
		super();
	}

	public EllipseModel(EllipseModel other) {
		super(other);
	}

	@Override
	public DrawingSvg getDrawingSvg() {
		EllipseSvg ellipseSvg = new EllipseSvg();
		ellipseSvg.setWidth((int) Math.round(getWidth()));
		ellipseSvg.setHeight((int) Math.round(getHeight()));
		ellipseSvg.setX((int) Math.round(getTopLeftX()));
		ellipseSvg.setY((int) Math.round(getTopLeftY()));
		ellipseSvg.setStyle(SvgHelper.getStyle(getDrawingOptions()));
		return ellipseSvg;
	}

	@Override
	public String toString() {
		return ("Ellipse");
	}
}
