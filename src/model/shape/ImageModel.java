package model.shape;

import java.awt.image.BufferedImage;

import model.DrawingModel;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;

public class ImageModel extends DrawingModel {
	private BufferedImage bufferedImage;

	public ImageModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptionsModel,
			BufferedImage bufferedImage) {
		super(pt, context, drawingOptionsModel);
		this.bufferedImage = bufferedImage;
		updateFinalPoint(new Point2D(bufferedImage.getWidth(), bufferedImage.getHeight()), context);
	}

	public ImageModel() {
		super();
	}

	public ImageModel(ImageModel other) {
		super(other);
		this.bufferedImage = other.bufferedImage;
		updateFinalPoint(new Point2D(bufferedImage.getWidth(), bufferedImage.getHeight()));
	}

	@Override
	public String toString() {
		return ("Image");
	}

	@Override
	public void update(Point2D pt, SceneContext context) {
		updateFinalPoint(pt, context);
	}

	public BufferedImage getBufferedImage() {
		return bufferedImage;
	}
}
