package model.shape;

import helpers.SvgHelper;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.svg.DrawingSvg;
import model.shape.svg.LineSvg;

public class LineModel extends ShapeModel {
	public LineModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptions) {
		super(pt, context, drawingOptions);
	}

	public LineModel() {
		super();
	}

	public LineModel(LineModel other) {
		super(other);
	}

	@Override
	public DrawingSvg getDrawingSvg() {
		LineSvg lineSvg = new LineSvg();
		lineSvg.setX1((int) Math.round(getXi()));
		lineSvg.setY1((int) Math.round(getYi()));
		lineSvg.setX2((int) Math.round(getXf()));
		lineSvg.setY2((int) Math.round(getYf()));
		lineSvg.setStyle(SvgHelper.getStyle(getDrawingOptions()));
		return lineSvg;
	}

	@Override
	public String toString() {
		return ("Line");
	}
}
