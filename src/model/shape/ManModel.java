package model.shape;

import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;

public class ManModel extends ShapeModel {
	public ManModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptions) {
		super(pt, context, drawingOptions);
	}

	public ManModel() {
		super();
	}

	public ManModel(ManModel other) {
		super(other);
	}

	@Override
	public String toString() {
		return ("Man");
	}
}