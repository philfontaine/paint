package model.shape;

import helpers.SvgHelper;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.svg.DrawingSvg;
import model.shape.svg.RectangleSvg;

public class RectangleModel extends ShapeModel {
	public RectangleModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptions) {
		super(pt, context, drawingOptions);
	}

	public RectangleModel() {
		super();
	}

	public RectangleModel(RectangleModel other) {
		super(other);
	}

	@Override
	public DrawingSvg getDrawingSvg() {
		RectangleSvg rectangleSvg = new RectangleSvg();
		rectangleSvg.setWidth((int) Math.round(getWidth()));
		rectangleSvg.setHeight((int) Math.round(getHeight()));
		rectangleSvg.setX((int) Math.round(getTopLeftX()));
		rectangleSvg.setY((int) Math.round(getTopLeftY()));
		rectangleSvg.setStyle(SvgHelper.getStyle(getDrawingOptions()));
		return rectangleSvg;
	}

	@Override
	public String toString() {
		return ("Rectangle");
	}
}
