package model.shape;

import model.DrawingModel;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;

public abstract class ShapeModel extends DrawingModel {

	public ShapeModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptions) {
		super(pt, context, drawingOptions);
	}

	public ShapeModel() {
		super();
	}

	public ShapeModel(ShapeModel other) {
		super(other);
	}

	@Override
	public void update(Point2D pt, SceneContext context) {
		updateFinalPoint(pt, context);
	}
}
