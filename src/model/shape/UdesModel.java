package model.shape;

import javax.xml.bind.annotation.XmlRootElement;

import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;

@XmlRootElement
public class UdesModel extends ShapeModel {
	public UdesModel(Point2D pt, SceneContext context, DrawingOptionsModel drawingOptions) {
		super(pt, context, drawingOptions);
	}

	public UdesModel() {
		super();
	}

	public UdesModel(UdesModel other) {
		super(other);
	}

	@Override
	public String toString() {
		return ("UdeS");
	}
}
