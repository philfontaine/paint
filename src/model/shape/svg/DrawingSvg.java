package model.shape.svg;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;

import helpers.ColorHelper;
import model.DrawingModel;
import model.DrawingOptionsModel;

@XmlSeeAlso({ RectangleSvg.class, EllipseSvg.class, LineSvg.class })
public abstract class DrawingSvg {
	@XmlAttribute
	protected String style = "";

	public void setStyle(String style) {
		this.style = style;
	}

	public DrawingModel getDrawingModel() {
		return null;
	}

	protected DrawingOptionsModel getDrawingOptionsModel(String style) {
		DrawingOptionsModel drawingOptionsModel = new DrawingOptionsModel();
		String[] styles = style.split(";");
		for (String s : styles) {
			String[] styleKeyValue = s.split(":");
			if (styleKeyValue.length == 2) {
				String key = styleKeyValue[0].trim();
				String value = styleKeyValue[1].trim();
				switch (key) {
				case "fill":
					drawingOptionsModel.setFillColor(ColorHelper.fromSvgColor(value));
					break;
				case "stroke":
					drawingOptionsModel.setBrushColor(ColorHelper.fromSvgColor(value));
					break;
				case "stroke-width":
					drawingOptionsModel.setBrushSize(Double.valueOf(value));
					break;
				default:
					break;
				}
			}
		}
		return drawingOptionsModel;
	}
}
