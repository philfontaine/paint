package model.shape.svg;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "svg")
public class DrawingSvgList {
	@XmlElementRef
	public List<DrawingSvg> list = new ArrayList<>();
}
