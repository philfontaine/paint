package model.shape.svg;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import model.DrawingModel;
import model.Point2D;
import model.shape.EllipseModel;

@XmlRootElement(name = "ellipse")
public class EllipseSvg extends DrawingSvg {
	@XmlAttribute
	private int cx;
	@XmlAttribute
	private int cy;
	@XmlAttribute
	private int rx;
	@XmlAttribute
	private int ry;

	@Override
	public DrawingModel getDrawingModel() {
		EllipseModel ellipseModel = new EllipseModel();
		ellipseModel.updateTopLeft(new Point2D(cx - rx, cy - ry));
		ellipseModel.updateWidth(rx * 2.0);
		ellipseModel.updateHeight(ry * 2.0);
		ellipseModel.setDrawingOptions(getDrawingOptionsModel(style));
		return ellipseModel;
	}

	public void setCx(int cx) {
		this.cx = cx;
	}

	public void setCy(int cy) {
		this.cy = cy;
	}

	public void setRx(int rx) {
		this.rx = rx;
	}

	public void setRy(int ry) {
		this.ry = ry;
	}

	public void setX(int x) {
		this.cx = x + this.rx;
	}

	public void setY(int y) {
		this.cy = y + this.ry;
	}

	public void setWidth(int width) {
		int x = cx - rx;
		this.rx = width / 2;
		this.cx = this.rx + x / 2;
	}

	public void setHeight(int height) {
		int y = cy - ry;
		this.ry = height / 2;
		this.cy = this.ry + y / 2;
	}

}
