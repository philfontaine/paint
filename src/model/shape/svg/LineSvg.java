package model.shape.svg;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import model.DrawingModel;
import model.Point2D;
import model.shape.LineModel;

@XmlRootElement(name = "line")
public class LineSvg extends DrawingSvg {
	@XmlAttribute
	private int x1;
	@XmlAttribute
	private int y1;
	@XmlAttribute
	private int x2;
	@XmlAttribute
	private int y2;

	@Override
	public DrawingModel getDrawingModel() {
		LineModel lineModel = new LineModel();
		lineModel.updateInitialPoint(new Point2D(x1, y1));
		lineModel.updateFinalPoint(new Point2D(x2, y2));
		lineModel.setDrawingOptions(getDrawingOptionsModel(style));
		return lineModel;
	}

	public void setX1(int x1) {
		this.x1 = x1;
	}

	public void setY1(int y1) {
		this.y1 = y1;
	}

	public void setX2(int x2) {
		this.x2 = x2;
	}

	public void setY2(int y2) {
		this.y2 = y2;
	}

}
