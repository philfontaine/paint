package model.shape.svg;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import model.DrawingModel;
import model.Point2D;
import model.shape.RectangleModel;

@XmlRootElement(name = "rect")
public class RectangleSvg extends DrawingSvg {
	@XmlAttribute
	private int x;
	@XmlAttribute
	private int y;
	@XmlAttribute
	private int width;
	@XmlAttribute
	private int height;

	@Override
	public DrawingModel getDrawingModel() {
		RectangleModel rectangleModel = new RectangleModel();
		rectangleModel.updateTopLeft(new Point2D(x, y));
		rectangleModel.updateWidth(width);
		rectangleModel.updateHeight(height);
		rectangleModel.setDrawingOptions(getDrawingOptionsModel(style));
		return rectangleModel;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
