@XmlSchema(namespace = "http://www.w3.org/2000/svg", xmlns = {
		@XmlNs(prefix = "", namespaceURI = "http://www.w3.org/2000/svg") }, elementFormDefault = XmlNsForm.QUALIFIED)

package model.shape.svg;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
