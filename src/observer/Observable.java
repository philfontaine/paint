package observer;

import java.util.ArrayList;

public class Observable {

	private ArrayList<IObserver> observers = new ArrayList<>();

	public void register(IObserver observer) {
		if (!observers.contains(observer)) {
			observers.add(observer);
		}
	}

	public void unregister(IObserver observer) {
		if (observers.contains(observer)) {
			observers.remove(observer);
		}
	}

	protected void notifyObservers() {
		for (IObserver observer : observers) {
			observer.update();
		}
	}

}
