package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.DrawingModel;

public class EyeButton extends Button{
	ImageView openView = new ImageView(new Image("image/openEye.png"));
	ImageView closedView = new ImageView(new Image("image/closedEye.png"));
	
	public EyeButton(DrawingModel model) {	
		setStyle("-fx-background-color: transparent;"); 
		setPrefHeight(10);
		setPrefWidth(10);
		setAlignment(Pos.CENTER);
		setView(model.isVisible());
		setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				model.changeVisible();
				setView(model.isVisible());
			}
		});   
	}
	
	private void setView(boolean open) {
		if(open) {
			setGraphic(openView);
		} else {
			setGraphic(closedView);
		}
	}
}
