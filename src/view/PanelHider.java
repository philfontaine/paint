package view;

import javafx.scene.Node;
import javafx.scene.layout.Pane;

public class PanelHider {

	private Pane wrapper;
	private Node panel;
	private boolean isVisible;
	
	public PanelHider(Pane w, Node p) {
		wrapper = w;
		panel = p;
		hide();
	}
	
	private void addPanel() {
		wrapper.getChildren().add(panel);
	}
	private void removePanel() {
		wrapper.getChildren().clear();
	}

	public void show() {
		if (!isVisible) {
			addPanel();
		}
		isVisible = true;
	}
	
	public void hide() {
		removePanel();
		isVisible = false;
	}
	
	public void changeState() {
		if (isVisible) {
			hide();
		} else {
			show();
		}
	}
	
	public boolean isVisible() {
		return isVisible;
	}
}
