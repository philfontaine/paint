package controller;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import controller.AttributesPanelController;
import javafx.scene.control.Spinner;
import javafx.scene.layout.Pane;
import model.DrawingModelList;
import model.command.CommandList;
import setup.JavaFXThreadingRule;

public class AttributesPanelControllerTest {

	private AttributesPanelController controller;
	private Pane pane;
	private Spinner<Double> x;
	private Spinner<Double> y;
	private Spinner<Double> w;
	private Spinner<Double> h;
	private Spinner<Double> r;
	private CommandList commandList;
	private DrawingModelList modelList;

	@Rule
	public JavaFXThreadingRule jfxRule = new JavaFXThreadingRule();

	@Before
	public void setUp() {
		double v = 42.0;
		pane = new Pane();
		x = new Spinner<>(v, v, v);
		y = new Spinner<>(v, v, v);
		w = new Spinner<>(v, v, v);
		h = new Spinner<>(v, v, v);
		r = new Spinner<>(v, v, v);
		modelList = new DrawingModelList();
		commandList = new CommandList(modelList);
		controller = new AttributesPanelController(pane, x, y, w, h, r, commandList, modelList);
	}

	@Test
	public void testModelNull() {
		controller.update();
		// assertEquals(0.0, x.getValueFactory().getValue(), 0.1);
		// assertEquals(0.0, y.getValue(), 0.1);
		// assertEquals(0.0, w.getValue(), 0.1);
		// assertEquals(0.0, h.getValue(), 0.1);
		// assertEquals(0.0, r.getValue(), 0.1);
		// assertEquals("", t.getText());
	}

}
