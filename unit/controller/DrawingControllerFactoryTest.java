package controller;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import controller.shape.BrushController;
import controller.shape.EllipseController;
import controller.shape.LineController;
import controller.shape.ManController;
import controller.shape.RectangleController;
import controller.shape.UdesController;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.BrushModel;
import model.shape.EllipseModel;
import model.shape.LineModel;
import model.shape.ManModel;
import model.shape.RectangleModel;
import model.shape.UdesModel;

public class DrawingControllerFactoryTest {
	Point2D pt = new Point2D(0, 0);
	SceneContext context = new SceneContext(0, 0);

	int id;
	DrawingOptionsModel drawingOptions = new DrawingOptionsModel();

	@Test
	public void testGetBrushController() {
		DrawingController drawingController = DrawingControllerFactory.getInstance()
				.getController(new BrushModel(pt, context, drawingOptions), context);
		assertTrue(drawingController instanceof BrushController);
	}

	@Test
	public void testGetEllipseController() {
		DrawingController drawingController = DrawingControllerFactory.getInstance().getController(new EllipseModel(),
				context);
		assertTrue(drawingController instanceof EllipseController);
	}

	@Test
	public void testGetLineController() {
		DrawingController drawingController = DrawingControllerFactory.getInstance().getController(new LineModel(),
				context);
		assertTrue(drawingController instanceof LineController);
	}

	@Test
	public void testGetManController() {
		DrawingController drawingController = DrawingControllerFactory.getInstance().getController(new ManModel(),
				context);
		assertTrue(drawingController instanceof ManController);
	}

	@Test
	public void testGetRectangleController() {
		DrawingController drawingController = DrawingControllerFactory.getInstance().getController(new RectangleModel(),
				context);
		assertTrue(drawingController instanceof RectangleController);
	}

	@Test
	public void testGetUdesController() {
		DrawingController drawingController = DrawingControllerFactory.getInstance().getController(new UdesModel(),
				context);
		assertTrue(drawingController instanceof UdesController);
	}

}
