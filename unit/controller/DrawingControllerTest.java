package controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

import controller.DrawingController;
import controller.shape.RectangleController;
import model.Color;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.RectangleModel;

public class DrawingControllerTest {

	private RectangleModel model;
	private static Point2D pti = new Point2D(10, 11);
	private static SceneContext context = new SceneContext(200, 200);

	private static double brushSize = 8;

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel();
	Color contourColor = new Color(120, 121, 122);
	Color fillColor = new Color(123, 124, 125);

	@Before
	public void setUp() throws Exception {
		drawingOptions.setBrushColor(contourColor);
		drawingOptions.setBrushSize(brushSize);
		drawingOptions.setFillColor(fillColor);

		model = new RectangleModel(pti, context, drawingOptions);
		model.update(pti, context);
	}

	@Test
	public void testDrawingController() {
		DrawingController controller = new RectangleController(model);
		assertEquals(controller.getModel(), model);
		// TODO : Test if the observer registration has worked.
	}

	@Test
	public void testGetSetModel() {
		DrawingController controller = new RectangleController(model);
		RectangleModel anotherModel = new RectangleModel();

		assertNotEquals(model, anotherModel);

		controller.setModel(anotherModel);
		assertEquals(anotherModel, controller.getModel());
	}
}
