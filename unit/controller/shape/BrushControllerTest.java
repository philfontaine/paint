package controller.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import model.Color;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.BrushModel;

public class BrushControllerTest {

	private BrushModel model;
	private static Point2D pti = new Point2D(10, 11);
	private static Point2D ptf = new Point2D(50, 61);
	private static SceneContext context = new SceneContext(200, 200);
	private static double brushSize = 8;

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel();
	Color contourColor = new Color(120, 121, 122);
	Color fillColor = new Color(123, 124, 125);

	@Before
	public void setUp() throws Exception {
		drawingOptions.setBrushColor(contourColor);
		drawingOptions.setBrushSize(brushSize);
		drawingOptions.setFillColor(fillColor);

		model = new BrushModel(pti, context, drawingOptions);
		model.update(ptf, context);
	}

	@Test
	public void testBrushController() {
		BrushController controller = new BrushController(model);
		BrushModel m = (BrushModel) controller.getModel();
		assertEquals(model, m);

		Node drawing = controller.getNode();
		assertTrue(drawing instanceof Pane);
	}

	@Test
	public void testDraw() {
		BrushController controller = new BrushController(model);

		controller.draw();

		Pane c = (Pane) controller.getNode();
		int expected = 2;
		int actual = c.getChildren().size();
		assertEquals(expected, actual);
	}
}
