package controller.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import controller.shape.EllipseController;
import helpers.ColorHelper;
import javafx.scene.Node;
import javafx.scene.shape.Ellipse;
import model.Color;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.EllipseModel;

public class EllipseControllerTest {

	private EllipseModel model;
	private static Point2D pti = new Point2D(10, 11);
	private static Point2D ptf = new Point2D(50, 61);
	private static SceneContext context = new SceneContext(200, 200);
	private static double brushSize = 8;
	private static double epsilon = 0.0001; // Value describes

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel();
	Color contourColor = new Color(120, 121, 122);
	Color fillColor = new Color(123, 124, 125);

	@Before
	public void setUp() throws Exception {
		drawingOptions.setBrushColor(contourColor);
		drawingOptions.setBrushSize(brushSize);
		drawingOptions.setFillColor(fillColor);

		model = new EllipseModel(pti, context, drawingOptions);
		model.update(ptf, context);
	}

	@Test
	public void testEllipseController() {
		EllipseController controller = new EllipseController(model);
		EllipseModel m = (EllipseModel) controller.getModel();
		assertEquals(model, m);

		Node drawing = controller.getNode();
		assertTrue(drawing instanceof Ellipse);
	}

	@Test
	public void testDraw() {
		EllipseController controller = new EllipseController(model);
		controller.draw();

		Ellipse r = (Ellipse) controller.getNode();

		double expected = (ptf.getX() - pti.getX()) / 2;
		double actual = r.getRadiusX();
		assertEquals(expected, actual, epsilon);

		expected = (ptf.getY() - pti.getY()) / 2;
		actual = r.getRadiusY();
		assertEquals(expected, actual, epsilon);

		expected = pti.getX() + model.getWidth() / 2;
		actual = r.getLayoutX();
		assertEquals(expected, actual, epsilon);

		expected = pti.getY() + model.getHeight() / 2;
		actual = r.getLayoutY();
		assertEquals(expected, actual, epsilon);

		Color expectedColor = contourColor;
		Color actualColor = ColorHelper.fromJavaFXColor((javafx.scene.paint.Color) r.getStroke());
		assertEquals(expectedColor.getR(), actualColor.getR());
		assertEquals(expectedColor.getG(), actualColor.getG());
		assertEquals(expectedColor.getB(), actualColor.getB());

		expectedColor = fillColor;
		actualColor = ColorHelper.fromJavaFXColor((javafx.scene.paint.Color) r.getFill());
		assertEquals(expectedColor.getR(), actualColor.getR());
		assertEquals(expectedColor.getG(), actualColor.getG());
		assertEquals(expectedColor.getB(), actualColor.getB());
	}
}
