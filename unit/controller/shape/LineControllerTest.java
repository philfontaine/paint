package controller.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import controller.shape.LineController;
import helpers.ColorHelper;
import javafx.scene.Node;
import javafx.scene.shape.Line;
import model.Color;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.LineModel;

public class LineControllerTest {

	private LineModel model;
	private static Point2D pti = new Point2D(10, 11);
	private static Point2D ptf = new Point2D(50, 61);
	private static SceneContext context = new SceneContext(200, 200);
	private static double brushSize = 8;
	private static double epsilon = 0.0001; // Value describes

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel();
	Color contourColor = new Color(120, 121, 122);
	Color fillColor = new Color(123, 124, 125);

	@Before
	public void setUp() throws Exception {
		drawingOptions.setBrushColor(contourColor);
		drawingOptions.setBrushSize(brushSize);
		drawingOptions.setFillColor(fillColor);

		model = new LineModel(pti, context, drawingOptions);
		model.update(ptf, context);
	}

	@Test
	public void testLineController() {
		LineController controller = new LineController(model);
		LineModel m = (LineModel) controller.getModel();
		assertEquals(model, m);

		Node drawing = controller.getNode();
		assertTrue(drawing instanceof Line);
	}

	@Test
	public void testDraw() {
		LineController controller = new LineController(model);

		controller.draw();

		Line l = (Line) controller.getNode();

		double expected = pti.getX();
		double actual = l.getStartX();
		assertEquals(expected, actual, epsilon);

		expected = pti.getY();
		actual = l.getStartY();
		assertEquals(expected, actual, epsilon);

		expected = ptf.getX();
		actual = l.getEndX();
		assertEquals(expected, actual, epsilon);

		expected = ptf.getY();
		actual = l.getEndY();
		assertEquals(expected, actual, epsilon);

		Color expectedColor = contourColor;
		Color actualColor = ColorHelper.fromJavaFXColor((javafx.scene.paint.Color) l.getStroke());
		assertEquals(expectedColor.getR(), actualColor.getR());
		assertEquals(expectedColor.getG(), actualColor.getG());
		assertEquals(expectedColor.getB(), actualColor.getB());

		expectedColor = fillColor;
		actualColor = ColorHelper.fromJavaFXColor((javafx.scene.paint.Color) l.getFill());
		assertEquals(expectedColor.getR(), actualColor.getR());
		assertEquals(expectedColor.getG(), actualColor.getG());
		assertEquals(expectedColor.getB(), actualColor.getB());
	}
}
