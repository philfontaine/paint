package controller.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import controller.shape.ManController;
import helpers.ColorHelper;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Path;
import model.Color;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.ManModel;

public class ManControllerTest {

	private ManModel model;
	private static Point2D pti = new Point2D(10, 11);
	private static Point2D ptf = new Point2D(50, 61);
	private static SceneContext context = new SceneContext(200, 200);
	private static double brushSize = 8;

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel();
	Color contourColor = new Color(120, 121, 122);
	Color fillColor = new Color(123, 124, 125);

	@Before
	public void setUp() throws Exception {
		drawingOptions.setBrushColor(contourColor);
		drawingOptions.setBrushSize(brushSize);
		drawingOptions.setFillColor(fillColor);

		model = new ManModel(pti, context, drawingOptions);
		model.update(ptf, context);
	}

	@Test
	public void testManController() {
		ManController controller = new ManController(model);
		ManModel m = (ManModel) controller.getModel();
		assertEquals(model, m);

		Node drawing = controller.getNode();
		assertTrue(drawing instanceof Pane);
	}

	@Test
	public void testDraw() {
		ManController controller = new ManController(model);

		controller.draw();

		Pane pane = (Pane) controller.getNode();

		Path path = (Path) pane.getChildren().get(0);

		// We ensure we drew the head and two eyes
		int expected = 3;
		int actual = pane.getChildren().size();
		assertEquals(expected, actual);

		// TODO : Test path and eyes geometrics.

		Color expectedColor = contourColor;
		Color actualColor = ColorHelper.fromJavaFXColor((javafx.scene.paint.Color) path.getStroke());
		assertEquals(expectedColor.getR(), actualColor.getR());
		assertEquals(expectedColor.getG(), actualColor.getG());
		assertEquals(expectedColor.getB(), actualColor.getB());

		expectedColor = fillColor;
		actualColor = ColorHelper.fromJavaFXColor((javafx.scene.paint.Color) path.getFill());
		assertEquals(expectedColor.getR(), actualColor.getR());
		assertEquals(expectedColor.getG(), actualColor.getG());
		assertEquals(expectedColor.getB(), actualColor.getB());
	}
}
