package controller.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import controller.shape.UdesController;
import helpers.ColorHelper;
import javafx.scene.Node;
import javafx.scene.shape.Path;
import model.Color;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.shape.UdesModel;

public class UdesControllerTest {

	private UdesModel model;
	private static Point2D pti = new Point2D(10, 11);
	private static Point2D ptf = new Point2D(50, 61);
	private static SceneContext context = new SceneContext(200, 200);
	private static double brushSize = 8;

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel();
	Color contourColor = new Color(120, 121, 122);
	Color fillColor = new Color(123, 124, 125);

	@Before
	public void setUp() throws Exception {
		drawingOptions.setBrushColor(contourColor);
		drawingOptions.setBrushSize(brushSize);
		drawingOptions.setFillColor(fillColor);

		model = new UdesModel(pti, context, drawingOptions);
		model.update(ptf, context);
	}

	@Test
	public void testUdesController() {
		UdesController controller = new UdesController(model);
		UdesModel m = (UdesModel) controller.getModel();
		assertEquals(model, m);

		Node drawing = controller.getNode();
		assertTrue(drawing instanceof Path);
	}

	@Test
	public void testDraw() {
		UdesController controller = new UdesController(model);

		controller.draw();

		Path path = (Path) controller.getNode();

		// TODO : Test path geometrics.

		Color expectedColor = contourColor;
		Color actualColor = ColorHelper.fromJavaFXColor((javafx.scene.paint.Color) path.getStroke());
		assertEquals(expectedColor.getR(), actualColor.getR());
		assertEquals(expectedColor.getG(), actualColor.getG());
		assertEquals(expectedColor.getB(), actualColor.getB());

		expectedColor = fillColor;
		actualColor = ColorHelper.fromJavaFXColor((javafx.scene.paint.Color) path.getFill());
		assertEquals(expectedColor.getR(), actualColor.getR());
		assertEquals(expectedColor.getG(), actualColor.getG());
		assertEquals(expectedColor.getB(), actualColor.getB());
	}
}
