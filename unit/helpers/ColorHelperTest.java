package helpers;

import helpers.ColorHelper;

import static org.junit.Assert.*;

import org.junit.Test;

public class ColorHelperTest {
	final int red = 50;
	final int green = 100;
	final int blue = 150;

	@Test
	public void testFromJavaFXColor() {
		javafx.scene.paint.Color javafxcolor = javafx.scene.paint.Color.rgb(red, green, blue, 0);
		model.Color modelColor = ColorHelper.fromJavaFXColor(javafxcolor);
		assertEquals(red, modelColor.getR());
		assertEquals(green, modelColor.getG());
		assertEquals(blue, modelColor.getB());
	}

	@Test
	public void testToJavaFXColor() {
		model.Color modelColor = new model.Color(red, green, blue);
		javafx.scene.paint.Color javafxcolor = ColorHelper.toJavaFXColor(modelColor);
		assertEquals(red, (int) (javafxcolor.getRed() * 255));
		assertEquals(green, (int) (javafxcolor.getGreen() * 255));
		assertEquals(blue, (int) (javafxcolor.getBlue() * 255));
	}

}
