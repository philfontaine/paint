package helpers;

import static org.junit.Assert.assertThat;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

public class ExceptionHelperTest {

	@Test
	public void testGetStackTraceAsString() {
		try {
			throw new Exception("TEST");
		} catch (Exception e) {
			assertThat(ExceptionHelper.getStackTraceAsString(e),
					CoreMatchers.containsString("java.lang.Exception: TEST"));
		}
	}

}
