package model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Color;

public class ColorTest {
	private final int defaultRed = 0;
	private final int defaultGreen = 0;
	private final int defaultBlue = 0;
	private final double defaultOpacity = 1.0;

	private final int minColorValue = 0;
	private final int maxColorValue = 255;
	private final double minOpacity = 0.0;
	private final double maxOpacity = 1.0;

	@Test
	public void testColor() {
		Color color = new Color();

		assertEquals(defaultRed, color.getR());
		assertEquals(defaultGreen, color.getG());
		assertEquals(defaultBlue, color.getB());
		assertEquals(defaultOpacity, color.getOpacity(), 0.1);
	}

	@Test
	public void testColorIntIntInt() {
		int red = 1;
		int green = 2;
		int blue = 3;
		Color color = new Color(red, green, blue);
		assertEquals(red, color.getR());
		assertEquals(green, color.getG());
		assertEquals(blue, color.getB());
		assertEquals(defaultOpacity, color.getOpacity(), 0.1);

		red = -1;
		green = -1;
		blue = -1;
		color = new Color(red, green, blue);
		assertEquals(minColorValue, color.getR());
		assertEquals(minColorValue, color.getG());
		assertEquals(minColorValue, color.getB());
		assertEquals(defaultOpacity, color.getOpacity(), 0.1);

		red = 300;
		green = 300;
		blue = 300;
		color = new Color(red, green, blue);
		assertEquals(maxColorValue, color.getR());
		assertEquals(maxColorValue, color.getG());
		assertEquals(maxColorValue, color.getB());
		assertEquals(defaultOpacity, color.getOpacity(), 0.1);
	}

	@Test
	public void testColorIntIntIntDouble() {
		int red = 1;
		int green = 2;
		int blue = 3;
		double opacity = 0.5;
		Color color = new Color(red, green, blue, opacity);
		assertEquals(red, color.getR());
		assertEquals(green, color.getG());
		assertEquals(blue, color.getB());
		assertEquals(opacity, color.getOpacity(), 0.1);

		red = -1;
		green = -1;
		blue = -1;
		opacity = -1;
		color = new Color(red, green, blue, opacity);
		assertEquals(minColorValue, color.getR());
		assertEquals(minColorValue, color.getG());
		assertEquals(minColorValue, color.getB());
		assertEquals(minOpacity, color.getOpacity(), 0.1);

		red = 300;
		green = 300;
		blue = 300;
		opacity = 2;
		color = new Color(red, green, blue, opacity);
		assertEquals(maxColorValue, color.getR());
		assertEquals(maxColorValue, color.getG());
		assertEquals(maxColorValue, color.getB());
		assertEquals(maxOpacity, color.getOpacity(), 0.1);
	}

	@Test
	public void testSetGetRed() {
		Color color = new Color();

		int red = 34;
		color.setR(red);
		assertEquals(red, color.getR());

		red = -1;
		color.setR(red);
		assertEquals(minColorValue, color.getR());

		red = 500;
		color.setR(red);
		assertEquals(maxColorValue, color.getR());
	}

	@Test
	public void testSetGetGreen() {
		Color color = new Color();

		int green = 34;
		color.setG(green);
		assertEquals(green, color.getG());

		green = -1;
		color.setG(green);
		assertEquals(minColorValue, color.getG());

		green = 500;
		color.setG(green);
		assertEquals(maxColorValue, color.getG());
	}

	@Test
	public void testSetGetBlue() {
		Color color = new Color();

		int blue = 34;
		color.setB(blue);
		assertEquals(blue, color.getB());

		blue = -1;
		color.setB(blue);
		assertEquals(minColorValue, color.getB());

		blue = 500;
		color.setB(blue);
		assertEquals(maxColorValue, color.getB());
	}

	@Test
	public void testSetGetOpacity() {
		Color color = new Color();

		double opacity = 0.5;
		color.setOpacity(opacity);
		assertEquals(opacity, color.getOpacity(), 0.1);

		opacity = -1;
		color.setOpacity(opacity);
		assertEquals(minOpacity, color.getOpacity(), 0.1);

		opacity = 500;
		color.setOpacity(opacity);
		assertEquals(maxOpacity, color.getOpacity(), 0.1);
	}

}
