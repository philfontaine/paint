package model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Color;
import model.DrawingAttributesModel;
import model.DrawingMode;
import model.DrawingOptionsModel;

public class DrawingAttributesModelTest {

	@Test
	public void test() {
		final double brushSize = 12;
		final int brushColorR = 34;
		final int brushColorG = 56;
		final int brushColorB = 78;
		final int fillColorR = 91;
		final int fillColorG = 23;
		final int fillColorB = 45;
		final String expectedDrawingMode = DrawingMode.RECTANGLE;
		Color brushColor = new Color(brushColorR, brushColorG, brushColorB);
		Color fillColor = new Color(fillColorR, fillColorG, fillColorB);
		DrawingOptionsModel drawingOptionsModel;
		DrawingAttributesModel drawingAttributesModel;

		// Store values
		drawingOptionsModel = new DrawingOptionsModel(brushSize, brushColor, fillColor);
		drawingAttributesModel = new DrawingAttributesModel();
		drawingAttributesModel.set(drawingOptionsModel, expectedDrawingMode);

		// Restore values
		drawingAttributesModel = new DrawingAttributesModel();
		drawingAttributesModel.initialize();
		drawingOptionsModel = drawingAttributesModel.getDrawingOptionsModel();
		String actualDrawingMode = drawingAttributesModel.getDrawingMode();

		assertEquals(expectedDrawingMode, actualDrawingMode);
		assertEquals(brushSize, drawingOptionsModel.getBrushSize(), 0.1);
		assertEquals(brushColorR, drawingOptionsModel.getBrushColor().getR());
		assertEquals(brushColorG, drawingOptionsModel.getBrushColor().getG());
		assertEquals(brushColorB, drawingOptionsModel.getBrushColor().getB());
		assertEquals(fillColorR, drawingOptionsModel.getFillColor().getR());
		assertEquals(fillColorG, drawingOptionsModel.getFillColor().getG());
		assertEquals(fillColorB, drawingOptionsModel.getFillColor().getB());

	}

}
