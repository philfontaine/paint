package model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.shape.EllipseModel;
import model.shape.RectangleModel;
import model.shape.UdesModel;

public class DrawingModelListTest {
	private DrawingModelList list = new DrawingModelList();
	
	private DrawingModel m1 = new RectangleModel();
	private DrawingModel m2 = new EllipseModel();
	private DrawingModel m3 = new UdesModel();
	
	private Point2D pt1 = new Point2D(10, 10);
	private Point2D pt2 = new Point2D(50, 50);
	private Point2D pt4 = new Point2D(10, 30);
	private Point2D pt5 = new Point2D(50, 30);
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSize() {
		assertEquals(0, list.size(), 0);
		list.add(m1);
		assertEquals(1, list.size(), 0);
		list.add(m2);
		assertEquals(2, list.size(), 0);
		list.remove(m1);
		assertEquals(1, list.size(), 0);
		list.remove(m1);
		assertEquals(1, list.size(), 0);
		list.remove(m2);
		assertEquals(0, list.size(), 0);
	}

	@Test
	public void testDrawingModelListDrawingModelList() {
		list.add(m1);
		list.add(m2);
		DrawingModelList listTest = new DrawingModelList(list);
		assertEquals(m1, listTest.get(0));
		assertEquals(m2, listTest.get(1));
	}

	@Test
	public void testSetCurrentDrawingModel() {
		list.add(m1);
		list.add(m2);
		list.setCurrent(m2);
		assertEquals(m2, list.getCurrent());
		list.setCurrent(null);
		assertNull(list.getCurrent());
		list.setCurrent(m1);
		assertEquals(list.getCurrent(), m1);
		list.setCurrent(m3);
		assertNull(list.getCurrent());
		
	}

	@Test
	public void testGetList() {
		assertEquals(0, list.getList().size(), 0);
		list.add(m1);
		list.add(m2);
		assertEquals(m1, list.getList().get(0));
		assertEquals(m2, list.getList().get(1));
	}

	@Test
	public void testSelectAll() {
		try {
			list.selectAll();
		} catch (Error e) {
			fail("can not selectAll() if empty");
		}
		list.add(m1);
		list.add(m2);
		list.selectAll();
		assertTrue(list.get(0).isSelected());
		assertTrue(list.get(1).isSelected());
	}

	@Test
	public void testSelectCurrent() {
		list.add(m1);
		list.add(m2);
		list.deselectAll();
		assertFalse(list.get(1).isSelected());
		list.selectCurrent();
		assertTrue(list.get(1).isSelected());
		list.deselectAll();
		list.setCurrent(null);
		list.selectCurrent();
		assertFalse(list.get(0).isSelected());
		assertFalse(list.get(1).isSelected());
		
	}

	@Test
	public void testDeselectAll() {
		list.add(m1);
		list.add(m2);
		list.selectAll();
		assertTrue(list.get(0).isSelected());
		assertTrue(list.get(1).isSelected());
		list.deselectAll();
		assertFalse(list.get(0).isSelected());
		assertFalse(list.get(1).isSelected());
	}

	@Test
	public void testGetSelectedItemsIndexes() {
		list.add(m1);
		list.add(m2);
		list.deselectAll();
		assertFalse(list.get(0).isSelected());
		assertFalse(list.get(1).isSelected());
		assertEquals(0, list.getSelectedItemsIndexes().size(), 0);
		m2.select();
		assertEquals(1, list.getSelectedItemsIndexes().size(), 0);
		assertEquals(1, list.getSelectedItemsIndexes().get(0), 0);
	}

	@Test
	public void testAnchorUnderMouse() {
		list.add(m1);
		m1.updateInitialPoint(pt1);
		m1.updateFinalPoint(pt2);
		list.add(m2);
		m2.updateInitialPoint(pt4);
		m2.updateFinalPoint(pt5);
		list.deselectAll();
		m2.select();
		assertNull(list.anchorUnderMouse(pt1));
		assertEquals(list.get(1).getAnchorList().getList().get(0), list.anchorUnderMouse(pt4));
	}

	@Test
	public void testMemorize() {
		list.add(m1);
		list.add(m2);
		assertNull(list.memorize(-5));
		assertNull(list.memorize(5));
		assertNotNull(list.memorize(0));
		assertEquals(m2.getClass(), list.memorize(1).getClass());
	}

	@Test
	public void testGetMemorized() {
		list.add(m1);
		list.add(m2);
		assertNull(list.getMemorized());
		assertNotNull(list.memorize(0));
		assertEquals(m1.getClass(), list.getMemorized().getClass());
		
	}

	@Test
	public void testSetMemorized() {
		list.add(m1);
		list.add(m2);
		assertNull(list.getMemorized());
		list.setMemorized(m3);
		assertNotNull(list.getMemorized());
		assertEquals(m3.getClass(), list.getMemorized().getClass());
		list.setMemorized(null);
		assertNull(list.getMemorized());
	}

	@Test
	public void testReplicate() {
		assertNull(list.replicate());
		list.setMemorized(m3);
		assertEquals(m3.getClass(), list.replicate().getClass());
	}

}
