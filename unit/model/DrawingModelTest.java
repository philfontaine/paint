package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.anchor.anchorposition.AnchorPositionNoX;
import model.anchor.anchorposition.AnchorPositionNoY;
import model.anchor.anchorposition.AnchorPositionSet;
import model.anchor.anchorposition.AnchorPositionType;
import model.anchor.anchorposition.AnchorPositionXI;
import model.anchor.anchorposition.AnchorPositionYI;

public class DrawingModelTest {
	DrawingModel drawingModel;
	private final double X = 100;
	private final double Y = 100;
	private final double WIDTH = 200;
	private final double HEIGHT = 200;
	private final DrawingOptionsModel DRAWING_OPTIONS_MODEL = new DrawingOptionsModel();
	private double halfBrush;

	@Before
	public void setUp() {
		setUpConstructorWithoutParams();
		halfBrush = DRAWING_OPTIONS_MODEL.getBrushSize() / 2;
	}

	public void setUpConstructorWithoutParams() {
		drawingModel = new DrawingModel() {

			@Override
			public void update(Point2D pt, SceneContext context) {
				// Undefined
			}
		};
	}

	public void setUpConstructorWithParams() {
		drawingModel = new DrawingModel(new Point2D(X, Y), new SceneContext(WIDTH, HEIGHT), DRAWING_OPTIONS_MODEL) {

			@Override
			public void update(Point2D pt, SceneContext context) {
				// Undefined

			}
		};
	}

	public void setUpConstructorWithOther() {
		drawingModel = new DrawingModel(
				new DrawingModel(new Point2D(X, Y), new SceneContext(WIDTH, HEIGHT), DRAWING_OPTIONS_MODEL) {
					@Override
					public void update(Point2D pt, SceneContext context) {
						// Undefined
					}
				}) {
			@Override
			public void update(Point2D pt, SceneContext context) {
				// Undefined
			}
		};
	}

	@Test
	public void testDrawingModel() {
		assertEquals(0, drawingModel.getXi(), 0.1);
		assertEquals(0, drawingModel.getXf(), 0.1);
		assertEquals(0, drawingModel.getYi(), 0.1);
		assertEquals(0, drawingModel.getYf(), 0.1);
		DrawingOptionsModel drawingOptionsModel = new DrawingOptionsModel();
		assertTrue(Helper.drawingOptionsModelAreEqual(drawingOptionsModel, drawingModel.getDrawingOptions()));
	}

	@Test
	public void testDrawingModelPoint2DSceneContextDrawingOptionsModel() {
		setUpConstructorWithParams();
		assertEquals(DRAWING_OPTIONS_MODEL, drawingModel.getDrawingOptions());
		assertEquals(X, drawingModel.getXi(), 0.1);
		assertEquals(X, drawingModel.getXf(), 0.1);
		assertEquals(Y, drawingModel.getXi(), 0.1);
		assertEquals(Y, drawingModel.getXf(), 0.1);
	}

	@Test
	public void testDrawingModelDrawingModel() {
		setUpConstructorWithOther();
		assertTrue(DRAWING_OPTIONS_MODEL.getBrushColor().isEqualTo(drawingModel.getDrawingOptions().getBrushColor()));
		assertTrue(DRAWING_OPTIONS_MODEL.getFillColor().isEqualTo(drawingModel.getDrawingOptions().getFillColor()));
		assertEquals(DRAWING_OPTIONS_MODEL.getBrushSize(), drawingModel.getDrawingOptions().getBrushSize(), 0);
		assertEquals(X, drawingModel.getXi(), 0.1);
		assertEquals(X, drawingModel.getXf(), 0.1);
		assertEquals(Y, drawingModel.getXi(), 0.1);
		assertEquals(Y, drawingModel.getXf(), 0.1);
		assertEquals(0, drawingModel.getRotation(), 0.1);
		assertEquals(0, drawingModel.getRotation(), 0.1);
	}

	@Test
	public void testGetDrawingSvg() {
		assertNull(drawingModel.getDrawingSvg());
	}

	@Test
	public void testSetDrawingOptions() {
		DrawingOptionsModel drawingOptions = new DrawingOptionsModel();
		assertNotSame(drawingModel.getDrawingOptions(), drawingOptions);
		drawingModel.setDrawingOptions(drawingOptions);
		assertEquals(drawingModel.getDrawingOptions(), drawingOptions);
	}

	@Test
	public void testGetTopLeftX() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(500, 600);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(pt1.getX(), drawingModel.getTopLeftX(), 0.001);

		drawingModel.updateInitialPoint(pt2, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(halfBrush, drawingModel.getTopLeftX(), 0.001);
	}

	@Test
	public void testGetTopLeftY() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(500, 600);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(pt1.getY(), drawingModel.getTopLeftY(), 0.001);

		drawingModel.updateInitialPoint(pt2, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(halfBrush, drawingModel.getTopLeftY(), 0.001);
	}

	@Test
	public void testGetTopLeft() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(500, 600);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(pt1.getX(), drawingModel.getTopLeft().getX(), 0.001);
		assertEquals(pt1.getY(), drawingModel.getTopLeft().getY(), 0.001);

		drawingModel.updateInitialPoint(pt2, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(halfBrush, drawingModel.getTopLeft().getX(), 0.001);
		assertEquals(halfBrush, drawingModel.getTopLeft().getY(), 0.001);
	}

	@Test
	public void testGetWidth() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(Math.abs(pt1.getX() - pt3.getX()), drawingModel.getWidth(), 0.001);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt2, ctx);
		assertEquals(Math.abs(halfBrush - pt1.getX()), drawingModel.getWidth(), 0.001);
	}

	@Test
	public void testGetHeight() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(Math.abs(pt1.getY() - pt3.getY()), drawingModel.getHeight(), 0.001);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt2, ctx);
		assertEquals(Math.abs(halfBrush - pt1.getY()), drawingModel.getHeight(), 0.001);
	}

	@Test
	public void testGetXi() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(pt1.getX(), drawingModel.getXi(), 0);

		drawingModel.updateInitialPoint(pt2, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(halfBrush, drawingModel.getXi(), 0);
	}

	@Test
	public void testGetXf() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(pt3.getX(), drawingModel.getXf(), 0);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt2, ctx);
		assertEquals(halfBrush, drawingModel.getXf(), 0);
	}

	@Test
	public void testGetYi() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(pt1.getY(), drawingModel.getYi(), 0);

		drawingModel.updateInitialPoint(pt2, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(halfBrush, drawingModel.getYi(), 0);
	}

	@Test
	public void testGetYf() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertEquals(pt3.getY(), drawingModel.getYf(), 0);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt2, ctx);
		assertEquals(halfBrush, drawingModel.getYf(), 0);
	}

	@Test
	public void testGetPi() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertTrue(drawingModel.getPi().sameAs(pt1));

		drawingModel.updateInitialPoint(pt2, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertTrue(drawingModel.getPi().sameAs(new Point2D(halfBrush, halfBrush)));
	}

	@Test
	public void testGetPf() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(200, 300);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		assertTrue(drawingModel.getPf().sameAs(pt3));

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt2, ctx);
		assertTrue(drawingModel.getPf().sameAs(new Point2D(halfBrush, halfBrush)));
	}

	@Test
	public void testSetAngleAndGetAngle() {
		double angleTest = -180;
		drawingModel.updateRotation(angleTest);
		assertEquals(angleTest, drawingModel.getRotation(), 0);
		angleTest = -1000;
		drawingModel.updateRotation(angleTest);
		assertEquals(-360, drawingModel.getRotation(), 0);
		angleTest = 1000;
		drawingModel.updateRotation(angleTest);
		assertEquals(360, drawingModel.getRotation(), 0);
	}

	@Test
	public void testSetAndGetTextModel() {
		DrawingTextModel textTest = new DrawingTextModel();
		String stringTest = "Test";
		textTest.setText(stringTest);
		drawingModel.updateText(textTest);
		assertEquals(stringTest, drawingModel.getTextModel().getText());
		assertEquals(textTest.getAlignment(), drawingModel.getTextModel().getAlignment());
		assertTrue(textTest.getColor().isEqualTo(drawingModel.getTextModel().getColor()));
		assertEquals(textTest.getFontWeight(), drawingModel.getTextModel().getFontWeight());
		assertEquals(textTest.getSize(), drawingModel.getTextModel().getSize());
		assertEquals(textTest.getVerticalPosition(), drawingModel.getTextModel().getVerticalPosition());
	}

	@Test
	public void testGetAnchorList() {
		// TODO
	}

	@Test
	public void testSelectAndDeselectAndIsSelected() {
		drawingModel.deselect();
		assertFalse(drawingModel.isSelected());
		drawingModel.deselect();
		assertFalse(drawingModel.isSelected());
		drawingModel.select();
		assertTrue(drawingModel.isSelected());
		drawingModel.select();
		assertTrue(drawingModel.isSelected());
		drawingModel.deselect();
		assertFalse(drawingModel.isSelected());
	}

	@Test
	public void testUpdateInitialPoint() {
		final double cw = 1000;
		final double ch = 1000;
		double xi = 200;
		double yi = 350;
		drawingModel.updateInitialPoint(new Point2D(xi, yi), new SceneContext(cw, ch));
		assertEquals(xi, drawingModel.getXi(), 0.1);
		assertEquals(yi, drawingModel.getYi(), 0.1);
	}

	@Test
	public void testUpdateFinalPoint() {
		final double cw = 1000;
		final double ch = 1000;
		double xf = 200;
		double yf = 350;
		drawingModel.updateFinalPoint(new Point2D(xf, yf), new SceneContext(cw, ch));
		assertEquals(xf, drawingModel.getXf(), 0.1);
		assertEquals(yf, drawingModel.getYf(), 0.1);
	}

	@Test
	public void testSetWidthWithContext() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(500, 600);

		final double widthTest1 = 200;
		final double widthTest2 = 2000;
		final double widthTest3 = -200;

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		final Point2D topLeft1 = drawingModel.getTopLeft();
		drawingModel.updateWidth(widthTest1, ctx.getWidth());
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft1));
		assertEquals(widthTest1, drawingModel.getWidth(), 0);

		drawingModel.updateInitialPoint(pt3, ctx);
		drawingModel.updateFinalPoint(pt1, ctx);
		final Point2D topLeft2 = drawingModel.getTopLeft();
		drawingModel.updateWidth(widthTest2, ctx.getWidth());
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft2));
		assertEquals(Math.abs(topLeft2.getX() - ctx.getWidth()) - halfBrush, drawingModel.getWidth(), 0);

		drawingModel.updateInitialPoint(pt3, ctx);
		drawingModel.updateFinalPoint(pt1, ctx);
		final Point2D topLeft3 = drawingModel.getTopLeft();
		final double width3 = drawingModel.getWidth();
		drawingModel.updateWidth(widthTest3, ctx.getWidth());
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft3));
		assertEquals(width3, drawingModel.getWidth(), 0);

		drawingModel.updateInitialPoint(pt3, ctx);
		drawingModel.updateFinalPoint(pt1, ctx);
		final Point2D topLeft4 = drawingModel.getTopLeft();
		final double width4 = drawingModel.getWidth();
		drawingModel.updateWidth(width4, ctx.getWidth());
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft4));
		assertEquals(width4, drawingModel.getWidth(), 0);
	}

	@Test
	public void testSetWidthWithoutContext() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt3 = new Point2D(150, 160);

		final double widthTest1 = 200;
		final double widthTest2 = 2000;
		final double widthTest3 = -200;

		drawingModel.updateInitialPoint(pt1);
		drawingModel.updateFinalPoint(pt3);
		final Point2D topLeft1 = drawingModel.getTopLeft();
		drawingModel.updateWidth(widthTest1);
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft1));
		assertEquals(widthTest1, drawingModel.getWidth(), 0);

		drawingModel.updateInitialPoint(pt3);
		drawingModel.updateFinalPoint(pt1);
		final Point2D topLeft2 = drawingModel.getTopLeft();
		drawingModel.updateWidth(widthTest2);
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft2));
		assertEquals(widthTest2, drawingModel.getWidth(), 0);

		drawingModel.updateInitialPoint(pt3);
		drawingModel.updateFinalPoint(pt1);
		final Point2D topLeft3 = drawingModel.getTopLeft();
		final double width3 = drawingModel.getWidth();
		drawingModel.updateWidth(widthTest3);
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft3));
		assertEquals(width3, drawingModel.getWidth(), 0);

		drawingModel.updateInitialPoint(pt3);
		drawingModel.updateFinalPoint(pt1);
		final Point2D topLeft4 = drawingModel.getTopLeft();
		final double width4 = drawingModel.getWidth();
		drawingModel.updateWidth(width4);
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft4));
		assertEquals(width4, drawingModel.getWidth(), 0);
	}

	@Test
	public void testSetHeightWithContext() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt3 = new Point2D(150, 160);
		final SceneContext ctx = new SceneContext(500, 600);

		final double heightTest1 = 200;
		final double heightTest2 = 2000;
		final double heightTest3 = -200;

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		final Point2D topLeft1 = drawingModel.getTopLeft();
		drawingModel.updateHeight(heightTest1, ctx.getHeight());
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft1));
		assertEquals(heightTest1, drawingModel.getHeight(), 0);

		drawingModel.updateInitialPoint(pt3, ctx);
		drawingModel.updateFinalPoint(pt1, ctx);
		final Point2D topLeft2 = drawingModel.getTopLeft();
		drawingModel.updateHeight(heightTest2, ctx.getHeight());
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft2));
		assertEquals(Math.abs(topLeft2.getY() - ctx.getHeight()) - halfBrush, drawingModel.getHeight(), 0);

		drawingModel.updateInitialPoint(pt3, ctx);
		drawingModel.updateFinalPoint(pt1, ctx);
		final Point2D topLeft3 = drawingModel.getTopLeft();
		final double height3 = drawingModel.getHeight();
		drawingModel.updateHeight(heightTest3, ctx.getHeight());
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft3));
		assertEquals(height3, drawingModel.getHeight(), 0);

		drawingModel.updateInitialPoint(pt3, ctx);
		drawingModel.updateFinalPoint(pt1, ctx);
		final Point2D topLeft4 = drawingModel.getTopLeft();
		final double height4 = drawingModel.getHeight();
		drawingModel.updateHeight(height4, ctx.getHeight());
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft4));
		assertEquals(height4, drawingModel.getHeight(), 0);
	}

	@Test
	public void testSetHeightWithoutContext() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt3 = new Point2D(150, 160);

		final double heightTest1 = 200;
		final double heightTest2 = 2000;
		final double heightTest3 = -200;

		drawingModel.updateInitialPoint(pt1);
		drawingModel.updateFinalPoint(pt3);
		final Point2D topLeft1 = drawingModel.getTopLeft();
		drawingModel.updateHeight(heightTest1);
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft1));
		assertEquals(heightTest1, drawingModel.getHeight(), 0);

		drawingModel.updateInitialPoint(pt3);
		drawingModel.updateFinalPoint(pt1);
		final Point2D topLeft2 = drawingModel.getTopLeft();
		drawingModel.updateHeight(heightTest2);
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft2));
		assertEquals(heightTest2, drawingModel.getHeight(), 0);

		drawingModel.updateInitialPoint(pt3);
		drawingModel.updateFinalPoint(pt1);
		final Point2D topLeft3 = drawingModel.getTopLeft();
		final double height3 = drawingModel.getHeight();
		drawingModel.updateHeight(heightTest3);
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft3));
		assertEquals(height3, drawingModel.getHeight(), 0);

		drawingModel.updateInitialPoint(pt3);
		drawingModel.updateFinalPoint(pt1);
		final Point2D topLeft4 = drawingModel.getTopLeft();
		final double height4 = drawingModel.getHeight();
		drawingModel.updateHeight(height4);
		assertTrue(drawingModel.getTopLeft().sameAs(topLeft4));
		assertEquals(height4, drawingModel.getHeight(), 0);
	}

	@Test
	public void testUpdateTopLeftWithContext() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final Point2D pt4 = new Point2D(170, 180);
		final SceneContext ctx = new SceneContext(500, 600);

		drawingModel.updateInitialPoint(pt1, ctx);
		drawingModel.updateFinalPoint(pt3, ctx);
		drawingModel.updateTopLeft(pt4, ctx);
		assertTrue(drawingModel.getTopLeft().sameAs(pt4));
		drawingModel.updateTopLeft(pt2, ctx);
		assertTrue(drawingModel.getTopLeft().sameAs(new Point2D(halfBrush, halfBrush)));

		drawingModel.updateInitialPoint(pt3, ctx);
		drawingModel.updateFinalPoint(pt1, ctx);
		drawingModel.updateTopLeft(pt4, ctx);
		assertTrue(drawingModel.getTopLeft().sameAs(pt4));
		drawingModel.updateTopLeft(pt2, ctx);
		assertTrue(drawingModel.getTopLeft().sameAs(new Point2D(halfBrush, halfBrush)));
	}

	@Test
	public void testUpdateTopLeftWithoutContext() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(-50, -60);
		final Point2D pt3 = new Point2D(150, 160);
		final Point2D pt4 = new Point2D(170, 180);

		drawingModel.updateInitialPoint(pt1);
		drawingModel.updateFinalPoint(pt3);
		drawingModel.updateTopLeft(pt4);
		assertTrue(drawingModel.getTopLeft().sameAs(pt4));
		drawingModel.updateTopLeft(pt2);
		assertTrue(drawingModel.getTopLeft().sameAs(pt2));

		drawingModel.updateInitialPoint(pt3);
		drawingModel.updateFinalPoint(pt1);
		drawingModel.updateTopLeft(pt4);
		assertTrue(drawingModel.getTopLeft().sameAs(pt4));
		drawingModel.updateTopLeft(pt2);
		assertTrue(drawingModel.getTopLeft().sameAs(pt2));
	}

	@Test
	public void testUpdateRotation() {
		final double rotationTest1 = -1000;
		final double rotationTest2 = -50;
		final double rotationTest3 = 50;
		final double rotationTest4 = 1000;

		drawingModel.updateRotation(rotationTest1);
		assertEquals(-360, drawingModel.getRotation(), 0);
		drawingModel.updateRotation(rotationTest2);
		assertEquals(rotationTest2, drawingModel.getRotation(), 0);
		drawingModel.updateRotation(rotationTest3);
		assertEquals(rotationTest3, drawingModel.getRotation(), 0);
		drawingModel.updateRotation(rotationTest3);
		assertEquals(rotationTest3, drawingModel.getRotation(), 0);
		drawingModel.updateRotation(rotationTest4);
		assertEquals(360, drawingModel.getRotation(), 0);
	}

	@Test
	public void testContains() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(100, 120);

		final Point2D ptTest1 = new Point2D(10, 10);
		final Point2D ptTest2 = new Point2D(80, 80);
		final Point2D ptTest3 = new Point2D(150, 150);
		final Point2D ptTest4 = new Point2D(80, 150);
		final Point2D ptTest5 = new Point2D(150, 80);
		final Point2D ptTest6 = new Point2D(80, 10);
		final Point2D ptTest7 = new Point2D(10, 80);

		final Point2D ptTestLimit1 = new Point2D(pt1.getX() - halfBrush, pt1.getY() - halfBrush);
		final Point2D ptTestLimit2 = new Point2D(pt2.getX() - halfBrush, pt2.getY() - halfBrush);

		drawingModel.updateInitialPoint(pt1);
		drawingModel.updateFinalPoint(pt2);

		assertFalse(drawingModel.contains(ptTest1));
		assertTrue(drawingModel.contains(ptTest2));
		assertFalse(drawingModel.contains(ptTest3));
		assertFalse(drawingModel.contains(ptTest4));
		assertFalse(drawingModel.contains(ptTest5));
		assertFalse(drawingModel.contains(ptTest6));
		assertFalse(drawingModel.contains(ptTest7));
		assertTrue(drawingModel.contains(ptTestLimit1));
		assertTrue(drawingModel.contains(ptTestLimit2));
	}

	@Test
	public void testIsWithin() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(100, 120);

		final Point2D ptTest1 = new Point2D(80, 80);
		final Point2D ptTest2 = new Point2D(90, 90);
		final Point2D ptTest3 = new Point2D(150, 150);
		final Point2D ptTest4 = new Point2D(10, 10);
		final Point2D ptTest5 = new Point2D(10, 150);
		final Point2D ptTest6 = new Point2D(150, 10);

		final Point2D ptTestLimit1 = new Point2D(pt1.getX(), pt1.getY());
		final Point2D ptTestLimit2 = new Point2D(pt2.getX(), pt2.getY());
		final Point2D ptTestLimit3 = new Point2D(pt1.getX(), pt2.getY());
		final Point2D ptTestLimit4 = new Point2D(pt2.getX(), pt1.getY());

		DrawingModel modelTest = new DrawingModel() {
			@Override
			public void update(Point2D pt, SceneContext context) {
				// Undefined
			}
		};

		modelTest.updateInitialPoint(pt1);
		modelTest.updateFinalPoint(pt2);

		drawingModel.updateInitialPoint(ptTest1);
		drawingModel.updateFinalPoint(ptTest2);
		assertTrue(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTest1);
		drawingModel.updateFinalPoint(ptTest3);
		assertFalse(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTest4);
		drawingModel.updateFinalPoint(ptTest2);
		assertFalse(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTest3);
		drawingModel.updateFinalPoint(ptTest4);
		assertFalse(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTest5);
		drawingModel.updateFinalPoint(ptTest6);
		assertFalse(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTest5);
		drawingModel.updateFinalPoint(ptTest1);
		assertFalse(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTest6);
		drawingModel.updateFinalPoint(ptTest2);
		assertFalse(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTestLimit1);
		drawingModel.updateFinalPoint(ptTestLimit2);
		assertTrue(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTestLimit4);
		drawingModel.updateFinalPoint(ptTestLimit3);
		assertTrue(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTestLimit3);
		drawingModel.updateFinalPoint(ptTest3);
		assertFalse(drawingModel.isWithin(modelTest));
		drawingModel.updateInitialPoint(ptTestLimit1);
		drawingModel.updateFinalPoint(ptTest5);
		assertFalse(drawingModel.isWithin(modelTest));

	}

	@Test
	public void testAnchorUnderMouse() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(100, 120);
		drawingModel.updateInitialPoint(pt1);
		drawingModel.updateFinalPoint(pt2);

		final Point2D ptTestTrue1 = new Point2D(pt1.getX(), pt1.getY());
		final Point2D ptTestTrue2 = new Point2D(pt1.getX(), (pt1.getY() + pt2.getY()) / 2);
		final Point2D ptTestTrue3 = new Point2D(pt1.getX(), pt2.getY());
		final Point2D ptTestTrue4 = new Point2D(pt2.getX(), pt1.getY());
		final Point2D ptTestTrue5 = new Point2D(pt2.getX(), (pt1.getY() + pt2.getY()) / 2);
		final Point2D ptTestTrue6 = new Point2D(pt2.getX(), pt2.getY());
		final Point2D ptTestTrue7 = new Point2D((pt1.getX() + pt2.getX()) / 2, pt1.getY());
		final Point2D ptTestTrue8 = new Point2D((pt1.getX() + pt2.getX()) / 2, pt2.getY());

		assertNotNull(drawingModel.anchorUnderMouse(ptTestTrue1));
		assertEquals(AnchorPositionType.XI, drawingModel.anchorUnderMouse(ptTestTrue1).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.YI, drawingModel.anchorUnderMouse(ptTestTrue1).getPosSet().getVpos().getType());

		assertNotNull(drawingModel.anchorUnderMouse(ptTestTrue2));
		assertEquals(AnchorPositionType.XI, drawingModel.anchorUnderMouse(ptTestTrue2).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.NOY,
				drawingModel.anchorUnderMouse(ptTestTrue2).getPosSet().getVpos().getType());

		assertNotNull(drawingModel.anchorUnderMouse(ptTestTrue3));
		assertEquals(AnchorPositionType.XI, drawingModel.anchorUnderMouse(ptTestTrue3).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.YF, drawingModel.anchorUnderMouse(ptTestTrue3).getPosSet().getVpos().getType());

		assertNotNull(drawingModel.anchorUnderMouse(ptTestTrue4));
		assertEquals(AnchorPositionType.XF, drawingModel.anchorUnderMouse(ptTestTrue4).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.YI, drawingModel.anchorUnderMouse(ptTestTrue4).getPosSet().getVpos().getType());

		assertNotNull(drawingModel.anchorUnderMouse(ptTestTrue5));
		assertEquals(AnchorPositionType.XF, drawingModel.anchorUnderMouse(ptTestTrue5).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.NOY,
				drawingModel.anchorUnderMouse(ptTestTrue5).getPosSet().getVpos().getType());

		assertNotNull(drawingModel.anchorUnderMouse(ptTestTrue6));
		assertEquals(AnchorPositionType.XF, drawingModel.anchorUnderMouse(ptTestTrue6).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.YF, drawingModel.anchorUnderMouse(ptTestTrue6).getPosSet().getVpos().getType());

		assertNotNull(drawingModel.anchorUnderMouse(ptTestTrue7));
		assertEquals(AnchorPositionType.NOX,
				drawingModel.anchorUnderMouse(ptTestTrue7).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.YI, drawingModel.anchorUnderMouse(ptTestTrue7).getPosSet().getVpos().getType());

		assertNotNull(drawingModel.anchorUnderMouse(ptTestTrue8));
		assertEquals(AnchorPositionType.NOX,
				drawingModel.anchorUnderMouse(ptTestTrue8).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.YF, drawingModel.anchorUnderMouse(ptTestTrue8).getPosSet().getVpos().getType());
	}

	@Test
	public void testAnchorAtPosition() {
		final Point2D pt1 = new Point2D(50, 60);
		final Point2D pt2 = new Point2D(100, 120);
		drawingModel.updateInitialPoint(pt1);
		drawingModel.updateFinalPoint(pt2);

		AnchorPositionXI xi = new AnchorPositionXI();
		AnchorPositionYI yi = new AnchorPositionYI();
		AnchorPositionNoX nox = new AnchorPositionNoX();
		AnchorPositionNoY noy = new AnchorPositionNoY();

		AnchorPositionSet xiYi = new AnchorPositionSet(xi, yi);
		AnchorPositionSet noxNoY = new AnchorPositionSet(nox, noy);

		assertNotNull(drawingModel.getAnchorAtPosition(xiYi));
		assertEquals(AnchorPositionType.XI, drawingModel.getAnchorAtPosition(xiYi).getPosSet().getHpos().getType());
		assertEquals(AnchorPositionType.YI, drawingModel.getAnchorAtPosition(xiYi).getPosSet().getVpos().getType());

		assertNull(drawingModel.getAnchorAtPosition(noxNoY));

	}
}
