package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;

import observer.IObserver;

public class DrawingOptionsModelTest {
	DrawingOptionsModel drawingOptionsModel;

	@Test
	public void testDrawingOptionsModel() {
		final double brushSize = 1;
		final Color brushColor = new Color(0, 0, 0);
		final Color fillColor = new Color(255, 255, 255);
		drawingOptionsModel = new DrawingOptionsModel();
		assertEquals(brushSize, drawingOptionsModel.getBrushSize(), 0.1);
		assertTrue(Helper.colorsAreEqual(brushColor, drawingOptionsModel.getBrushColor()));
		assertTrue(Helper.colorsAreEqual(fillColor, drawingOptionsModel.getFillColor()));
	}

	@Test
	public void testDrawingOptionsModelDoubleColorColor() {
		final double brushSize = 1;
		final Color brushColor = new Color(0, 0, 0);
		final Color fillColor = new Color(255, 255, 255);
		drawingOptionsModel = new DrawingOptionsModel(brushSize, brushColor, fillColor);
		assertEquals(brushSize, drawingOptionsModel.getBrushSize(), 0.1);
		assertEquals(brushColor, drawingOptionsModel.getBrushColor());
		assertEquals(fillColor, drawingOptionsModel.getFillColor());
	}

	@Test
	public void testDrawingOptionsModelDrawingOptionsModel() {
		final double brushSize = 1;
		final Color brushColor = new Color(0, 0, 0);
		final Color fillColor = new Color(255, 255, 255);
		DrawingOptionsModel otherDrawingOptionsModel = new DrawingOptionsModel(brushSize, brushColor, fillColor);
		drawingOptionsModel = new DrawingOptionsModel(otherDrawingOptionsModel);
		assertEquals(brushSize, drawingOptionsModel.getBrushSize(), 0.1);
		assertTrue(Helper.colorsAreEqual(brushColor, drawingOptionsModel.getBrushColor()));
		assertTrue(Helper.colorsAreEqual(fillColor, drawingOptionsModel.getFillColor()));
	}

	@Test
	public void testSetBrushSize() {
		final double brushSize = 7;
		IObserver observer = mock(IObserver.class);
		drawingOptionsModel = new DrawingOptionsModel();
		drawingOptionsModel.register(observer);
		drawingOptionsModel.setBrushSize(brushSize);
		assertEquals(brushSize, drawingOptionsModel.getBrushSize(), 0.1);
		verify(observer, times(1)).update();
	}

	@Test
	public void testSetBrushColor() {
		final Color brushColor = new Color();
		IObserver observer = mock(IObserver.class);
		drawingOptionsModel = new DrawingOptionsModel();
		drawingOptionsModel.register(observer);
		drawingOptionsModel.setBrushColor(brushColor);
		assertEquals(brushColor, drawingOptionsModel.getBrushColor());
		verify(observer, times(1)).update();
	}

	@Test
	public void testSetFillColor() {
		final Color fillColor = new Color();
		IObserver observer = mock(IObserver.class);
		drawingOptionsModel = new DrawingOptionsModel();
		drawingOptionsModel.register(observer);
		drawingOptionsModel.setFillColor(fillColor);
		assertEquals(fillColor, drawingOptionsModel.getFillColor());
		verify(observer, times(1)).update();
	}

}
