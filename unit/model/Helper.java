package model;

import model.Color;
import model.DrawingOptionsModel;

public class Helper {
	private Helper() {
	}

	public static boolean drawingOptionsModelAreEqual(DrawingOptionsModel d1, DrawingOptionsModel d2) {
		return d1.getBrushSize() == d2.getBrushSize() && d1.getBrushColor().getR() == d2.getBrushColor().getR()
				&& d1.getBrushColor().getG() == d2.getBrushColor().getG()
				&& d1.getBrushColor().getB() == d2.getBrushColor().getB()
				&& d1.getFillColor().getB() == d2.getFillColor().getB()
				&& d1.getFillColor().getB() == d2.getFillColor().getB()
				&& d1.getFillColor().getB() == d2.getFillColor().getB();
	}

	public static boolean colorsAreEqual(Color c1, Color c2) {
		return c1.getR() == c2.getR() && c1.getG() == c2.getG() && c1.getB() == c2.getB();
	}
}
