package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import model.MagneticGrid;
import model.Point2D;
import model.SceneContext;

public class MagneticGridTest {
	private MagneticGrid magneticGrid;
	final int scaleWidth = 150;
	final int scaleHeight = 300;

	@Before
	public void setUp() {
		magneticGrid = new MagneticGrid(scaleWidth, scaleHeight);
	}

	@Test
	public void testMagneticGrid() {
		assertEquals(scaleWidth, magneticGrid.getScaleWidth());
		assertEquals(scaleHeight, magneticGrid.getScaleHeight());
		assertFalse(magneticGrid.getIsActive());
	}

	@Test
	public void testSetGetWidth() {
		final int newScaleWidth = 315;

		magneticGrid.setScaleWidth(newScaleWidth);

		assertEquals(newScaleWidth, magneticGrid.getScaleWidth());
	}

	@Test
	public void testSetGetHeight() {
		final int newScaleHeight = 430;

		magneticGrid.setScaleHeight(newScaleHeight);

		assertEquals(newScaleHeight, magneticGrid.getScaleHeight());
	}

	@Test
	public void testSetGetActive() {
		boolean isActive = magneticGrid.getIsActive();

		magneticGrid.changeIsActive();

		assertEquals(!isActive, magneticGrid.getIsActive());
		
		magneticGrid.changeIsActive();

		assertEquals(isActive, magneticGrid.getIsActive());
	}

	@Test
	public void testNearestPoint() {
		Point2D p1 = new Point2D(290,910);
		SceneContext c1= new SceneContext(200,1000);
		
		Point2D r1 = magneticGrid.nearestPoint(p1, c1);
		
		assertEquals(200, r1.getX(), 0);
		assertEquals(900, r1.getY(), 0);
	}

}
