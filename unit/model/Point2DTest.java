package model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Point2D;

public class Point2DTest {

	@Test
	public void testPoint2D() {
		final double x = 0;
		final double y = 0;
		Point2D point2d = new Point2D();
		assertEquals(x, point2d.getX(), 0.1);
		assertEquals(y, point2d.getY(), 0.1);
	}

	@Test
	public void testPoint2DDoubleDouble() {
		final double x = 7;
		final double y = 12;
		Point2D point2d = new Point2D(x, y);
		assertEquals(x, point2d.getX(), 0.1);
		assertEquals(y, point2d.getY(), 0.1);
	}

	@Test
	public void testSetGetX() {
		Point2D point2d = new Point2D();
		final double x = 15;
		point2d.setX(x);
		assertEquals(x, point2d.getX(), 0.1);
	}

	@Test
	public void testSetGetY() {
		Point2D point2d = new Point2D();
		final double y = 15;
		point2d.setY(y);
		assertEquals(y, point2d.getY(), 0.1);
	}

}
