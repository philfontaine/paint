package model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.SceneContext;

public class SceneContextTest {

	@Test
	public void testSceneContext() {
		final double width = 0;
		final double height = 0;
		SceneContext sceneContext = new SceneContext();
		assertEquals(width, sceneContext.getWidth(), 0.1);
		assertEquals(height, sceneContext.getHeight(), 0.1);
	}

	@Test
	public void testSceneContextDoubleDouble() {
		final double width = 7;
		final double height = 12;
		SceneContext sceneContext = new SceneContext(width, height);
		assertEquals(width, sceneContext.getWidth(), 0.1);
		assertEquals(height, sceneContext.getHeight(), 0.1);
	}

	@Test
	public void testSetGetWidth() {
		SceneContext sceneContext = new SceneContext();
		final double width = 15;
		sceneContext.setWidth(width);
		assertEquals(width, sceneContext.getWidth(), 0.1);
	}

	@Test
	public void testSetGetHeight() {
		SceneContext sceneContext = new SceneContext();
		final double height = 15;
		sceneContext.setHeight(height);
		assertEquals(height, sceneContext.getHeight(), 0.1);
	}

}
