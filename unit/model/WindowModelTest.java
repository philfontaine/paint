package model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.WindowModel;

public class WindowModelTest {

	@Test
	public void test() {
		WindowModel windowModel;

		final double x = 12;
		final double y = 34;
		final double width = 56;
		final double height = 78;
		final boolean isMaximized = false;

		// Store values while not maximized, should store everything
		windowModel = new WindowModel();
		windowModel.set(x, y, width, height, isMaximized);

		// Restore values
		windowModel = new WindowModel();
		windowModel.initialize();
		assertEquals(x, windowModel.getX(), 0.1);
		assertEquals(y, windowModel.getY(), 0.1);
		assertEquals(width, windowModel.getWidth(), 0.1);
		assertEquals(height, windowModel.getHeight(), 0.1);
		assertEquals(isMaximized, windowModel.isMaximized());

		// Store values while maximized, should not store window position
		double newX = 15;
		double newY = 16;
		double newWidth = 21;
		double newHeight = 98;
		boolean newIsMaximized = true;
		windowModel = new WindowModel();
		windowModel.set(newX, newY, newWidth, newHeight, newIsMaximized);

		// Restore values
		windowModel = new WindowModel();
		windowModel.initialize();
		assertEquals(x, windowModel.getX(), 0.1);
		assertEquals(y, windowModel.getY(), 0.1);
		assertEquals(width, windowModel.getWidth(), 0.1);
		assertEquals(height, windowModel.getHeight(), 0.1);
		assertEquals(newIsMaximized, windowModel.isMaximized());
	}

}
