package model.anchor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.Point2D;
import model.anchor.anchorposition.AnchorPositionNoX;
import model.anchor.anchorposition.AnchorPositionNoY;
import model.anchor.anchorposition.AnchorPositionSet;
import model.anchor.anchorposition.AnchorPositionXF;
import model.anchor.anchorposition.AnchorPositionXI;
import model.anchor.anchorposition.AnchorPositionYF;
import model.anchor.anchorposition.AnchorPositionYI;
import model.anchor.anchorposition.IAnchorPosition;
import model.shape.RectangleModel;

public class AnchorPointListTest {
	private AnchorPointList anchorList;

	private DrawingModel m;

	private Point2D pt1 = new Point2D(20, 20);
	private Point2D pt2 = new Point2D(50, 50);
	private Point2D pt4 = new Point2D(100, 100);

	private double halfAnchor;

	private int tolerance = 2;

	@Before
	public void setUp() throws Exception {
		m = new RectangleModel();
		m.updateInitialPoint(pt1);
		m.updateFinalPoint(pt2);

		anchorList = new AnchorPointList(m);

		halfAnchor = anchorList.getAnchorSize() / 2;

	}

	@Test
	public void testIsOnAnchor() {
		Point2D ptTest = new Point2D(pt1.getX(), pt1.getY());
		assertEquals(anchorList.getList().get(0), anchorList.isOnAnchor(ptTest));
		ptTest = new Point2D(pt1.getX() + halfAnchor, pt1.getY() + halfAnchor + tolerance);
		assertEquals(anchorList.getList().get(0), anchorList.isOnAnchor(ptTest));
		ptTest = new Point2D(pt1.getX(), pt1.getY() - halfAnchor - tolerance);
		assertEquals(anchorList.getList().get(0), anchorList.isOnAnchor(ptTest));
		ptTest = new Point2D(pt1.getX() - halfAnchor, pt1.getY());
		assertEquals(anchorList.getList().get(0), anchorList.isOnAnchor(ptTest));
		ptTest = new Point2D(pt1.getX(), pt4.getY());
		assertNull(anchorList.isOnAnchor(ptTest));
		ptTest = new Point2D(pt4.getX(), pt1.getY());
		assertNull(anchorList.isOnAnchor(ptTest));
		ptTest = new Point2D(pt4.getX(), pt4.getY());
		assertNull(anchorList.isOnAnchor(ptTest));
	}

	@Test
	public void testGetListAndSetter() {
		IAnchorPosition top = new AnchorPositionYI();
		IAnchorPosition bottom = new AnchorPositionYF();
		IAnchorPosition left = new AnchorPositionXI();
		IAnchorPosition right = new AnchorPositionXF();
		IAnchorPosition noHorizontal = new AnchorPositionNoX();
		IAnchorPosition noVertical = new AnchorPositionNoY();

		assertTrue(anchorList.getList().get(0).asSamePosSet(new AnchorPositionSet(left, top)));
		assertTrue(anchorList.getList().get(1).asSamePosSet(new AnchorPositionSet(left, noVertical)));
		assertTrue(anchorList.getList().get(2).asSamePosSet(new AnchorPositionSet(left, bottom)));
		assertTrue(anchorList.getList().get(3).asSamePosSet(new AnchorPositionSet(noHorizontal, bottom)));
		assertTrue(anchorList.getList().get(4).asSamePosSet(new AnchorPositionSet(right, bottom)));
		assertTrue(anchorList.getList().get(5).asSamePosSet(new AnchorPositionSet(right, noVertical)));
		assertTrue(anchorList.getList().get(6).asSamePosSet(new AnchorPositionSet(right, top)));
		assertTrue(anchorList.getList().get(7).asSamePosSet(new AnchorPositionSet(noHorizontal, top)));
	}

	@Test
	public void testGetModel() {
		assertEquals(m, anchorList.getModel());
	}

	@Test
	public void testGetAnchorSize() {
		assertEquals(10, anchorList.getAnchorSize(), 0);
	}

}
