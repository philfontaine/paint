package model.anchor;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;
import model.anchor.anchorposition.AnchorPositionSet;
import model.anchor.anchorposition.AnchorPositionXI;
import model.anchor.anchorposition.AnchorPositionYF;
import model.anchor.anchorposition.AnchorPositionYI;
import model.shape.RectangleModel;

public class AnchorPointTest {
	
	private AnchorPoint anchor;
	private DrawingModel m;
	private AnchorPositionSet pos;
	
	private Point2D pt1 = new Point2D(12, 13);
	private Point2D pt2 = new Point2D(14, 15);
	private Point2D pt4 = new Point2D(-5,-5);
	
	private SceneContext ctx = new SceneContext(200,200);
	private double halfBrush;

	@Before
	public void setUp() throws Exception {
		m = new RectangleModel();
		m.updateInitialPoint(pt1);
		m.updateFinalPoint(pt2);
		AnchorPositionXI xi = new AnchorPositionXI();
		AnchorPositionYI yi = new AnchorPositionYI();
		pos = new AnchorPositionSet(xi, yi);
		
		anchor = new AnchorPoint(m, pos);
		
		halfBrush = m.getBrushSize() / 2;
	}

	@Test
	public void testSetPositionWithContext() {
		anchor.setPosition(pt4, ctx);
		assertEquals(halfBrush, anchor.getPosition().getX(), 0.0001);
		assertEquals(halfBrush, anchor.getPosition().getY(), 0.0001);
	}

	@Test
	public void testSetPosition() {
		anchor.setPosition(pt4);
		assertEquals(pt4.getX(), anchor.getPosition().getX(), 0.0001);
		assertEquals(pt4.getY(), anchor.getPosition().getY(), 0.0001);
	}

	@Test
	public void testGetPosition() {
		assertEquals(pt1.getX(), anchor.getPosition().getX(), 0.0001);
		assertEquals(pt1.getY(), anchor.getPosition().getY(), 0.0001);
	}

	@Test
	public void testGetModel() {
		assertEquals(m, anchor.getModel());
	}

	@Test
	public void testGetPosSet() {
		assertEquals(pos, anchor.getPosSet());
	}

	@Test
	public void testAsSamePosSet() {
		AnchorPositionXI xi = new AnchorPositionXI();
		AnchorPositionYI yi = new AnchorPositionYI();
		AnchorPositionYF yf = new AnchorPositionYF();
		assertTrue(anchor.asSamePosSet(new AnchorPositionSet(xi, yi)));
		assertFalse(anchor.asSamePosSet(new AnchorPositionSet(xi, yf)));
	}

}
