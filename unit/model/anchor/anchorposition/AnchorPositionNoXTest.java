package model.anchor.anchorposition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;
import model.anchor.anchorposition.AnchorPositionNoX;
import model.anchor.anchorposition.AnchorPositionType;
import model.shape.RectangleModel;

public class AnchorPositionNoXTest {

	private AnchorPositionNoX anchorPos = new AnchorPositionNoX();
	
	private DrawingModel model1 = new RectangleModel();
	private DrawingModel model2 = new RectangleModel();
	double halfBrush;
	
	private SceneContext ctx = new SceneContext(200,200);
	private Point2D pt1 = new Point2D(12, 13);
	private Point2D pt2 = new Point2D(14, 15);
	private Point2D pt3 = new Point2D(-5, -5);
	private Point2D pt4 = new Point2D(18,19);
	
	@Before
	public void setUp() throws Exception {
		model1.updateInitialPoint(pt1);
		model1.updateFinalPoint(pt2);
		halfBrush = model1.getBrushSize() / 2;
	}
	
	@Test
	public void testGetType() {
		assertEquals(AnchorPositionType.NOX, anchorPos.getType());
	}

	@Test
	public void testModifyWhithContext() {
		anchorPos.modify(model1, pt3, ctx);
		assertEquals(model1.getPi().getX(), pt1.getX(), 0);
		assertEquals(model1.getPi().getY(), pt1.getY(), 0);
		assertEquals(model1.getPf().getX(), pt2.getX(), 0);
		assertEquals(model1.getPf().getY(), pt2.getY(), 0);
	}
	
	@Test
	public void testModifyWhithoutContext() {
		anchorPos.modify(model1, pt4);
		assertEquals(model1.getPi().getX(), pt1.getX(), 0);
		assertEquals(model1.getPi().getY(), pt1.getY(), 0);
		assertEquals(model1.getPf().getX(), pt2.getX(), 0);
		assertEquals(model1.getPf().getY(), pt2.getY(), 0);
	}

	@Test
	public void testGetPosition() {
		model2.updateInitialPoint(pt2);
		model2.updateFinalPoint(pt1);
		assertEquals((pt1.getX() + pt2.getX()) / 2, anchorPos.getPosition(model2), 0.0001);
	}

}
