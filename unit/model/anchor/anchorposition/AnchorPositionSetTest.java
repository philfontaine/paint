package model.anchor.anchorposition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;
import model.shape.RectangleModel;

public class AnchorPositionSetTest {
	
	private AnchorPositionYI yi;
	private AnchorPositionYF yf;
	private AnchorPositionXI xi;
	private AnchorPositionXF xf;
	private AnchorPositionNoX nox;
	private AnchorPositionNoY noy;
	
	private AnchorPositionSet xiYi;
	private AnchorPositionSet xiYf;
	private AnchorPositionSet xfYi;
	private AnchorPositionSet xfYf;
	private AnchorPositionSet noxYi;
	private AnchorPositionSet noxYf;
	private AnchorPositionSet xiNoY;
	private AnchorPositionSet xfNoY;
	private AnchorPositionSet noxNoY;
	private AnchorPositionSet yiXi;

	private Point2D pt1 = new Point2D(12, 13);
	private Point2D pt2 = new Point2D(14, 15);
	private Point2D pt3 = new Point2D(16,17);
	private Point2D pt4 = new Point2D(-5,-5);
	
	private DrawingModel m;
	private double halfBrush;
	
	private SceneContext ctx = new SceneContext(200,200);
	
	@Before
	public void setUp() throws Exception {
		xi = new AnchorPositionXI();
		xf = new AnchorPositionXF();
		yi = new AnchorPositionYI();
		yf = new AnchorPositionYF();
		nox = new AnchorPositionNoX();
		noy = new AnchorPositionNoY();
		
		
		m = new RectangleModel();
		m.updateInitialPoint(pt1);
		m.updateFinalPoint(pt2);
		
		halfBrush = m.getBrushSize() / 2;
		
		xiYi = new AnchorPositionSet(xi, yi);
		xiYf = new AnchorPositionSet(xi, yf);
		xfYi = new AnchorPositionSet(xf, yi);
		xfYf = new AnchorPositionSet(xf, yf);
		noxYi = new AnchorPositionSet(nox, yi);
		noxYf = new AnchorPositionSet(nox, yf);
		xiNoY = new AnchorPositionSet(xi, noy);
		xfNoY = new AnchorPositionSet(xf, noy);
		noxNoY = new AnchorPositionSet(nox, noy);
		yiXi = new AnchorPositionSet(yi, xi);
	}

	@Test
	public void testSetter() {
		AnchorPositionSet xiYi = new AnchorPositionSet(xi, yi);
		assertEquals(xiYi.getHpos().getType(), AnchorPositionType.XI);
		assertEquals(xiYi.getVpos().getType(), AnchorPositionType.YI);
		
		AnchorPositionSet xiYf = new AnchorPositionSet(xi, yf);
		assertEquals(xiYf.getHpos().getType(), AnchorPositionType.XI);
		assertEquals(xiYf.getVpos().getType(), AnchorPositionType.YF);
		
		AnchorPositionSet xfYi = new AnchorPositionSet(xf, yi);
		assertEquals(xfYi.getHpos().getType(), AnchorPositionType.XF);
		assertEquals(xfYi.getVpos().getType(), AnchorPositionType.YI);
		
		AnchorPositionSet xfYf = new AnchorPositionSet(xf, yf);
		assertEquals(xfYf.getHpos().getType(), AnchorPositionType.XF);
		assertEquals(xfYf.getVpos().getType(), AnchorPositionType.YF);
		
		AnchorPositionSet noxYi = new AnchorPositionSet(nox, yi);
		assertEquals(noxYi.getHpos().getType(), AnchorPositionType.NOX);
		assertEquals(noxYi.getVpos().getType(), AnchorPositionType.YI);
		
		AnchorPositionSet noxYf = new AnchorPositionSet(nox, yf);
		assertEquals(noxYf.getHpos().getType(), AnchorPositionType.NOX);
		assertEquals(noxYf.getVpos().getType(), AnchorPositionType.YF);
		
		AnchorPositionSet xiNoY = new AnchorPositionSet(xi, noy);
		assertEquals(xiNoY.getHpos().getType(), AnchorPositionType.XI);
		assertEquals(xiNoY.getVpos().getType(), AnchorPositionType.NOY);
		
		AnchorPositionSet xfNoY = new AnchorPositionSet(xf, noy);
		assertEquals(xfNoY.getHpos().getType(), AnchorPositionType.XF);
		assertEquals(xfNoY.getVpos().getType(), AnchorPositionType.NOY);
		
		AnchorPositionSet noxNoY = new AnchorPositionSet(nox, noy);
		assertEquals(noxNoY.getHpos().getType(), AnchorPositionType.NOX);
		assertEquals(noxNoY.getVpos().getType(), AnchorPositionType.NOY);
		
		AnchorPositionSet yiXi = new AnchorPositionSet(yi, xi);
		assertEquals(yiXi.getHpos().getType(), AnchorPositionType.NOX);
		assertEquals(yiXi.getVpos().getType(), AnchorPositionType.NOY);
	}

	@Test
	public void testModifyXiYi() {
		xiYi.modify(m, pt3);
		assertEquals(pt3.getX(), m.getPi().getX(), 0);
		assertEquals(pt3.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyXiYf() {
		xiYf.modify(m, pt3);
		assertEquals(pt3.getX(), m.getPi().getX(),0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt3.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyXfYi() {
		xfYi.modify(m, pt3);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt3.getY(), m.getPi().getY(), 0);
		assertEquals(pt3.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyXfYf() {
		xfYf.modify(m, pt3);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt3.getX(), m.getPf().getX(), 0);
		assertEquals(pt3.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyXiNoY() {
		xiNoY.modify(m, pt3);
		assertEquals(pt3.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyXfNoY() {
		xfNoY.modify(m, pt3);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt3.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyNoXYi() {
		noxYi.modify(m, pt3);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt3.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyNoXYf() {
		noxYf.modify(m, pt3);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt3.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyNoXNoY() {
		noxNoY.modify(m, pt3);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyYiXi() {
		yiXi.modify(m, pt3);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyWithContextXiYi() {
		xiYi.modify(m, pt4, ctx);
		assertEquals(halfBrush, m.getPi().getX(), 0.001);
		assertEquals(halfBrush, m.getPi().getY(), 0.001);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyWithContextXiYf() {
		xiYf.modify(m, pt4, ctx);
		assertEquals(halfBrush, m.getPi().getX(), 0.001);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(halfBrush, m.getPf().getY(), 0.001);
	}

	@Test
	public void testModifyWithContextXfYi() {
		xfYi.modify(m, pt4, ctx);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(halfBrush, m.getPi().getY(), 0.001);
		assertEquals(halfBrush, m.getPf().getX(), 0.001);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyWithContextXfYf() {
		xfYf.modify(m, pt4, ctx);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(halfBrush, m.getPf().getX(), 0.001);
		assertEquals(halfBrush, m.getPf().getY(), 0.001);
	}

	@Test
	public void testModifyWithContextXiNoY() {
		xiNoY.modify(m, pt4, ctx);
		assertEquals(halfBrush, m.getPi().getX(), 0.001);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyWithContextXfNoY() {
		xfNoY.modify(m, pt4, ctx);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(halfBrush, m.getPf().getX(), 0.001);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyWithContextNoXYi() {
		noxYi.modify(m, pt4, ctx);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(halfBrush, m.getPi().getY(), 0.001);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyWithContextNoXYf() {
		noxYf.modify(m, pt4, ctx);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(halfBrush, m.getPf().getY(), 0.001);
	}

	@Test
	public void testModifyWithContextNoXNoY() {
		noxNoY.modify(m, pt4, ctx);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testModifyWithContextYiXi() {
		yiXi.modify(m, pt4, ctx);
		assertEquals(pt1.getX(), m.getPi().getX(), 0);
		assertEquals(pt1.getY(), m.getPi().getY(), 0);
		assertEquals(pt2.getX(), m.getPf().getX(), 0);
		assertEquals(pt2.getY(), m.getPf().getY(), 0);
	}

	@Test
	public void testGetPosition() {
		assertEquals(pt1.getX(), xiYi.getPosition(m).getX(), 0);
		assertEquals(pt1.getY(), xiYi.getPosition(m).getY(), 0);
	}
	
	@Test
	public void testSameAs() {
		assertTrue(xiYi.sameAs(new AnchorPositionSet(xi, yi)));
		assertFalse(xiYi.sameAs(new AnchorPositionSet(xi, yf)));
		assertFalse(xiYi.sameAs(new AnchorPositionSet(xf, yi)));
		assertFalse(xiYi.sameAs(new AnchorPositionSet(xf, yf)));
	}

}
