package model.anchor.anchorposition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.Point2D;
import model.SceneContext;
import model.anchor.anchorposition.AnchorPositionYF;
import model.anchor.anchorposition.AnchorPositionType;
import model.shape.RectangleModel;

public class AnchorPositionYFTest {

	private AnchorPositionYF anchorPos = new AnchorPositionYF();
	
	private DrawingModel model1 = new RectangleModel();
	private DrawingModel model2 = new RectangleModel();
	double halfBrush;
	
	private SceneContext ctx = new SceneContext(200,200);
	private Point2D pt1 = new Point2D(12, 13);
	private Point2D pt2 = new Point2D(14, 15);
	private Point2D pt3 = new Point2D(-5, -5);
	private Point2D pt4 = new Point2D(18,19);
	
	@Before
	public void setUp() throws Exception {
		model1.updateInitialPoint(pt1);
		model1.updateFinalPoint(pt2);
		halfBrush = model1.getBrushSize() / 2;
	}
	
	@Test
	public void testGetType() {
		assertEquals(AnchorPositionType.YF, anchorPos.getType());
	}

	@Test
	public void testModifyWhithContext() {
		anchorPos.modify(model1, pt3, ctx);
		assertEquals(pt1.getX(), model1.getPi().getX(), 0);
		assertEquals(pt1.getY(), model1.getPi().getY(), 0);
		assertEquals(pt2.getX(), model1.getPf().getX(), 0);
		assertEquals(halfBrush, model1.getPf().getY(), 0);
	}
	
	@Test
	public void testModifyWhithoutContext() {
		anchorPos.modify(model1, pt4);
		assertEquals(pt1.getX(), model1.getPi().getX(), 0);
		assertEquals(pt1.getY(), model1.getPi().getY(), 0);
		assertEquals(pt2.getX(), model1.getPf().getX(), 0);
		assertEquals(pt4.getY(), model1.getPf().getY(), 0);
	}

	@Test
	public void testGetPosition() {
		model2.updateInitialPoint(pt2);
		model2.updateFinalPoint(pt1);
		assertEquals(model2.getYf(), anchorPos.getPosition(model2), 0);
	}

}
