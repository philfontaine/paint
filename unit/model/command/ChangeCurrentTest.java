package model.command;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.DrawingModelList;
import model.command.ChangeCurrent;
import model.shape.RectangleModel;

public class ChangeCurrentTest {

	private DrawingModelList list;

	private DrawingModel m1;
	private DrawingModel m2;
	private DrawingModel m3;

	@Before
	public void setUp() throws Exception {
		list = new DrawingModelList();
		m1 = new RectangleModel();
		list.add(m1);
		m2 = new RectangleModel();
		list.add(m2);
		m3 = new RectangleModel();
		list.add(m3);

		assertEquals(list.size(), 3);
		assertEquals(list.getCurrent(), m3);
	}

	@Test
	public void testExecuteUndoCommand() {
		ChangeCurrent c = new ChangeCurrent(1);

		c.execute(list);
		assertEquals(m2, list.getCurrent());

		c.undo();
		assertEquals(m3, list.getCurrent());
	}

	@Test
	public void testToString() {
		ChangeCurrent c = new ChangeCurrent();
		assertEquals(c.toString(), "Change Current");
	}

	@Test
	public void testChangeCurrentInt() {
		int expected = 3;
		ChangeCurrent c = new ChangeCurrent(expected);
		assertEquals(expected, c.newIndex);
	}

}
