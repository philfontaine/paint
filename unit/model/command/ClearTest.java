package model.command;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.DrawingModelList;
import model.command.Clear;
import model.shape.RectangleModel;

public class ClearTest {

	private DrawingModelList list;

	private DrawingModel m1;
	private DrawingModel m2;
	private DrawingModel m3;

	@Before
	public void setUp() throws Exception {
		list = new DrawingModelList();
		m1 = new RectangleModel();
		list.add(m1);
		m2 = new RectangleModel();
		list.add(m2);
		m3 = new RectangleModel();
		list.add(m3);

		assertEquals(list.size(), 3);
		assertEquals(list.getCurrent(), m3);
	}

	@Test
	public void testExecuteUndoCommand() {
		Clear c = new Clear();

		c.execute(list);
		assertEquals(list.size(), 0);

		c.undo();
		assertEquals(m1, list.get(0));
		assertEquals(m2, list.get(1));
		assertEquals(m3, list.get(2));
		assertEquals(m3, list.getCurrent());
	}

	@Test
	public void testToString() {
		Clear c = new Clear();
		assertEquals(c.toString(), "Clear");
	}
}
