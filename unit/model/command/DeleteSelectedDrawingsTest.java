package model.command;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.DrawingModelList;
import model.command.DeleteSelectedDrawings;
import model.shape.RectangleModel;

public class DeleteSelectedDrawingsTest {

	private DrawingModelList list;

	private DrawingModel m1;
	private DrawingModel m2;
	private DrawingModel m3;

	@Before
	public void setUp() throws Exception {
		list = new DrawingModelList();
		m1 = new RectangleModel();
		list.add(m1);
		m2 = new RectangleModel();
		list.add(m2);
		m3 = new RectangleModel();
		list.add(m3);

		assertEquals(list.size(), 3);
		assertEquals(list.getCurrent(), m3);
	}

	@Test
	public void testExecuteUndoCommand() {
		List<Integer> indexes = new ArrayList<>();
		indexes.add(0);
		indexes.add(2);

		DeleteSelectedDrawings c = new DeleteSelectedDrawings(indexes);
		c.execute(list);
		assertEquals(list.size(), 1);
		assertEquals(list.getCurrent(), m2);
		assertEquals(list.getFirst(), m2);
		assertEquals(list.getLast(), m2);

		c.undo();
		assertEquals(m1, list.get(0));
		assertEquals(m2, list.get(1));
		assertEquals(m3, list.get(2));
		assertEquals(m3, list.getCurrent());
	}

	@Test
	public void testToString() {
		DeleteSelectedDrawings c = new DeleteSelectedDrawings();
		assertEquals(c.toString(), "Deleting selected drawings");
	}
}
