package model.command;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.Color;
import model.DrawingModel;
import model.DrawingModelList;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.command.EditDrawingBrushSize;
import model.shape.RectangleModel;

public class EditDrawingBrushSizeTest {

	private DrawingModelList list;

	private DrawingModel m1;
	private DrawingModel m2;

	private Color c1 = new Color(12, 13, 14);
	private double brushSize1 = 5.0;
	private double brushSize2 = 10.0;

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel(brushSize1, c1, c1);
	private Point2D pt = new Point2D(1, 2);
	private SceneContext context = new SceneContext(200, 200);
	
	private int indexTest = 0;

	@Before
	public void setUp() throws Exception {
		list = new DrawingModelList();
		m1 = new RectangleModel(pt, context, new DrawingOptionsModel(drawingOptions));
		m2 = new RectangleModel(pt, context, new DrawingOptionsModel(drawingOptions));
		list.add(m1);
		list.add(m2);

		assertEquals(list.size(), 2);
		assertEquals(list.getCurrent(), m2);
	}

	@Test
	public void testExecuteUndoCommand() {
		EditDrawingBrushSize c = new EditDrawingBrushSize(brushSize1, brushSize2, indexTest);

		c.execute(list);
		assertEquals(list.get(indexTest).getDrawingOptions().getBrushSize(), brushSize2, 0);

		c.undo();
		assertEquals(list.get(indexTest).getDrawingOptions().getBrushSize(), brushSize1, 0);
	}

	@Test
	public void testToString() {
		EditDrawingBrushSize c = new EditDrawingBrushSize();
		assertEquals(c.toString(), "Edit Brush Size");
	}
}
