package model.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.Color;
import model.DrawingModel;
import model.DrawingModelList;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.command.EditDrawingFillColor;
import model.shape.RectangleModel;

public class EditDrawingFillColorTest {

	private DrawingModelList list;

	private DrawingModel m1;
	private DrawingModel m2;

	private Color c1 = new Color(12, 13, 14);
	private Color c2 = new Color(15, 16, 17);

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel(1, c1, c1);
	private Point2D pt = new Point2D(1, 2);
	private SceneContext context = new SceneContext(200, 200);

	private int indexTest = 0;
	
	@Before
	public void setUp() throws Exception {
		list = new DrawingModelList();
		m1 = new RectangleModel(pt, context, new DrawingOptionsModel(drawingOptions));
		m2 = new RectangleModel(pt, context, new DrawingOptionsModel(drawingOptions));
		list.add(m1);
		list.add(m2);

		assertEquals(list.size(), 2);
		assertEquals(list.getCurrent(), m2);
	}

	@Test
	public void testExecuteUndoCommand() {
		EditDrawingFillColor c = new EditDrawingFillColor(c1, c2, indexTest);

		c.execute(list);
		assertTrue(list.get(indexTest).getDrawingOptions().getFillColor().equals(c2));

		c.undo();
		assertTrue(list.get(indexTest).getDrawingOptions().getFillColor().equals(c1));
	}

	@Test
	public void testToString() {
		EditDrawingFillColor c = new EditDrawingFillColor();
		assertEquals(c.toString(), "Edit Fill Color");
	}
}
