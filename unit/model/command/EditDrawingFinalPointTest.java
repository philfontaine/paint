package model.command;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.Color;
import model.DrawingModel;
import model.DrawingModelList;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.command.EditDrawingFinalPoint;
import model.shape.RectangleModel;

public class EditDrawingFinalPointTest {

	private DrawingModelList list;

	private DrawingModel m1;

	private Color c1 = new Color(12, 13, 14);

	private Point2D p1 = new Point2D(20, 21);
	private Point2D p2 = new Point2D(30, 31);

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel(1, c1, c1);
	private Point2D pt = new Point2D(1, 2);
	private SceneContext context = new SceneContext(200, 200);

	private int indexTest = 0;
	
	@Before
	public void setUp() throws Exception {
		list = new DrawingModelList();
		m1 = new RectangleModel(pt, context, new DrawingOptionsModel(drawingOptions));
		m1.updateFinalPoint(p1);
		list.add(m1);

		assertEquals(list.size(), 1);
		assertEquals(list.getCurrent(), m1);
	}

	@Test
	public void testExecuteUndoCommand() {
		EditDrawingFinalPoint c = new EditDrawingFinalPoint(p1, p2, indexTest);

		c.execute(list);
		assertEquals(list.get(indexTest).getXf(), p2.getX(), 0);
		assertEquals(list.get(indexTest).getYf(), p2.getY(), 0);

		c.undo();
		assertEquals(list.get(indexTest).getXf(), p1.getX(), 0);
		assertEquals(list.get(indexTest).getYf(), p1.getY(), 0);
	}

	@Test
	public void testToString() {
		EditDrawingFinalPoint c = new EditDrawingFinalPoint();
		assertEquals(c.toString(), "Edit Anchor");
	}
}
