package model.command;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import model.Color;
import model.DrawingModel;
import model.DrawingModelList;
import model.DrawingOptionsModel;
import model.Point2D;
import model.SceneContext;
import model.command.EditDrawingHeight;
import model.shape.RectangleModel;

public class EditDrawingHeightTest {

	private DrawingModelList list;

	private DrawingModel m1;

	private Color c1 = new Color(12, 13, 14);

	private double height1 = 20;
	private double height2 = 30;

	private DrawingOptionsModel drawingOptions = new DrawingOptionsModel(1, c1, c1);
	private Point2D pt = new Point2D(1, 2);
	private SceneContext context = new SceneContext(200, 200);
	
	private int indexTest = 0;

	@Before
	public void setUp() throws Exception {
		list = new DrawingModelList();
		m1 = new RectangleModel(pt, context, new DrawingOptionsModel(drawingOptions));
		m1.updateFinalPoint(new Point2D(pt.getX(), pt.getY() + height1));
		list.add(m1);

		assertEquals(list.size(), 1);
		assertEquals(list.getCurrent(), m1);
	}

	@Test
	public void testExecuteUndoCommand() {
		EditDrawingHeight c = new EditDrawingHeight(height1, height2, context, indexTest);

		c.execute(list);
		assertEquals(list.get(indexTest).getHeight(), height2, 0);

		c.undo();
		assertEquals(list.get(indexTest).getHeight(), height1, 0);
	}

	@Test
	public void testToString() {
		EditDrawingHeight c = new EditDrawingHeight();
		assertEquals(c.toString(), "Edit Height");
	}
}
