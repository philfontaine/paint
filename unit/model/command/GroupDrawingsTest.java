package model.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import model.DrawingModel;
import model.DrawingModelList;
import model.command.GroupDrawings;
import model.shape.RectangleModel;

public class GroupDrawingsTest {

	private DrawingModelList list;

	private DrawingModel m1;
	private DrawingModel m2;
	private DrawingModel m3;

	@Before
	public void setUp() throws Exception {
		list = new DrawingModelList();
		m1 = new RectangleModel();
		list.add(m1);
		m2 = new RectangleModel();
		list.add(m2);
		m3 = new RectangleModel();
		list.add(m3);

		assertEquals(list.size(), 3);
		assertEquals(list.getCurrent(), m3);
	}

	@Test
	public void testExecuteUndoCommand() {
		List<Integer> indexes1 = new ArrayList<>();
		indexes1.add(0);
		indexes1.add(2);

		GroupDrawings c = new GroupDrawings(indexes1);

		c.execute(list);
		assertEquals(list.size(), 2);
		assertTrue(!list.getList().contains(m1));
		assertTrue(list.getList().contains(m2));
		assertEquals(m2, list.get(0));
		assertTrue(!list.getList().contains(m3));

		c.undo();
		assertEquals(3, list.size());
		assertEquals(m1, list.get(0));
		assertEquals(m2, list.get(1));
		assertEquals(m3, list.get(2));
		assertEquals(m3, list.getCurrent());
	}

	@Test
	public void testExecuteUndoCommand2() {
		List<Integer> indexes1 = new ArrayList<>();
		indexes1.add(0);

		GroupDrawings c = new GroupDrawings(indexes1);

		c.execute(list);
		assertEquals(3, list.size());
		assertEquals(m1, list.get(0));
		assertEquals(m2, list.get(1));
		assertEquals(m3, list.get(2));
		assertEquals(m3, list.getCurrent());

		c.undo();
		assertEquals(3, list.size());
		assertEquals(m1, list.get(0));
		assertEquals(m2, list.get(1));
		assertEquals(m3, list.get(2));
		assertEquals(m3, list.getCurrent());
	}

	@Test
	public void testToString() {
		GroupDrawings c = new GroupDrawings();
		assertEquals(c.toString(), "Group Drawings");
	}
}
